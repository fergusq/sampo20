
sampo_worker = new Worker('sampo-loader.js');

const consoleElem = document.getElementById("console");
const textInput = document.getElementById("text");
const saveButton = document.getElementById("save");
const runButton = document.getElementById("run");
const stackElem = document.getElementById("stack");

editorContent = "";
lines = [""];
index = 0;

function printToPage(msg) {
	if (msg.endsWith("§")) {
		msg = msg.slice(0, -1);
	} else {
		msg += "\n";
	}
	message_lines = msg.split("\n");
	lines[lines.length-1] = lines[lines.length-1].substring(0, index) + " ".repeat(Math.max(0, index - lines[lines.length-1].length)) + message_lines[0] + lines[lines.length-1].substring(index + message_lines[0].length);
	if (message_lines.length > 1) {
		index = 0;
	}
	message_lines.slice(1).forEach(x => lines.push(x));
	lines = lines.slice(Math.max(0, lines.length-20));
	consoleElem.textContent = lines.join("\n");
	if (lines[lines.length-1] !== "") {
		index += lines[lines.length-1].length;
		var width = (70 - lines[lines.length-1].length) * 8;
		textInput.style.width = width + "px";
	}
}

function clearPage() {
	lines = [""];
	index = 0;
	consoleElem.textContent = "";
}

function tab(n) {
	index = n;
}

sampo_worker.onmessage = function (message) {
	if (message.data[0] === "print") {
		printToPage(message.data[1]);
	} else if (message.data[0] == "run") {
		eval(message.data[1]);
	} else if (message.data[0] == "update render") {
		turtle_update_render(message.data[1]);
	} else if (message.data[0] == "update message") {
		updateLoading(message.data[1]);
	} else if (message.data[0] == "update test") {
		document.getElementById('results').innerHTML += message.data[1];
		if (message.data.length == 3) {
			document.getElementById('results').lastChild.lastChild.lastChild.innerText = message.data[2];
		}
		window.scrollBy({ top: 1000 });
	} else if (message.data[0] == "clear page") {
		clearPage();
	} else if (message.data[0] == "tab") {
		tab(message.data[1]);
	} else if (message.data[0] == "edit frame") {
		editorContent = myCodeMirror.getValue();
		myCodeMirror.setValue(message.data[1].split("\n").map(line => line.replace(/\s+$/g, "")).join("\n"));
		textInput.disabled = true;
		runButton.disabled = true;
		saveButton.disabled = false;
	} else if (message.data[0] == "update stack") {
		stackElem.innerText = "";
		message.data[1].forEach(line => {
			stackElem.innerText += line + "\n";
		});
	} else if (message.data[0] == "hide stack") {
		stackElem.innerText = "";
	}
}

textInput.focus();
textInput.addEventListener('keyup', function (e) {
	if (e.key === 'Enter' || e.keyCode === 13) {
		sampo_worker.postMessage(["input", textInput.value]);
		textInput.value = "";
	}
});

var myCodeMirror = CodeMirror(document.querySelector(".editor-container"), {
	value: `Ö Tähän kenttään kirjoitetun teksti uusin versio tallentuu
Ö jatkuvasti selaimen paikalliseen muistiin (local
Ö storage), jotta koodi ei katoaisi kun välilehti
Ö suljetaan tai virkistetään

LUO :koko YMP
    36 KERTAA
        :koko ETEEN
        10 OIKEA
    VIELÄ?
VALMIS

KONNA
5 KERTAA I * 5 = YMP VIELÄ?`,
	mode: "sampo",
	theme: "sampo",
	lineNumbers: true,
	firstLineNumber: 0,
	indentUnit: 4,
	electricChars: true,
	smartIndent: true,
	extraKeys: {
		Tab: (cm) => {
			if (cm.somethingSelected()) {
				cm.execCommand('indentMore');
			} else {
				cm.execCommand('insertSoftTab');
			}
		},
		Backspace: (cm) => {
			if (!cm.somethingSelected()) {
				const cursorsPos = cm.listSelections().map((selection) => selection.anchor);
				const indentUnit = cm.options.indentUnit;
				let shouldDelChar = false;
				for (let cursorPos of cursorsPos) {
					const indentation = cm.getStateAfter(cursorPos.line).indented;
					if (!(indentation !== 0 &&
						cursorPos.ch <= indentation &&
						cursorPos.ch % indentUnit === 0 &&
						cursorPos.ch !== 0 &&
						myCodeMirror.getLine(cursorPos.line).substring(0, cursorPos.ch).trim() === '')) {
						shouldDelChar = true;
					}
				}
				if (!shouldDelChar) {
					cm.execCommand('indentLess');
				} else {
					cm.execCommand('delCharBefore');
				}
			} else {
				cm.execCommand('delCharBefore');
			}
		},
		'Shift-Tab': (cm) => cm.execCommand('indentLess')
	}
});

if(localStorage.getItem('current code') != null){
	myCodeMirror.setValue(localStorage.getItem('current code'));
}

myCodeMirror.on("keyup", () => {
	localStorage.setItem('current code', myCodeMirror.getValue());
});

saveButton.addEventListener("click", () => {
	sampo_worker.postMessage(["save frame", myCodeMirror.getValue()]);
	textInput.disabled = false;
	runButton.disabled = false;
	saveButton.disabled = true;
	myCodeMirror.setValue(editorContent);
});

runButton.addEventListener("click", () => {
	sampo_worker.postMessage(["input", myCodeMirror.getValue()]);
});
