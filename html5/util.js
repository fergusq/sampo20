function updateLoading(msg){
        document.getElementById("lataustila").textContent = msg;
}

// CPS-tyylinen kutsu, mutta data kulkee result-globaalimuuttujassa
// Dataa ei muuten saa fiksusti chibistä takaisin kutsuun
// Tämä tarvitaan FS-olion löytämiseksi

result = [];

async function loadFiles(files) {
	postMessage(["update message", "Ladataan SAMPO-tiedostojärjestelmä"]);
	result = await (await fetch("fs.json")).json();
}

function writeFiles(FS) {
    for(i = 0; i < result.length; i++){
	postMessage(["update message", "Kirjoitetaan tiedosto: " + result[i].path]);
	if(result[i].is_dir){
		FS.mkdir(result[i].path);
	}else{
		FS.writeFile(result[i].path, result[i].contents);
	}
    }
    postMessage(['update message', 'Ladattu SAMPO-kirjasto']);
}
