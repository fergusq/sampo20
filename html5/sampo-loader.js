importScripts("util.js", "chibi.js", "turtle-client.js");

read_cursor = 0;
write_cursor = 0;

messages = {};

function printLine(line) {
	postMessage(["print", line]);
}

function setMessage(msg){
	message = msg;
}

function pollLine() {
	if(read_cursor < write_cursor){
		return "true";
	} else {
		return "false";
	}
}

function readLine() {
	localMessage = messages[read_cursor];
	// TODO: Poista viesti jonosta
	read_cursor += 1
	return localMessage;
}

frame = "";

// Aloitetaan Chibi-tulkki vasta kun tiedostot on ladattu

async function initSampo(program) {
	await loadFiles();
	postMessage(["update message", "Alustetaan Chibi Scheme"]);
	gchibi = Chibi({
		print: printLine,
		printErr: (text) => {
			console.log("err: " + text);
		},
		program
	});
}

initSampo(`
(import (scheme base) (scheme write) (scheme char) (chibi emscripten))

; Tästä kontekstista päästään käsiksi Chibin sisuskaluihin niin, että voidaan ladata sampo muista tiedostoista

(eval-script! "writeFiles(FS);")

(import (sampo) (util))

(eval-script! "
onmessage = function (msg) {
	if (msg.data[0] == 'input') {
		messages[write_cursor] = msg.data[1];
		write_cursor += 1;
		Module['resume']();
	} else if (msg.data[0] == 'set canvas size') {
		canvas_width = msg.data[1];
		canvas_height = msg.data[2];
	} else if (msg.data[0] == 'save frame') {
		frame = msg.data[1];
		Module['resume']();
	}
}")

(define (web-reader message)
    (newline)
    (display message)
    (display "§")
    (newline)
    ; Tarkistetaan, onko tiedossa syöterivi
    (if (not (eq? (string-eval-script "pollLine();") "true"))
        (wait-on-event!)) ; Jos ei ole, niin annetaan tilanteen hallinta Webworkerin JS-puolelle (joka odottaa kunnes sisään tulee viesti)
    ; Tässä vaiheessa tiedetään, että readLine(); palauttaa jonkin arvon
    (let ((line (string-eval-script "readLine();")))
	  (display line)
	  (display " §")
	  (newline)
	  (parameterize ((current-input-port (open-input-string line)))
	    (read-code (list web-reader)))))

(eval-script! "postMessage(['update message', 'Valmis!']);")

(display "SAMPO 20\n\n")

(with-exception-handler
  (lambda (x)
	(display "VIRHE\n")
	(display x)
	(newline)
	(raise x))
  (lambda ()
    (parameterize ((sampo-reader web-reader)
				   (sampo-prompt "")
				   (sampo-prompt-partial "")
				   (sampo-response "ok"))
      (sampo-repl initial-state))))


(display "LOPETETTU\n")

`)
