// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

// Based on Forth mode by Aliaksei Chapyzhenka

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
  "use strict";

  function toWordList(words) {
    var ret = [];
    words.split(' ').forEach(function(e){
      ret.push({name: e});
    });
    return ret;
  }

  var coreWordList = toWordList(
'SAMAT? KOPIO TUHOA ED SEU TIE LST MUU LAS PIN OHJ LAA GRA SYÖ HAL AUTA PISTE UR RIVI V O T E A Y K Ö UUSIRIVI UUSISIVU SCHEMEDISPLAY ? . POISTA VAIHDA KIERRÄ TUPLAA YLI KPL PÄÄ HÄNTÄ PERÄ ALKUPÄÄ PÄÄKSI PERÄKSI LIITÄ KUULUU? LISTAKSI ALKIOKSI LUVUKSI ALKIO? LISTA? =$ KERTAA TOISTA JOS TOSI? VALE? LUO MUUTTUJA JONO VAKIO UNOHDA ASEMA PINO NÄYTÄ PIILOTA ARVO LLE = + - * / MOD ^ == <> < <= > >= TAI JA SATTUMA INT " # SIN COS LUELUKU LUKUTEKSTI KONNA UUSI KOTIIN OIKEA VASEN ETEEN TAAKSE KULMA PAIKKA VIIVA RVIIVA JANA TULOSTA SIVUSUHDE YLÖS ALAS PIIRRÄ PYYHI KUVA XKO YKO SUUNTA RUUTUINEN TIEDOSTO AVAA SULJE LUE *LUE TALLETA *TALLETA SUORITA KENTÄT SÄÄNTÖ TIETOKANTA POIMI APU _VIIMEAPU DIR CD A: B: C: SELITÄ TEE LOPETA I J');

  var immediateWordList = toWordList('KERTAA VIELÄ? TOISTA ALKUUN YHÄ? JOS TOSI? VALE? MUUTEN JATKA ON NIIN LOPPUUN LOPPU LUO VALMIS MUUTTUJA JONO VAKIO UNOHDA TIEDOSTO AVAA CD SELITÄ');

  var plusIndent = 'KERTAA YHÄ? TOSI? VALE? ON NIIN LUO'.split(' ');
  var minusIndent = 'VIELÄ? ALKUUN JATKA LOPPUN LOPPU VALMIS'.split(' ');

  CodeMirror.defineMode('sampo', function() {
    function searchWordList (wordList, word) {
      var i;
      for (i = wordList.length - 1; i >= 0; i--) {
        if (wordList[i].name === word.toUpperCase()) {
          return wordList[i];
        }
      }
      return undefined;
    }
  return {
    startState: function() {
      return {
        state: '',
        base: 10,
        coreWordList: coreWordList,
        immediateWordList: immediateWordList,
        wordList: [],
        indented: 0,
      };
    },
    //electricChars: " \n",
    electricInput: new RegExp("\\s+(" + minusIndent.join("|").replace("?", "\\?") + "|MUUTEN)$", "i"),
    indent: function(stt, textAfter) {
      var mat = textAfter.match(/^(\S+)(\s+|$)/);
      if (mat && (minusIndent.includes(mat[1].toUpperCase()) || mat[1].toUpperCase() === 'MUUTEN')) {
        return stt.indented - 4;
      }
      return stt.indented;
    },
    token: function (stream, stt) {
      var mat;
      if (stream.eatSpace()) {
        return null;
      }
      if (stt.state === 'vardef') {
        mat = stream.match(/^(\:\S+)(\s+|$)/);
        if (mat) {
          stt.wordList.push({name: mat[1].toUpperCase()});
          return 'def';
        }
        mat = stream.match(/^(\S+)(\s+|$)/);
        if (mat) {
          stt.state = '';
          stt.wordList.push({name: mat[1].toUpperCase()});
          return 'def';
        }
      }
      mat = stream.match(/^(MUUTTUJA|VAKIO|JONO|LUO)(\s+|$)/i);
      if (mat) {
        if (plusIndent.includes(mat[1].toUpperCase())) {
          stt.indented += 4;
        } else if (minusIndent.includes(mat[1].toUpperCase())) {
          stt.indented -= 4;
        }
        stt.state = 'vardef'
        return 'keyword';
      }
      mat = stream.match(/^("|#)\s+(\S+)(\s|$)+/);
      if (mat) {
        return 'string';
      }

      // dynamic wordlist
      mat = stream.match(/^(\S+)(\s+|$)/);
      if (mat) {
        if (plusIndent.includes(mat[1].toUpperCase())) {
          stt.indented += 4;
        } else if (minusIndent.includes(mat[1].toUpperCase())) {
          stt.indented -= 4;
        }

        if (searchWordList(stt.wordList, mat[1]) !== undefined) {
          return 'variable';
        }

        // comments
        if (mat[1] === 'ö' || mat[1] === 'Ö') {
          stream.skipToEnd();
            return 'comment';
          }

          // core words
          if (searchWordList(stt.immediateWordList, mat[1]) !== undefined) {
            return 'keyword';
          }
          if (searchWordList(stt.coreWordList, mat[1]) !== undefined) {
            return 'builtin';
          }

          if (mat[1] === '(') {
            var depth = 1;
            stream.eatWhile(function (s) {
              if (s === '(') depth++;
              else if (s === ')') depth--;
              return depth > 0;
            });
            stream.eat(')');
            return 'string';
          }

          // numbers
          if (mat[1].replace(",", ".").replace("/", ".") - 0xfffffffff) {
            return 'number';
          }
          // if (mat[1].match(/^[-+]?[0-9]+\.[0-9]*/)) {
          //     return 'number';
          // }

          return 'atom';
        }
      }
    };
  });
  CodeMirror.defineMIME("text/x-sampo", "sampo");
});
