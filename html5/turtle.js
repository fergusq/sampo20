var x_factor = 1;
var y_factor = 1;

resolution.addEventListener("change", () => {
	if (resolution.value == "small") {
		x_factor = 1;
		y_factor = 2;
	} else {
		x_factor = y_factor = 1;
	}
	sampo_worker.postMessage(["set canvas size", 640/x_factor, 400/y_factor]);
	console.log("Muutettiin resoluutioksi " + 640/x_factor + "x" + 400/y_factor);
})

function turtle_update_render(data){
	var ctx = canvas.getContext("2d");
	ctx.fillStyle = "rgb(0, 0, 0)";
	ctx.fillRect(0, 0, 640, 400);
	for(var i = 0; i < data.chains.length; i++){
		if(data.chains[i].color){
			ctx.strokeStyle = "white";
		}else{
			ctx.strokeStyle = "black";
		}
		ctx.beginPath();
		ctx.moveTo(data.positions[data.chains[i].start].x * x_factor, 400 - data.positions[data.chains[i].start].y * y_factor);
		for(var ii = data.chains[i].start; ii < data.chains[i].end; ii++){
			ctx.lineTo(data.positions[ii].x * x_factor, 400 - data.positions[ii].y * y_factor);
		}
		ctx.stroke();
	}
	if(data.pen_is_down){
		ctx.strokeStyle = "white";
		var x_i = -Math.sin(data.direction * Math.PI / 180)*7;
		var x_j = Math.cos(data.direction * Math.PI / 180)*7;
		var y_i = -Math.cos(data.direction * Math.PI / 180)*20;
		var y_j = Math.sin(data.direction * Math.PI / 180)*20;
		var tip_x = data.positions[data.positions.length-1].x;
		var tip_y = data.positions[data.positions.length-1].y;
		ctx.beginPath();
		ctx.moveTo(tip_x * x_factor, 400 - tip_y * y_factor);
		ctx.lineTo((tip_x + x_i + y_i) * x_factor, 400 - (tip_y - y_j + x_j) * y_factor);
		ctx.lineTo((tip_x - x_i + y_i) * x_factor, 400 - (tip_y - y_j - x_j) * y_factor);
		ctx.lineTo(tip_x * x_factor, 400 - tip_y * y_factor);
		ctx.stroke();
	}
	if(data.turtle_visible){
		ctx.font = "20px MxPlus";
		ctx.fillStyle = "white";
		ctx.fillText("X = " + Math.floor(data.positions[data.positions.length-1].x) + " Y = " + Math.floor(data.positions[data.positions.length-1].y) + " KULMA = " + Math.floor(data.direction), 10, 20)
	}
	for(var text of data.texts) {
		ctx.font = (8 * y_factor) + "px MxPlus";
		ctx.fillStyle = "white";
		ctx.fillText(text.content, text.x * x_factor, 400 - text.y * y_factor);
	}
}
