
importScripts("util.js", "chibi.js", "turtle-client.js");

// Lisätään dokumenttiin testitulostaulukko

postMessage(['run', `

document.body.innerHTML += \`
<table id="results" border="1" style="width: 100%;">
  <tr>
    <th style="width: 10%;">Testi</th>
    <th style="width: 10%;">Tulema</th>
    <th style="width: 80%;">Viesti</th>
  </tr>
</table>\`;
document.getElementsByClassName("development-environment")[0].style.display = "none";
`]);

function add_success(test, significant){
	var classes = "success result-cell";
	if(significant){
		classes += " significant";
	}
	postMessage(['update test', "<tr><td>" + test + '</td><td class="'+classes+'">✓</td><td></td>']);
}

function add_failure(test, message, significant){
	var classes = "failure result-cell";
	if(significant){
		classes += " significant";
	}
	postMessage(['update test', "<tr><td>" + test + '</td><td class="'+classes+'">✗</td><td></td></tr>', message]);
}

function add_header(message){
	postMessage(['update test', '<tr><th colspan="3">' + message + "</th></tr>"]);
}

function final_result(success, fail, global_failure){
	var of_which_significant = "";
	if(global_failure){
		of_which_significant = ", vähintään yksi merkitsevä testi epäonnistui";
	}
	postMessage(['update test', '<tr><th colspan="3">' + success + " / " + (success + fail) + of_which_significant + "</th></tr>"]);
	Module['resume']();
}

// Aloitetaan Chibi-tulkki vasta kun tiedostot on ladattu

async function initSampo(program) {
	await loadFiles();
	postMessage(["update message", "Ladattu kaikki tiedostot, alustetaan SAMPO"]);
	gchibi = Chibi({
		print: console.log,
		printErr: (text) => {
			console.log("err: " + text);
		},
		program
	});
}

initSampo(`
(import (scheme base) (scheme write) (scheme char) (chibi emscripten))

; Tästä kontekstista päästään käsiksi Chibin sisuskaluihin niin, että voidaan ladata sampo muista tiedostoista

(eval-script! "writeFiles(FS);")

; Nyt sampo voidaan ladata turvallisesti

(import (sampo-test) (util))

(define (add-success test significant)
    (eval-script!
      (construct-call "add_success" (list test significant))))

(define (add-failure test message significant)
    (eval-script!
      (construct-call "add_failure" (list test message significant))))

(define (add-header header)
    (eval-script!
      (construct-call "add_header" (list header))))

(define (final-result success fail global-failure)
    (eval-script!
      (construct-call "final_result" (list success fail global-failure))))

(run-tests add-success add-failure add-header final-result)

`)
