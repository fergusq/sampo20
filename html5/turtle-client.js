var canvas_width = 640;
var canvas_height = 400;

class Point {
	constructor(x, y){
		this.x = x;
		this.y = y;
	}
}

class Chain {
	constructor(start, end, color){
		this.start = start;
		this.end = end;
		this.color = color;
	}
}

var c = null;
var positions = [];
var chains = [];
var texts = [];
var pen_is_down = false;
var is_drawing = false;
var turtle_visible = false;
var is_started = false;
var direction = 0;
var x_factor = 1;
var y_factor = 1;

function get_x_pos(i) {
	return positions[i].x;
}

function get_y_pos(i) {
	return positions[i].y;
}

function get_chain_qty() {
	return chains.length; 
}

function get_chain_end(i) {
	return chains[i].end;
}

function get_chain_start(i) {
	return chains[i].start;
}

function get_chain_color(i) {
	return chains[i].color;
}

function get_direction() {
	if(Number.isInteger(direction)){
		return direction.toPrecision(4);
	}else{
		return direction.toPrecision();
	}
}

function turtle_x_pos(){
	return positions[positions.length - 1].x;
}

function turtle_y_pos(){
	return positions[positions.length - 1].y;
}

function turtle_start(){

	pen_is_down = true;
	is_drawing = true;
	if(!is_started){
		x_factor = 1;
		y_factor = 1;
		direction = 90;
	}
	is_started = true;

	postMessage(['run', `
	c = document.getElementById("canvas");
	c.hidden = false;
	`]);
}

function turtle_clear_position(){
	var pen_is_down_ = pen_is_down;
	pen_is_down = false;
	push_stroke(new Point(canvas_width / 2, canvas_height / 2));
	pen_is_down = pen_is_down_;
}

function turtle_clear_direction(){
	direction = 90;
}

function turtle_clear_drawing(){
	var current = new Point(canvas_width / 2, canvas_height / 2);
	if(positions.length > 0){
		current = positions[positions.length-1];
	}
	positions = [];
	chains = [];
	texts = [];
	push_stroke(current);
}

function set_factors(x, y) {
	x_factor = x;
	y_factor = y;
}

function allocate_new_chain(){
	chains.push(new Chain(positions.length-2, positions.length, is_drawing));
}

function continue_chain(){
	chains[chains.length-1].end++;
}

function push_stroke(position){

	// A new stroke is always allocated

	positions.push(position);

	// Logic on updating chains:
	// - If pen is up, do nothing
	// - If the chain_qty == 0
	// allocate a new chain
	// - If the chain_qty > 0
	//   - The color is the same
	//       - If we have an existing chain, continue it
	//       - If we don't have an existing chain, allocate a new one
	//   - The color is different
	//       - Allocate a new chain
	
	if(pen_is_down){
		if(chains.length == 0){
			allocate_new_chain();
		}else if(chains[chains.length-1].color == is_drawing){
			if(chains[chains.length-1].end == positions.length-1){
				continue_chain();
			}else{
				allocate_new_chain();
			}
		}else{
			allocate_new_chain();
		}
	}
}

function move_to(position){

	var prev = new Point(0, 0);
	if(positions.length > 0){
		prev.x = positions[positions.length-1].x;
		prev.y = positions[positions.length-1].y;
	}
	var delta = new Point(position.x - prev.x, position.y - prev.y);
	if(pen_is_down) {
		while((prev.x + delta.x < 0 || prev.x + delta.x >= canvas_width || prev.y + delta.y < 0 || prev.y + delta.y >= canvas_height)){
			var k_top = (canvas_height - prev.y) / (delta.y);
			var k_bottom = (0 - prev.y) / (delta.y);
			var k_right = (canvas_width - prev.x) / (delta.x);
			var k_left = (0 - prev.x) / (delta.x);
			if(delta.y == 0 || Math.max(k_top, k_bottom) >= Math.max(k_left, k_right)){
				if(delta.x < 0){
					push_stroke(new Point(0, k_left*delta.y + prev.y));
					penup();
					push_stroke(new Point(canvas_width, k_left*delta.y + prev.y));
					pendown();
					delta.x += prev.x;
					var delta_y_copy = delta.y;
					delta.y -= k_left*delta.y;
					prev.x = canvas_width;
					prev.y = k_left*delta_y_copy + prev.y;
				}else{
					push_stroke(new Point(canvas_width, k_right*delta.y + prev.y));
					penup();
					push_stroke(new Point(0, k_right*delta.y + prev.y));
					pendown();
					delta.x -= canvas_width - prev.x;
					var delta_y_copy = delta.y;
					delta.y -= k_right*delta.y;
					prev.x = 0;
					prev.y = k_right*delta_y_copy + prev.y;
				}
			}else{
				if(delta.y < 0){
					push_stroke(new Point(k_bottom*delta.x + prev.x, 0));
					penup();
					push_stroke(new Point(k_bottom*delta.x + prev.x, canvas_height));
					pendown();
					delta.y += prev.y;
					var delta_x_copy = delta.x;
					delta.x -= k_bottom*delta.x;
					prev.y = canvas_height;
					prev.x = k_bottom*delta_x_copy + prev.x;
				}else{
					push_stroke(new Point(k_top*delta.x + prev.x, canvas_height));
					penup();
					push_stroke(new Point(k_top*delta.x + prev.x, 0));
					pendown();
					delta.y -= canvas_height - prev.y
					var delta_x_copy = delta.x;
					delta.x -= k_top*delta.x;
					prev.y = 0;
					prev.x = k_top*delta_x_copy + prev.x;
				}
			}

		}
	}
	if(Math.abs(delta.x) > 0.001 || Math.abs(delta.y) > 0.001) {
		push_stroke(new Point((prev.x + delta.x) % canvas_width, (prev.y + delta.y) % canvas_height));
	}
}

function forwards(distance){
	var delta = new Point(Math.cos(direction * Math.PI / 180) * x_factor * distance, Math.sin(direction * Math.PI / 180) * y_factor * distance);
	move_to(new Point(delta.x + positions[positions.length - 1].x, delta.y + positions[positions.length - 1].y));
}

function alter_direction(amount){
	direction += amount;
	direction = direction % 360;
	if(direction < 0) direction += 360;
}

function set_direction(d){
	direction = d;
	direction = direction % 360;
	if(direction < 0) direction += 360;
}

function turtle_home(){

}

function turtle_update_render(){
	postMessage(["update render", {"positions": positions, "chains": chains, "pen_is_down": pen_is_down, "direction": direction, "turtle_visible": turtle_visible, "texts": texts}]);
}

function penup(){
	pen_is_down = false;
}

function pendown(){
	pen_is_down = true;
}

function draw(){
	is_drawing = true;
}

function erase(){
	is_drawing = false;
}

function show_turtle(){
	turtle_visible = true;
}

function hide_turtle(){
	turtle_visible = false;
}

function draw_segment(point1, point2) {
	var prev = new Point(0, 0);
	if(positions.length > 0){
		prev.x = positions[positions.length-1].x;
		prev.y = positions[positions.length-1].y;
	}
	var pen_is_down_prev = pen_is_down;
	penup();
	move_to(point1);
	pendown();
	move_to(point2);
	penup();
	move_to(prev);
	pen_is_down = pen_is_down_prev;
}

function draw_text(point, text) {
	texts.push({
		content: text,
		x: point.x,
		y: point.y,
	});
}