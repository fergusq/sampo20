# Sampo 20

Sampo 20 on yritys elvyttää 80-luvun suomalainen ohjelmointikieli Sampo. [Täältä](https://fergusq.gitlab.io/sampo20/) voit kokeilla Sampoa suoraan selaimessa. Jos tietokoneellasi on Linux (testattu Ubuntulla ja Fedoralla), voit ladata Sampon komentoriviversion [täältä](https://gitlab.com/fergusq/sampo20/-/jobs/artifacts/master/download?job=app-image). Lisätietoa projektista sekä teknisen määrittelyn (mukaan lukien yksittäisten komentojen käyttöohjeet) löydät [verkkosivuna](https://fergusq.gitlab.io/sampo20/documentation/) ja [pdf-tiedostona](https://gitlab.com/fergusq/sampo20/-/jobs/artifacts/master/download?job=pdf). Komentoriviversion ja nettiversion kääntämisohjeet ynnä komentoriviversion suoritusohjeet löydät alta.

## Komentoriviversio (valmiiksi ḱäännetty)

Valmiiksi ladattava komentoriviversio on AppImage-tiedoston sisältävä .zip-arkisto. Mitään erillisiä riippuvuuksia ei tarvitse asentaa. Pura .zip arkisto distribuutiosi työkalulla. Tee sitten Sampo-tiedostosta suoritettava:

```
chmod +x polku/Sampo-appimageen
```

Suorita sitten .AppImage-tiedosto komentoriviltä.


### Komentoriviversio (kääntäminen)

### Riippuvuudet

Ohjelma sisältää C-kielellä kirjoitetun komponentin, jonka kääntäminen vaatii `gcc`n, `gnumake`n, `pkg-configin` ja `SDL`n sekä `SDL_ttf`n. Dokumentaation kääntämistä varten vaaditaan `pandoc` ja `wkhtmltopdf`. Muutaman ulkoisen työkalun lataamiseksi vaaditaan lisäksi `git`. Lisäksi Sampo 20 riippuu suoritusaikaisesti chibi-tulkista, jonka se osaa ladata ja kääntää.

#### Ubuntu

Ubuntulla riippuvuudet asentuvat komennolla:

```
sudo apt install gcc make pkg-config libsdl2-dev libsdl2-ttf-dev git pandoc wkhtmltopdf
```

#### Fedora

Fedoralla riippuvuudet asentuvat komennolla:

```
sudo dnf install SDL2 SDL2_ttf SDL2-devel SDL2_ttf-devel git pandoc wkhtmltopdf
```

#### NixOS

NixOS:llä kaikki tarpeelliset riippuvuudet on merkitty tämän koodiarkiston shell.nix-tiedostoon. Riittää siis ajaa

```
nix-shell
```

### Kääntäminen ja suoritus

Sampo 20 suorittuu komennolla

```
make run
```

Tämä kääntää kaiken vaadittavan ensimmäisellä suorituskerralla.

### Testit

Sampo 20 pitää sisällään testipatterin, joka suorittuu komennolla

```
make test
```

### AppImage

AppImage-tiedoston voit kääntää myös itse. Tällöin tarvitset yllä mainittujen riippuvuuksien lisäksi paketit `wget` ja `fuse`. `chibi-scheme`n lähdekoodi ladataan ja se käännetään osaksi AppImagea. Tarvittava komento on

```
make appimage
```

## Nettiversio

Nämä ohjeet ovat tarpeellisia vain, jos tahdot kääntää nettiversion omalle palvelimellesi. Nettiversioon pääsee käsiksi suoraan [täältä](https://fergusq.gitlab.io/sampo20/). 
Näiden ohjeiden seuraaminen tuottaa staattisen kansion `html5-release`, joka sisältää kaiken tarvittavan. Sen tiedostojen avaaminen suoraan selaimella ei kuitenkaan onnistu, sillä selaimet tuntuvat hämmentyvän wasm-tiedostojen mime-tyypeistä. Kansio pitää siis tarjota jollain pätevällä palvelimella. (Paikalliseen testailuun helpoimmaksi koettu on ajaa `python -m http.server` kansiossa `html5-release`)

### Riippuvuudet

Nettiversio riippuu käännösaikaisesti samoista paketeista kuin työpöytäversio, pois lukien `SDL` ja `SDL_ttf`. Se lataa itse `emscripten`-työkalun sampo20-koodiarkiston sisälle kääntääkseen chibin (jonka lähdekoodin se myös lataa) nettitekniikaksi.


### Kääntäminen

Tämä onnistuu komennolla

```
make js
```

### Testit

Tämä onnistuu komennolla

```
make js-test
```

## Muista make-komennoista

`Makefile`-tiedostossa on myös komento `make clean`, joka hävittää kääntämisen tulokset. `documentation`-kansiossa on oma makefilensä, ja koodiarkiston juuritasolla olevan makefilen komennot `make documentation/web`, `make documentation/pdf` ja `make documentation/blk` koostavat vastaavat dokumentaatiokokonaisuudet `documentation`-kansioon.

## Tekijänoikeuksista

Kirjoittamamme koodi julkaistaan GNU GPLv3 -lisenssillä. Lisäksi Makefilemme lataa verkosta seuraavat kokonaisuudet:

| Koodi | Tarkoitus | Lisenssi | Projektin kotisivu |
|-------|-----------|----------|--------------------|
|chibi-scheme|tulkki, jonka päällä SAMPO20 suorittuu|[Omansa](https://github.com/ashinn/chibi-scheme/blob/master/COPYING)|[https://github.com/ashinn/chibi-scheme](https://github.com/ashinn/chibi-scheme)|
|emscripten|kääntäjä, joka kääntää chibi-tulkin javscriptiksi|[MIT ja University of Illinois/NCSA Open Source License](https://emscripten.org/docs/introducing_emscripten/emscripten_license.html)|[https://emscripten.org/index.html](https://emscripten.org/index.html)|
|linuxdeploy|työkalu, joka helpottaa AppImage-tiedoston koostamista|[MIT](https://github.com/linuxdeploy/linuxdeploy/blob/master/LICENSE.txt)|[https://github.com/linuxdeploy/linuxdeploy](https://github.com/linuxdeploy/linuxdeploy)|
|Retro|CSS-tyyli dokumentaatiosivulle|[MIT](https://github.com/markdowncss/retro/blob/master/LICENSE)|[https://github.com/markdowncss/retro](https://github.com/markdowncss/retro)|

Olemme saaneet tähän projektiin sekä julkaisuluvan, että käytännön apua Heikki Putkoselta. Kiitokset hänelle. Fontti poimittu täältä https://int10h.org/oldschool-pc-fonts/
