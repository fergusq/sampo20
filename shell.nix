{
  run ? "bash"
}:
with import <nixpkgs> {}; 
(buildFHSUserEnv {
  name = "sampo-shell";
  targetPkgs = pkgs: [ 
     chibi
     gnumake
     gcc
     git
     pkg-config
     glibc.static
     zlib.out
     SDL2.dev
     SDL2_ttf
     pandoc
     wkhtmltopdf
     python3
  ];
  profile="export PATH=${python3}/bin:$PATH";
  runScript = "${run}";
}).env
