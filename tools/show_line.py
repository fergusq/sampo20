import argparse

def main():
    parser = argparse.ArgumentParser(description="Displays line that contains the place referenced by scheme interpreter")
    parser.add_argument("file")
    parser.add_argument("char", type=int)
    args = parser.parse_args()

    with open(args.file, "r") as f:
        i = 0
        n = 1
        for line in f:
            if i+len(line) > args.char:
                line_number = str(n) + ": "
                print(line_number + line.rstrip() + "\n" + " "*(args.char - i + len(line_number)) + "^")
                break

            i += len(line)
            n += 1

if __name__ == "__main__":
    main()