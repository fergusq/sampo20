import argparse
import re

def read_lines(filename):
    lines = []
    with open(filename, "r") as f:
        for line in f:
            lines.append(line.strip("\n\r"))
    
    return lines

def write_lines(lines, filename):
    with open(filename, "w") as f:
        for line in lines:
            print(line, file=f)

def print_lines(lines):
    for line in lines:
        print(line)

SPECIAL_FORMS = ["begin", "lambda", "define", "define-syntax", "syntax-rules", "with-output-to-string", "with-input-from-string"]
SPLIT_RE = re.compile(r"(?<!\\)((?=[()[\]\"])|(?<=[()[\]\"])| )")
PAREN_OPEN_RE = re.compile(r"[([]")
PAREN_CLOSE_RE = re.compile(r"[)\]]")
QUOTE_RE = re.compile(r"[\"]")
COMMENT_RE = re.compile(r";")

def format_lines(lines):
    formatted_lines = []
    indent_stack = [0]
    function_stack = []
    quote = False
    for line in lines:
        if line.strip() == "":
            formatted_lines.append("")
            continue
        
        tokens = [t for t in SPLIT_RE.split(line) if t]
        x = 0
        i = 0
        for i, token in enumerate(tokens):
            if token.strip() == "":
                x += len(token)
            else:
                break
        
        tokens = tokens[i:]
        if not quote:
            x = indent_stack[-1]
        
        formatted_line = " " * x
        comment = False
        for token, next_token in zip(tokens, tokens[1:]+[None]):     
            x += len(token)
            formatted_line += token

            if comment:
                pass
            elif QUOTE_RE.fullmatch(token):
                quote = not quote
            elif not quote:
                if COMMENT_RE.fullmatch(token):
                    comment = True
                elif PAREN_OPEN_RE.fullmatch(token):
                    if next_token in SPECIAL_FORMS:
                        indent_stack.append(x + 2)
                    else:
                        indent_stack.append(x)
                    
                    function_stack.append(next_token in SPECIAL_FORMS)
                elif token.strip() == "" and len(indent_stack) > 1:
                    if not function_stack[-1]:
                        indent_stack[-1] = x
                        function_stack[-1] = True
                elif PAREN_CLOSE_RE.fullmatch(token):
                    indent_stack.pop()
        
        formatted_lines.append(formatted_line)
    
    return formatted_lines

def main():
    parser = argparse.ArgumentParser(description="Format Scheme code")
    parser.add_argument("-i", "--in_place", action="store_true", help="format file in place")
    parser.add_argument("filename", help="file to be formatted")
    args = parser.parse_args()

    lines = read_lines(args.filename)
    lines = format_lines(lines)
    if args.in_place:
        write_lines(lines, args.filename)
    else:
        print_lines(lines)

if __name__ == "__main__":
    main()