# Sampo 20 -määrittely

SAMPO-ohjelmointikielen elvytetyn version määrittely. Verkkoversiota pääset lukemaan [tästä](https://fsmnarmosta.gitlab.io/sampo-2.0-specification/) ja ajantasaisen pdf-version voit ladata [tästä](https://gitlab.com/fsmnarmosta/sampo-2.0-specification/-/jobs/artifacts/master/download?job=build-pdf).

Itse teksti elää `markdown`-kansiossa pienempiin `.md` -tiedostoihin jaettuna. Makefile määrittelee komennot `build-web` ja `build-pdf`, jotka nimiensä mukaan tuottavat `.html` ja `.pdf` -tiedostot. Näiden ajamiseen tarvittavat työkalut ovat `make`, `pandoc` ja `wkhtmltopdf`. Ne asentuvat myös koodiarkiston `shell.nix` -tiedostolla. Arkiston `.gitlab-ci.yml`-tiedosto määrittelee pipelinet, jotka tuottavat muokattaessa `.pdf`-tiedoston ja päivittävät nettisivut.
