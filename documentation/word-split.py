import sys
import re
import subprocess

# Halutaan ensimmäinen sana; selitä on oltava muotoa
# SELITÄ ASEMA: tulos, joka sisältää sekä ASEMA NÄYTÄ, että ASEMA PIILOTA

def sanitize_name(name):
    full_command = name.split("`")[0]

    # Loikkasanat

    if re.match("[a-zåäö]+ --", full_command):
        return full_command.split(" ")[0].upper()

    # Iljettävät tapaukset

    try:
        return {
                "x == y":"==",
                "x <> y":"<>",
                "x > y":">",
                "x < y":"<",
                "x >= y":">=",
                "x <= y":"<=",
                "x ? -- x":"?",
                "x . --":".",
                "a b =$ t":"=$",               
                "( x *Y ) PÄÄ -- x":"PÄÄ",
                "( *X ) ( *Y ) PÄÄKSI -- ( *Y *X ), ( *X ) y PÄÄKSI -- ( y *X )":"PÄÄKSI",
                "( *X y ) PERÄ -- y":"PERÄ",
                "( *X ) ( *Y ) PERÄKSI -- ( *X *Y ), ( *X ) y PERÄKSI -- ( *X y )":"PERÄKSI",
                "( x *Y ) HÄNTÄ -- ( *Y )":"HÄNTÄ",
                "( *X y ) ALKUPÄÄ -- ( *X )":"ALKUPÄÄ",
                "( *X ) TEE --":"TEE",
                "( *X )":"*X",
                "+ -- s":"+",
                "- -- s":"-",
                "* -- s":"*",
                "/ -- s":"JAKO",
                "^ -- s":"^",
                "MOD -- s":"MOD",
                }[full_command]
    except:
        pass

    # Tolkun tapaukset

    return re.search("([#A-ZÅÄÖ\"\*_]+\S* ?)(.*([#A-ZÅÄÖ\"\*_]+\S* ?))*", full_command).group(0).split(" ")[0]

def files():
    for name in sys.argv[1:]:
        with open(name) as f:
            contents = "\n"+f.read()
            for paragraph in contents.split("\n#"):
                if not paragraph.startswith("### `"):
                    continue
                yield (sanitize_name(paragraph.split("### `")[1]), "#" + paragraph.strip("\n"))

result = {}

for command, paragraph in files():
    if command in result.keys():
        result[command] += "\n\n" + paragraph
    else:
        result[command] = paragraph

for key in result:
    with open(f"blk/individual_md/{key}.md", "w") as f:
        f.write(result[key])
    subprocess.run(["python3", "blkify.py", f"blk/individual_md/{key}.md", f"blk/individual_blk/SELITÄ_{key}.BLK"])

