### Tyypit

Sampo 20 -kielessä jokaisella pinossa olevalla arvolla on tyyppi. Alla on lueteltu tunnetut tyypit sekä esimerkkejä siitä, miten kyseisen tyyppisiä arvoja voi luoda.

#### Rajoittamaton etumerkillinen kokonaisluku

```
123 1267650600228229401496703205376  ok
PINO NÄYTÄ

0
123
1267650600228229401496703205376
Pinon pohja
```


#### 32-bittinen liukuluku

Kyseessä on etumerkillinen liukuluku. Sampo 20:ssä desimaalierottimeksi kelpavat `.`, `,` ja `/`.

```
1,5 1/3 -2.5  ok
PINO NÄYTÄ

-2,5
1,2999999
1,5
Pinon pohja
```


#### Lista

Linkattu lista, talletettu muistiin

```
( ) ( kissa )  ok
PINO NÄYTÄ

( kissa )
( )
Pinon pohja
```


#### Alkio

Yksi sana/symboli, talletettu muistiin

```
( kissa ) pää  ok
20 alkioksi  ok
" talo  ok
PINO NÄYTÄ

talo
20
kissa
Pinon pohja
```


#### Fakta

Yksi sana/symboli, talletettu muistiin

```
# KISSA . FAKTA KISSA ok
```


#### Operaattori

Ainakin `+`, `-`, `*`, `/`, `MOD`, `\^`, `==`, `<>`, `<`, `<=`, `>`, `>=` ovat symboleita, jotka eivät kuitenkaan ole alkioita

```
+ - * / ok
PINO NÄYTÄ

/
*
-
+
Pinon pohja
```


#### Muuttujaviittaus

Viittaus johonkin muistissa olevaan muuttujaan.

```
muuttuja x muuttuja koira  ok
x koira  ok
PINO NÄYTÄ

KOIRA
X
Pinon pohja
```


#### Viittaus jonon alkioon

Viittaus johonkin muistissa olevan jonomuuttujan alkioon

```
2 jono sanat  ok
1 sanat  ok
PINO NÄYTÄ

1 SANAT
Pinon pohja
```
