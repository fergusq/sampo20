### Listat

#### `( *X )`

Luo uuden listan ja työntää sen pinoon. Listan sisällä olevia sanoja ei suoriteta eikä niiden tyyppiä tulkita, ainoana poikkeuksena "(" ja ")", joilla määritellään sisäkkäinen lista. Tällä voi olla yllättävät seuraukset:

```
( 1 2 3 ) 3 KUULUU? . 0 Ö Luku 3 ei kuulu listaan
( 1 2 3 ) " 3 KUULUU? . -1 Ö Alkio 3 kuuluu listaan
( 1 ) PÄÄ + 1 = Ö Tuottaa virheen, koska alkiota 1 ja lukua 1 ei voi laskea yhteen.
( ( 1 ) ( 2 ) ) ( 1 ) KUULUU? . -1 Ö Alkion sisältävä lista ( 1 ) kuuluu listaan.
```


#### `x KPL -- #x`

Työntää pinoon `x`:n koon. `x` saa olla lista tai alkio.


#### `( x *Y ) PÄÄ -- x`

Työntää pinoon `x`:n ensimmäisen alkion jos x on lista tai `x`:n ensimmäisen merkin jos se on alkio.


#### `( *X ) ( *Y ) PÄÄKSI -- ( *Y *X ), ( *X ) y PÄÄKSI -- ( y *X )`

Komennon käytös riippuu parametrien tyypeistä.

| `x`n tyyppi | `y`n tyyppi | tulos |
| :---------- | :---------- | :---- |
| lista       | lista       | lista |
| lista       | mikä tahansa| lista |
| alkio       | alkio       | alkio |
| alkio       | mikä tahansa| virhe |

```
" ELÄIN " KISSA PÄÄKSI . ok KISSAELÄIN
" KISSA 1 PÄÄKSI
Virhe sanan PÄÄKSI kutsussa: Ei-alkiota 1 (kokonaisluku) ei voida asettaa alkion KISSA päähän ok
( 1 2 3 ) 0 PÄÄKSI . ok ( 0 1 2 3 )
( 1 2 3 ) ( -1 0 ) PÄÄKSI . ok ( -1 0 1 2 3 )
```


#### `( *X y ) PERÄ -- y`

Työntää pinoon `x`:n viimeisen alkion jos x on lista tai `x`:n viimeisen merkin jos se on alkio.


#### `( *X ) ( *Y ) PERÄÄN -- ( *X *Y ), ( *X ) y PERÄÄN -- ( *X y )`


Komennon käytös riippuu parametrien tyypeistä.

| `x`n tyyppi | `y`n tyyppi | tulos |
| :---------- | :---------- | :---- |
| lista       | lista       | lista |
| lista       | mikä tahansa| lista |
| alkio       | alkio       | alkio |
| alkio       | mikä tahansa| virhe |

```
" ELÄIN " KISSA PERÄÄN. ok ELÄINKISSA
" KISSA 1 PERÄÄN
Virhe sanan PERÄÄN kutsussa: Ei-alkiota 1 (kokonaisluku) ei voida asettaa alkion KISSA perään ok
( 1 2 3 ) 0 PERÄÄN . ok ( 1 2 3 0 )
( 1 2 3 ) ( -1 0 ) PERÄÄN . ok ( 1 2 3 -1 0 )
```


#### `( x *Y ) HÄNTÄ -- ( *Y )`

Mikäli `x` on lista, työntää pinoon listan muista paitsi ensimmäisestä alkiosta. Mikäli `x` on alkio, työntää työntää pinoon alkion, jossa on kaikki paitsi `x`n ensimmäinen merkki.


#### `( *X y ) ALKUPÄÄ -- ( *X )`

Mikäli `x` on lista, työntää pinoon listan muista paitsi viimeisestä alkiosta. Mikäli `x` on alkio, työntää työntää pinoon alkion, jossa on kaikki paitsi `x`n viimeinen merkki.


#### `x y LIITÄ -- ( x y )`

Tekee kahdesta arvosta listan.


#### `x y KUULUU? -- t`

Työntää pinoon `-1`, jos `y` on listan `x` jäsen, muulloin työntää pinoon `0`. Listojen sisäisten arvojen tila voi välillä olla yllättävä.


#### `x y SAMAT? -- t`

Työntää pinoon `-1`, jos lista `x` on yhdenmukainen listan `y` kanssa, muulloin työntää pinoon `0`.


#### `x KOPIO -- x'`

Sampo 20:ssä tämä on synonyymi sanalle `TUPLAA`, joka on suositeltu muoto uusissa ohjelmissa. SAMPO 2.0:ssa se työntää pinoon kopion `x`:stä, joka on eri muistiosoitteessa. SAMPO 2.0:ssä on kuitenkin roskienkeruu, joten tällaisia muistinhallintasanoja ei tarvita. `KOPIO` on olemassa puhtaasti takaisinyhteensopivuussyistä.


#### `x TUHOA --`

Sampo 20:ssä tämä on synonyymi sanalle `POISTA`, joka on suositeltu muoto uusissa ohjelmissa. SAMPO 2.0:ssa se tuhoaa listan `x` ja vapauttaa sen muistin. Sampo 20:ssä on kuitenkin roskienkeruu, joten tällaisia muistinhallintasanoja ei tarvita. `TUHOA` on olemassa puhtaasti takaisinyhteensopivuussyistä.


#### `" s -- a`

Luo alkion sanasta `s`.


#### `x ALKIO? -- t`

Työtää pinoon joko `-1` tai `0` riippuen siitä, onko `x` alkio.


#### `# s -- f`

Luo faktan sanasta `s`. Fakta käyttäytyy muuten kuin alkio, mutta sen merkkijonoesitys alkaa sanalla `FAKTA` ja sana `ALKIO?` palauttaa siitä epätoden arvon.

```
# KISSA . FAKTA KISSA ok
# KOIRA ALKIO? . 0 ok
```


#### `l INT n`

Pyöristää luvun `l` suurimmaksi kokonaisluvuksi `n`, jolle pätee `n &le; l`.


#### `l ALKIOKSI -- a`

Muuttaa luvun `l` alkioksi.


#### `l MERKIKSI -- a`

Jos `l` kuvaa ASCII-merkin, niin `MERKIKSI` tulkitsee sen kyseistä merkkiä kuvaavaksi alkioksi. Jos `l` kuvaa muun utf-8 -merkin, niin Sampo 20:n `MERKIKSI`-toteutus tulkitsee sen myös kyseistä merkkiä kuvaavaksi alkioksi.

SAMPO 2.0 tuki 7-bittistä asciita niin, että ääkköset osuivat lukuväleille 91-93 ja 123-125. Näin ollen näissä tapauksissa SAMPO 2.0 ja Sampo 20 käyttäytyvät eri tavalla, muuten niiden kuuluisi olla vastaavat.

#### `a LUVUKSI -- l`

Jäsentää alkion `a` luvuksi.


#### `a ASCIIKSI -- l`

Muuttaa alkion `a` sen ensimmäistä merkkiä vastaavaksi ASCII-koodiksi, mikäli ensimmäinen merkki kuuluu ASCIIhin. Muussa tapauksessa palautetaan utf-8 -koodi. SAMPO 2.0 tuki 7-bittistä asciita niin, että ääkköset osuivat lukuväleille 91-93 ja 123-125. Näin ollen näissä tapauksissa SAMPO 2.0 ja Sampo 20 käyttäytyvät eri tavalla.

#### `x LISTA? -- t`

Työtää pinoon joko `-1` tai `0` riippuen siitä, onko `x` lista.


#### `a LISTAKSI -- ( a )`

Muuttaa alkion `a` listaksi.


#### `n SATTUMA m`

Tuottaa pinon päälle satunnaisluvun `{0, ..., n}`. Satunnaislukuja voi sekoittaa vaihtamalla muuttujan `SIEMEN` arvoa. Sampo 20 ei takaa, että samalla siemenarvolla saataisiin samat satunnaisluvut kaikilla alustoilla.


#### `( *X ) TEE --`

Jäsentää `*X`:n Sampo 20 -ohjelmana ja suorittaa sen.

