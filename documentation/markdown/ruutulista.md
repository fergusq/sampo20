|Kappale                 |Ruutu|
|:----------------------:|:---:|
|Alkupuhe                |1    |
|Tulkista ja dokumentista|7    |
|Ydinkomentoja           |9    |
|Konnakomennot           |46   |
|Listat                  |65   |
|Tiedostonhallinta       |77   |
|Tyypit                  |89   |
|BLK- ja TKA-tiedostot   |95   |
|Varatut sanat           |97   |
|Liitteet                |105  |
|Viitteet                |106  |
