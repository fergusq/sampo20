### Varatut sanat

SAMPO 2.0 toteutti komentoja, joita Sampo 20 ei toteuta. Nämä ovat kuitenkin __varattuja sanoja__, jotka saatetaan myöhemmin toteuttaa.

#### `PAPERILLE --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tämä siirsi tulosteen ruudulta paperille.


#### `RUUDULLE --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tämä siirsi tulosteen paperilta ruudulle.


#### `MUISTA --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tämä käynnisti pikalaajennuksen (vie kirjoitetut komentorivit näyttöruudun lisäksi sampo.blk:n ruudulle 0, joka on talletettavissa myöhemmin laajennusruuduksi).


#### `ÄLÄMUISTA --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tämä lopetti pikalaajennustilan.


#### `LAAJENNUS x`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa talletti Sampon tilanteen tiedostoksi nimellä `x`. Kaikki uudet LUO-komennolla aikaansaadut Sampo-sanat tallettuvat uuteen versioon Sampon varsinaisiksi sanoiksi. Esim. `LAAJENNUS PROLOG.COM`


#### `Ös`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa merkitsi kokonaisen ruudun kommentiksi.

#### `hal --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 2, `Hallintasanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `syö --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 3, `Syöttö ja tulostussanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `gra --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 5, `Grafiikkasanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `laa --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 6, `Laajennettavuussanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `ohj --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 7-8, `Ohjausrakennesanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `pin --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 9, `Pinosanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `las --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 10, `Laskentasanat`. SAMPO 2.0:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `muu --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 11, `Muuttuja- ja jonosanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `lst --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 12, `Listasanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.


#### `tie --`

Varattu sana, jota Sampo 20 ei toteuta. SAMPO 2.0:ssa tulostaa `AUTA.BLK`-dokumentaation ruudun 15, `Tiedosto- ja tiedonhallintasanat`. Sampo 20:ssä dokumentaation rakenne on hieman erilainen, joten nämä on jätetty toteuttamatta.
