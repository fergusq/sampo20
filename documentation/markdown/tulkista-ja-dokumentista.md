## Sampo 20-tulkki

Sampo 20 on FORTHin sukuinen kieli. Tämä tarkoittaa, että Sampo 20:en komennot suoritetaan vasemmalta oikealle niin, että käskyjen käsittelemät arvot käyvät pinon läpi. Käskysanoista käytetään nykyajan valtavirtakieliä käyttäneelle ohjelmoijalle hieman erikoista merkintää. Esimerkiksi konnagrafiikkatilassa konnan pisteeseen `(x, y)` siirtävä käsky `x y PAIKKA` lukee pinon päältä kaksi lukua.

### Tietoa tyyppivihjeistä

Tässä määrityksessä käytetään komentojen yhteydessä tyyppivihjeitä.


| Vihje    | Kuvaus                                                  |
| :------: | :-----------------------------------------------------: |
| `a`      | alkio                                                   |
| `f`      | fakta                                                   |
| `l`      | kokonaisluku tai liukuluku                              |
| `n`, `m` | kokonaisluku                                            |
| `t`      | totuusarvo (kokonaisluku `-1` (tosi) tai `0` (epätosi)) |
| `v`      | muuttujaviittaus                                        |
| `s`      | sana                                                    |
| `( *X )` | `*X` viittaa listan sisällä oleviin alkioihin           |

