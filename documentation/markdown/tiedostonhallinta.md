### Tiedostonhallinta

#### `DIR --`

Tulostaa nykyisen hakemiston sisällön.


#### `A: --`

Sampo 20:ssä ei tee mitään, säilytetty takaisinyhteensopivuussyistä. SAMPO 2.0ssa siirsi nykyisen tarkasteltavan hakemiston dosin `A:`-levylle.


#### `B: --`

SAMPO 2.0:ssä ei tee mitään, säilytetty takaisinyhteensopivuussyistä. SAMPO 2.0:ssa siirsi nykyisen tarkasteltavan hakemiston dosin `B:`-levylle.


#### `C: --`

Sampo 20:ssä vaihtaa nykyiseksi hakemistoksi Sampon asennuskansion. SAMPO 2.0:ssa siirsi nykyisen tarkasteltavan hakemiston dosin `C:`-levylle.


#### `AVAA s --`

Avaa ruututiedoston (BLK-tiedoston) tai tietokantatiedoston (TKA-tiedoston) `s`. Kirjoittaa tiedostojärjestelmään edellisen avoimen tiedoston (tai tietokannan) ja lukee sitten uuden tiedoston sisällön ruutuina.

```
AVAA PROLOG.BLK
```


#### `TIETOKANTA --`

Tulkitsee avoimen tiedoston tietokantana.

```
AVAA PAIKAT.TKA TIETOKANTA
```


#### `SULJE --`

Sulkee avattuna olevan ruutu- tai tietokantatiedoston. Kirjoittaa sen muistista tiedostojärjestelmään.


#### `n LUE --`

Lukee `n`:nnen ruudun tiedostosta SAMPO.BLK (asettaa sen nykyiseksi ruuduksi ja tulostaa sen). Ei lue tiedostojärjestelmästä vaan sampon välimuistissa olevasta esityksestä.

```
2 LUE
```


#### `n *LUE --`

Lukee `n`:nnen ruudun `AVAA`-komennolla avatusta tiedostosta (asettaa sen nykyiseksi ruuduksi ja tulostaa sen). Ei lue tiedostojärjestelmästä vaan sampon välimuistissa olevasta esityksestä.

```
AVAA PROLOG.BLK
2 *LUE
```


#### `SUORITA --`

Jäsentää nykyisen ruudun Sampo-ohjelmana ja suorittaa sen.


#### `KORJAA --`

Avaa nykyisen ruudun tekstieditorissa, on toteutusriippuvaista mikä tekstieditori on kyseessä. Komentorivimuotoinen Sampo 20 suorittaa Linux-alustalla `EDITOR`-ympäristömuuttujassa nimetyn editorin antaen parametriksi väliaikaistiedoston, johon ruutu on kirjoitettu (utf-8 -muodossa, rivinvaihdoilla). Sampo-tulkki jää odottamaan, kunnes editori on suljettu ja lukee sitten muokatun version samasta tiedostosta. Selainmuotoinen sampo avaa ruudun samalla sivulla olevaan tekstikenttään.


#### `n TALLETA --`

Tallettaa nykyisen ruudun `n`:nneksi ruuduksi tiedostoon SAMPO.BLK. Muutos tehdään sampon välimuistiin eikä tiedostojärjestelmään. 

```
Ö Kopioi SAMPO.BLK:n toinen ruutu kolmanneksi.
2 LUE
3 TALLETA
```


#### `n *TALLETA --`

Tallettaa nykyisen ruudun `n`:nneksi ruuduksi `AVAA`-komennolla avattuun tiedostoon. Muutos tehdään sampon välimuistiin eikä tiedostojärjestelmään.

```
Ö Kopioi SAMPO.BLK:n toinen ruutu KISSA.BLK:n kolmanneksi.
AVAA KISSA.BLK
2 LUE
3 *TALLETA
```


#### `x n m VIE --`

Tallettaa `AVAA`-komennolla avattuun tiedostoon ruutuun `n` riville `m` arvon (luvun, alkion tai listan) `x`. Muutos tehdään sampon välimuistiin eikä tiedostojärjestelmään.

```
AVAA KISSAT.BLK
( tiikeri ) 2 3 VIE
```


#### `n m TUO -- x`

Lisää pinoon `AVAA`-komennolla avatun tiedoston ruudulta `n` riviltä `m` luetun arvon (luvun, alkion tai listan). Ei lue tiedostojärjestelmästä vaan sampon välimuistista.

```
AVAA KISSAT.BLK
2 3 TUO .
```


#### `n HÄVITÄ --`

Poistaa avatusta tietokannasta tietueen (ruudun) `n` (Luku `n` vastaa `POIMI`-sanan numerointia). Muutos tehdään sampon välimuistiin eikä tiedostojärjestelmään.


#### `LISÄÄ --`

Lisää avattuun tietokantaan uuden tietueen (ruudun). Kysyy käyttäjältä tietueen tiedot. Muutos tehdään sampon välimuistiin eikä tiedostojärjestelmään.


#### `POIMI --`

Suorittaa jokaiselle tietueelle ensin `SÄÄNTÖ`-muuttujassa olevan ehtolausekkeen. Ehtolausekkeen koodissa voi käyttää kenttien nimiä komentoina, jotka asettavat pinoon ko. kentän arvon. Jos ehtolause on tosi, suoritetaan `KENTÄT`-muuttujassa oleva koodi. Myös siinä voi käyttää kenttien nimiä. Komento `KAIKKITULOSTA` tulostaa kaikkien kenttien nimet ja arvot. Muuttujan `SÄÄNTÖ` oletusarvo on `0`. Muuttujan `KENTÄT` oletusarvo on `( KAIKKITULOSTA )`.

```
AVAA PAIKAT.TKA TIETOKANTA
( SADEMÄÄRÄ >= 50 ) SÄÄNTÖ LLE
( KAUPUNKI . SADEMÄÄRÄ . ) KENTÄT LLE
POIMI
```


#### `TIEDOSTO s`

Luo `s`-nimisen BLK-tiedoston nykyiseen kansioon. Tiedostossa on oletuksena kymmenen ruutua, mutta ruutumäärään voi vaikuttaa `n RUUTUINEN`-komennolla.


#### `n RUUTUINEN`

Määrittää sen, moniruutuisen tiedoston `TIEDOSTO s` luo. Esimerkiksi `3 RUUTUINEN TIEDOSTO KISSA.BLK` luo kolmiruutuisen tiedoston `KISSA.BLK`.


#### `AUTA --`

Tulostaa `AUTA.BLK`-dokumentaation ruudun 1, joka selittää AUTA-dokumentaation rakennetta ja sitä, miten sen eri ruutuja pystyy lukemaan.

#### `x APU --`

Tulostaa `AUTA.BLK`-dokumentaation ruudun `x`.


#### `ED --`

Loikkaa viimeiseksi näytetyn tiedoston edelliseen ruutuun.


#### `SEU --`

Loikkaa viimeiseksi näytetyn tiedostn seuraavaan ruutuun.


#### `SELITÄ s`

Tulostaa kansiosta `SELITYS` sanaa `s` vastaavan selityksen.


#### `CD s`

Siirtyy hakemistoon `s`.

```
CD /home/user/sampo/
```

