## Liitteet

 

### A: Implementaatiokohtaiset ominaisuudet

Nämä komennot eivät kuulu itse Sampo 20-kieleen. Ne on kuitenkin toteutettu molemmissa Sampo 20-versioissa. Ei ole taattua, että ne löytyvät tulevista versioista tai käyttäytyvät samalla tavalla.


#### `x SCHEMEDISPLAY -- x`

Tulostaa alkion $x$ sisäisen esityksen alla olevassa Scheme-tulkissa.
