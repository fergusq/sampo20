## Ydinkomentoja

### Syöttö ja tulostus tekstitilassa

Sampo 20 -tulkin kanssa vuorovaikutetaan komentoriviltä joko päätteessä tai verkkosivulla. Tietoa saadaan virtaamaan käyttäjän ja Sampon välillä syötteen ja tulosteen avulla.

#### `LUELUKU -- n`

Lukee luvun päätteeltä pinon päähän. Luvun yhteydessä käytettävää ohjetekstiä (oletuksena `LUKU:`) voit muuttaa muttujan `LUKUTEKSTI` arvoa muuttamalla

```
LUELUKU
LUKU: 1337  ok
. 1337  ok
```


#### `UUSIRIVI --`

Tuottaa rivinvaihdon tulostuksessa. SAMPO 2.0 tuki paperille tulostamista, paperille tulostettaessa `UUSIRIVI` tuotti sille rivivaihdon.

```
UUSIRIVI
 ok
```


#### `RIVI --`

SAMPO 2.0:sta peräisin oleva synonyymi sanalle `UUSIRIVI`

```
RIVI
 ok
```


#### `UR --`

SAMPO 2.0:sta peräisin oleva synonyymi sanalle `UUSIRIVI`

```
UR
 ok
```


#### `UUSISIVU --`

Tyhjentään pääteruudun. SAMPO 2.0 tuki paperille tulostamista, paperille tulostettaessa `UUSIRIVU` tuotti sille rivivaihdon.


#### `n TAB --`

Komento `n TAB` siirtää tulostuksen `n`:nteen sarakkeeseen.


#### `LUELISTA -- l`

Lukee listan päätteeltä pinon päähän. Luvun yhteydessä tulostuvaa ohjetekstiä voit muuttaa muuttujaa `LISTATEKSTI` muuttamalla.

```
LUELISTA 
LISTA: ( VIEKAS KETTU PUNATURKKI LAISKAN KOIRAN TAKAA KURKKI ) ok
. ( VIEKAS KETTU PUNATURKKI LAISKAN KOIRAN TAKAA KURKKI ) ok
```


#### `LUEALKIO -- a`

Lukee alkion päätteeltä pinon päähän. Luvun yhteydessä tulostuvaa ohjetekstiä voit muuttaa muuttujaa `ALKIOTEKSTI` muuttamalla.

```
LUEALKIO SANA: kissa  ok
. kissa  ok
```

### Pinot

#### `PINO NÄYTÄ --`

Tuottaa pinon näkyviin. On toteutusriippuvaista, minne pino tulee näkyviin. On toteutusriippuvaista, tulostetaanko standarditulosteeseen mitään ja jos tulostetaan niin tulostetaanko kerran vai toistuvasti (`PINO` on komento, joka työntää pinoon muuttujaviittauksen `PPINO`)

```
1 2 3 4  ok
PINO NÄYTÄ

4
3
2
1
Pinon pohja
```


#### `PINO PIILOTA --`

Poistaa pinon näyttöruudulta.



#### `x TUPLAA -- x x`

Kopioi pinon päällimmäisen alkion pinon päälle.

```
1 2 3  ok
tuplaa  ok
PINO NÄYTÄ

3
3
2
1
Pinon pohja
```


#### `x y VAIHDA -- y x`

Vaihtaa pinon kaksi päällimmäistä alkiota keskenään.

```
1 2 3  ok
tuplaa  ok
PINO NÄYTÄ

2
3
1
Pinon pohja
```


#### `x y z KIERRÄ -- y z x`

Poistaa pinon kolmannen alkion ja lisää sen pinon päälle.

```
1 2 3 4 ok
kierrä ok
PINO NÄYTÄ

2
4
3
1
Pinon pohja
```


#### `x POISTA --`

Poistaa pinon päällimmäisen alkion.

```
1 2 3  ok
POISTA  ok
PINO NÄYTÄ

2
1
Pinon pohja
```


#### `x y YLI -- x y x`

Kopioi pinon päälle päällimmäistä edeltävän alkion.

```
1 2 3  ok
YLI  ok
PINO NÄYTÄ

2
3
2
1
Pinon pohja
```


#### `x ? -- x`

Tulostaa pinon pään

```
1 2 3  ok
? 3  ok
PINO NÄYTÄ

3
2
1
Pinon pohja
```



#### `x . --`

Tulostaa ja tuhoaa pinon pään

```
1 2 3  ok
. 3  ok
PINO NÄYTÄ

2
1
Pinon pohja
```

### Totuusarvot ja ehtolauseet

Sampo 20:n totuusarvot ovat _-1_ (tosi) ja _0_ (epätosi). Totuusarvoja käsitellään ehtolauseilla.

#### `x == y`

Työntää pinon päälle symbolin `==`. Ehtolauseessa se kuvaa yhtäsuuruuden. Käytännön totuusarvon laskeminen tapahtuu `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.

```
1 == 2 = . 0  ok
2 == 1 = . 0  ok
1 == 1 = . -1  ok
LUO F JOS 1 == 2 TOSI? 10 MUUTEN 20 JATKA VALMIS  ok
F . 20  ok
```



#### `x <> y`

Työntää pinon päälle symbolin `<>`. Ehtolauseessa se kuvaa erisuuruuden. Käytännön totuusarvon laskeminen tapahtuu `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.

```
1 <> 2 = . -1  ok
2 <> 1 = . -1  ok
1 <> 1 = . 0  ok
LUO F JOS 1 <> 2 TOSI? 10 MUUTEN 20 JATKA VALMIS  ok
F . 10  ok
```



#### `x > y`

Työntää pinon päälle symbolin `>`. Ehtolauseessa se kuvaa suuremmuuden. Käytännön totuusarvon laskeminen tapahtuu `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.

```
1 < 2 = . -1  ok
2 < 1 = . 0  ok
1 < 1 = . 0  ok
LUO F JOS 1 < 2 TOSI? 10 MUUTEN 20 JATKA VALMIS  ok
F . 10  ok
```



#### `x < y`

Työntää pinon päälle symbolin `<`. Ehtolauseessa se kuvaa pienemmyyden. Käytännön totuusarvon laskeminen tapahtuu `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.

```
1 > 2 = . 0  ok
2 > 1 = . -1  ok
1 > 1 = . 0  ok
LUO F JOS 1 > 2 TOSI? 10 MUUTEN 20 JATKA VALMIS  ok
F . 20  ok
```



#### `x >= y`

Työntää pinon päälle symbolin `>=`. Ehtolauseessa se kuvaa suuremmuuden tai yhtäsuuruuden. Käytännön totuusarvon laskeminen tapahtuu `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.

```
1 >= 2 = . 0  ok
2 >= 1 = . 0  ok
1 >= 1 = . -1  ok
LUO F JOS 1 >= 2 TOSI? 10 MUUTEN 20 JATKA VALMIS  ok
F . 20  ok
```



#### `x <= y`

Työntää pinon päälle symbolin `<=`. Ehtolauseessa se kuvaa pienemmyyden. Käytännön totuusarvon laskeminen tapahtuu `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.

```
1 <= 2 = . -1  ok
2 <= 1 = . 0  ok
1 <= 1 = . -1  ok
LUO F JOS 1 <= 2 TOSI? 10 MUUTEN 20 JATKA VALMIS  ok
F . 10  ok
```



#### `x JA y`

Työntää pinon päälle symbolin `JA`. Ehtolauseessa se kuvaa lauseiden yhdistämisen Boolelaisella JA-operaattorilla. Tulokseksi tulee -1, jos molemmat lauseet ovat tosia, muuten 0. Käytännön totuusarvon laskeminen tapahtyy `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.


#### `x TAI y`

Työntää pinon päälle symbolin `TAI`. Ehtolauseessa se kuvaa lauseiden yhdistämisen Boolelaisella TAI-operaattorilla. Tulokseksi tulee -1, jos ainakin toinen lause on tosi, muuten 0. Käytännön totuusarvon laskeminen tapahtyy `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.


#### `EI y`

Työntää pinon päälle nollan ja symbolin `EI`. Ehtolauseessa se kuvaa lauseen negaation. Tulokseksi tulee -1, jos kiellettävä lause on tosi ja 1, jos kiellettävä lause on epätosi. Käytännön totuusarvon laskeminen tapahtyy `=`-sanalla. Ohjausrakenteet laskevat totuusarvon automaattisesti auki.

```
>>> PINO NÄYTÄ
Pinon pohja
>>> EI
EI
0
Pinon pohja
>>> -1 
-1
EI
0
Pinon pohja
>>> = . 0
Pinon pohja
```


#### `a b =$ t`

Postfiksivertailu. `a b =$` vastaa vertailua `a == b =`.

### Ohjausrakenteet

Ohjausrakenteiden avulla saadaan käyttöön ehto-, toisto- ja valintarakenteet. Ohjausrakenteissa sovelletaan ehtolauseita.

#### `n KERTAA x VIELÄ?`

> Toistaa toiminnat `x` `n` kertaa. Sana `VIELÄ?` ilmoittaa toimintojen lopun. [...] Tavallisena sanana (ei `LUO`-sanan sisällä) käytettäessä `VIELÄ?`-sana voi puuttua, jolloin rivin loppu ilmoittaa rakenteen lopun.
> 
>   `KERTAA`-rakenteen kierroslaskureina käytetään sanoja `I` (ulompi) ja `J` (sisempi). Näitä voidaan käyttää hyväksi rakenteen sisäisessä laskennassa, sillä näiden avulla viedään pinon päähän ko. sanan arvo.
> 
>   `KERTAA`-sanasta voidaan poistua kesken suorituksen sanan `ULOS` avulla.

[käskysanat](#käskysanat)

```
LUO F 4 KERTAA 3 KERTAA I * J = . VIELÄ? VIELÄ? VALMIS  ok
F 1 2 3 2 4 6 3 6 9 4 8 12  ok
```

Jos `KERTAA`-rakenteita on sisäkkäin yli kaksi, `I` ja `J` viittaavat aina kahteen uloimpaan silmukkaan.

```
2 KERTAA 3 KERTAA 4 KERTAA I . J . 1 1 1 1 1 1 1 1 1 2 1 2 1 2 1 2 1 3 1 3 1 3 1 3 2 1 2 1 2 1 2 1 2 2 2 2 2 2 2 2 2 3 2 3 2 3 2 3 ok
```



#### `TOISTA ehto YHÄ? x ALKUUN`

> Toistorakenne, joka vastaa lausekielten ns. `WHILE-DO` rakennetta. [käskysanat](#käskysanat)

Suorittaa ensin komennot kohdassa `ehto` ja lukee sen jälkeen pinon päältä ehtolauseen. Jos ehto on tosi, suoritetaan _x_ ja palataan alkuun. Muuten poistutaan silmukasta.

```
LUO F 0 TUPLAA TOISTA < 10 YHÄ? ? + 1 = TUPLAA ALKUUN VALMIS  ok
F 0 1 2 3 4 5 6 7 8 9  ok
```



#### `TOISTA ehto LOPETA? x ALKUUN`

> Toistorakenne, joka vastaa lausekielten ns. `REPEAT-UNTIL` rakennetta. [käskysanat](#käskysanat)

Suorittaa ensin komennot kohdassa `ehto` ja lukee sen jälkeen pinon päältä ehtolauseen. Jos ehto on epätosi, suoritetaan _x_ ja palataan alkuun. Muuten poistutaan silmukasta.

```
LUO F 0 TUPLAA TOISTA >= 9 LOPETA? ? + 1 = TUPLAA ALKUUN VALMIS  ok
F 0 1 2 3 4 5 6 7 8 9  ok
```



#### `JOS ehto TOSI? x MUUTEN y JATKA`

> Suorittaa toiminnat `x`, jos ehto on tosi, muuten toiminnot `y`. Tämän jälkeen suoritus jatkuu `JATKA`:n jälkeisestä sanasta. [käskysanat](#käskysanat)

```
LUO PARILLINEN TUPLAA JOS < 2 TOSI? - 1 = MUUTEN - 2 = PARILLINEN JATKA VALMIS  ok
3 PARILLINEN . 0  ok
0 PARILLINEN . -1  ok
128 PARILLINEN . -1  ok
```


#### `JOS ehto VALE? x MUUTEN y JATKA`

Suorittaa toiminnat `x`, jos ehto on epätosi, muuten toiminnot `y`. Tämän jälkeen suoritus jatkuu `JATKA`:n jälkeisestä sanasta.

```
LUO PARILLINEN TUPLAA JOS >= 2 VALE? - 1 = MUUTEN - 2 = PARILLINEN JATKA VALMIS  ok
3 PARILLINEN . 0  ok
0 PARILLINEN . -1  ok
128 PARILLINEN . -1  ok
```


#### `JOS ehto ON arvo1 NIIN x1 LOPPUUN ... ELLEI LOPPU`

> Valintarakenne, jossa annetun ehdon arvojen (`arvo1, ...`) mukaan haaraudutaan toimenpiteisiin `x1, ...` [käskysanat](#käskysanat)

```
LUO R ( Kruuna ) VALMIS  ok
LUO L ( Klaava ) VALMIS  ok
LUO K ( Kolikko jäi kantilleen ) VALMIS  ok
LUO HEITÄ 3 SATTUMA VALMIS  ok
LUO KOLIKKO JOS HEITÄ ON 1 NIIN R LOPPUUN 2 NIIN L LOPPUUN ELLEI K LOPPU VALMIS  ok
KOLIKKO . ( Kolikko jäi kantilleen ) ok
KOLIKKO . ( Kolikko jäi kantilleen ) ok
KOLIKKO . ( Kruuna ) ok
KOLIKKO . ( Klaava ) ok
```

### Laskutoimitukset

Sampo 20 -kielessä laskutoimitukset kuvataan FORTH-kielille hieman epätyypillisellä tavalla. Komennot `+`, `-`, `*`, `/`, `^` ja `MOD` puskevat pinon päälle vastaavan symbolin ja varsinainen laskenta tapahtuu vasta kun komento `=` annetaan. Kaikki nämä toimitukset ovat infiksimuotoisia ja niille on seuraava laskujärjestys:

1. `^`

2. `*`, `/`, `MOD`

3. `+`, `-`

Laskutoimitukset tuottavat kokonaisluvun, mikäli tulos voidaan kuvata sellaisella tarkasti. Mikäli ei, ne tuottavat liukuluvun.

#### `+ -- s`

Puskee pinon päälle symbolin `+`, joka ei kuitenkaan ole alkio.

#### `- -- s`

Puskee pinon päälle symbolin `-`, joka ei kuitenkaan ole alkio.

#### `* -- s`

Puskee pinon päälle symbolin `*`, joka ei kuitenkaan ole alkio.

#### `/ -- s`

Puskee pinon päälle symbolin `/`, joka ei kuitenkaan ole alkio.

#### `^ -- s`

Puskee pinon päälle symbolin `^`, joka ei kuitenkaan ole alkio.

#### `MOD -- s`

Puskee pinon päälle symbolin `MOD`, joka ei kuitenkaan ole alkio.

### Muuttujat

#### `MUUTTUJA s --`

Luo uuden muuttujan _s_. Muuttujan nimi on komento, joka työntää pinoon muuttujaviittauksen. Muuttuja saa oletusarvokseen luvun 0.


#### `n JONO s --`

Luo uuden jonomuuttujan _s_. Muuttujan nimi on komento, joka ottaa luvun ja palauttaa muokattavan viittauksen sitä vastaavaan soluun. Jonot ovat 1-indeksoituja.


#### `x VAKIO s --`

Luo uuden vakion _s_. Muuttujan nimi on komento, joka työntää pinoon arvon _x_.


#### `x v LLE --`

Asettaa arvon $x$ muuttujaviittaukseen $v$.

```
MUUTTUJA KISSA ok
10 KISSA LLE ok
KISSA ARVO . 10 ok
```

Koska jonomuuttujan nimi on komento, joka työntää pinon päälle muokattavan viittauksen, jonomuuttujan solujen arvoja voi asettaa `LLE`-komennolla.

```
10 JONO KISSA ok
" VIIMEINEN 10 KISSA LLE ok
10 KISSA ARVO . VIIMEINEN ok
```


#### `v ARVO -- x`

Lukee arvon muuttujaviittauksesta _v_.


### Sanojen käsittely

#### `LUO x :a :b ... VALMIS`

> Määrittelee uuden sanan nimeltä _x_. Määrittely lopetetaan sanalla "`VALMIS`". Määrittely voi sisältää[^parametrihuomio] parametreja, jotka esitetään `:`llä  alkavina termeinä ennen sanaa _x_.  Parametreja käytetään sanamäärittelyissä esittelymuodon mukaisesti vakioargumenttien sijasta. [käskysanat](#käskysanat)

[^parametrihuomio]: SAMPO 2.0:ssa parametrien määrälle on viiden yläraja. Sampo 20 poistaa tämän rajoituksen.

SAMPO 2.0:ssa parametrien määrälle on viiden yläraja. Sampo 20 poistaa tämän rajoituksen.

```
LUO :N KERTOMA
JOS :N <= 1 TOSI? 1 MUUTEN :N - 1 = KERTOMA * :N = JATKA VALMIS ok
7 KERTOMA . 5040 ok
```

On toteutusriippuvaista, kuinka `LUO` ... `VALMIS` -lohkossa määritellyt muuttujat, jonot ja vakiot käyttäytyvät. SAMPO 2.0:ssa niiden määritteleminen on virhe, Sampo 20 hyväksyy ne ja ne ovat paikallisia määrittelevälle funktiolle.

```
LUO F " KELPO VAKIO X X . VALMIS ok
F KELPO ok
X
Tuntematon komento X. Kutsupino: ()
UNOHDA F ok
LUO F MUUTTUJA X " KELPO X LLE X ARVO . VALMIS ok
F KELPO ok
X
Tuntematon komento X. Kutsupino: ()
UNOHDA F ok
LUO F 10 JONO X " KELPO 5 X LLE 5 X ARVO . VALMIS ok
F KELPO ok
X
Tuntematon komento X. Kutsupino: ()
```

#### `UNOHDA x`

> Unohtaa sanan $x$ ja sen jälkeen määritellyt sanat. [käskysanat](#käskysanat)

```
LUO A 1337 VALMIS ok
LUO B A * 2 = VALMIS ok
UNOHDA A ok
B
Tuntematon komento B!
```

`UNOHDA x` poistaa vain $x$n uusimman määrittelyn jälkeen tehdyt määrittelyt. Jos jokin `UNOHDA` sanan hävittämä sana oli määritelty ennen parametriksi annetun sanan viimeistä määrittelyä, se palautuu aikaisempaan muotoonsa.

On toteutusriippuvaista käytöstä, kuinka sanamäärittelyn sisältä (esim. `LUO F UNOHDA G VALMIS F`) tehtävä kutsu käyttäytyy. Sampo 20 poistaa sanan `G` senhetkisen `F`-kutsun nimiavaruudesta. SAMPO 2.0 ei hyväksy kyseistä koodia. (Sama pätee toistorakenteiden ja haarojen sisällä tapahtuvaan unohtamiseen, sillä niiden esiintyminen sanamäärittelyn ulkopuolella on Sampo 20 -laajennos.)

```
LUO A 10 VALMIS ok
LUO B 20 VALMIS ok
LUO A B VALMIS ok
UNOHDA B ok
A . 10 ok
```

Unohda hävittää sekä komennoilla `MUUTTUJA`, `JONO` ja `VAKIO`, että komennolla `LUO` luotuja sanoja. Jos sana on aluksi ollut muuttuja ja myöhemmin uudelleenmääritelty `LUO` sanalla, se palautuu muuttujaksi ja siihen voi soveltaa esimerkiksi sanoja `LLE` ja `ARVO`. Käänteisessä tapauksessa `UNOHDA`-sanan jälkeen sanoja `LLE` ja `ARVO` ei voida käyttää.

```
MUUTTUJA B ok
3 B LLE ok
LUO X 10 VALMIS ok
LUO B 20 VALMIS ok
UNOHDA B ok
X . 10 ok
B ARVO . 3 ok
```



### Sekalaista

#### `LOPETA --`

Lopettaa Sampo-tulkin suorituksen. Ennen sitä, avoin tiedosto (BLK tai TKA) kirjoitetaan välimuistista tiedostojärjestelmään.


#### `Ö --`

Merkitsee loput rivistä kommentiksi

