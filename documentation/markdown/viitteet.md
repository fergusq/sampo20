## Viitteet

#### [1] [*Sampo - opaskirja*](https://www.cs.helsinki.fi/u/thfr/documents/opaskirja_ocr.pdf), Systiimi Oy, 1987. {#opaskirja}

#### [2] [*Sampo - käskysanat*](https://www.cs.helsinki.fi/u/thfr/documents/k%C3%A4skysanat_ocr.pdf), Systiimi Oy, 1987. {#käskysanat}

#### [3] [*Psykologiasta pohja kehitykselle - Oulussa taotaan kouluille SAMPOA*](https://raw.githubusercontent.com/sampo-lang/sampo/master/Documenrtaatio/Sampo_DOC_Manuaalikopiot/Insino%CC%88o%CC%88riuutiset.pdf), Insinööriuutiset, 24.4.1985 {#insinööriuutiset}
