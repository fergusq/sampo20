### Konnagrafiikkakomennot

#### `ASEMA NÄYTÄ --`

> Tuottaa konnan aseman näkyviin näyttöruudun yläriville. [s. 5, käskysanat](#käskysanat)

Vaikutus näkyy vain `KONNA`-komennolla avatussa ikkunassa. Näkyvät luvut on pyöristetty kokonaisluvuiksi, mutta sisäisesti ne tallennetaan 64-bittisinä liukulukuina. Ajettaessa komennot

<a id="asema_näytä_esimerkki"></a>

```
KONNA
320 100 PAIKKA
45 VASEN
4 KERTAA 50 ETEEN 90 OIKEA 50 ETEEN 90 VASEN VIELÄ?
ASEMA NÄYTÄ
```

Saadaan kuvaa vastaava tulos.

![`ASEMA NÄYTÄ` tuo konnagrafiikkaikkunan vasempaan yläkulmaan tiedon koordinaateista ja kulmasta](kuvat/asema_näytä_esimerkki.png)

#### `ASEMA PIILOTA --`

> Poistaa konnan aseman näkyvistä. [s. 5, käskysanat](#käskysanat)

Komennon vaikutus on käänteinen sanojen `ASEMA NÄYTÄ` vaikutukseen.


#### `YLÖS --`

> Nostaa konnan ylös. [s. 6, käskysanat](#käskysanat)

Kun konnaa tämän jälkeen siirretään `ETEEN` ja `TAAKSE` -komennoilla, konnasta ei jää jälkeä. `KONNA`-komennon avaamassa ikkunassa tämä myös piilottaa konnan sijaintia materialisoivan kolmion.

<a id="ylös_esimerkki"></a>

```
KONNA
18 KERTAA ALAS 10 ETEEN 10 VASEN YLÖS 10 ETEEN 10 VASEN
```

Saadaan kuvaa vastaava tulos.

![Vaihtelemalla konnan ylhäällä ja alhaalla olemisen välillä sanoilla `YLÖS` ja `ALAS`, syntyy katkoviivaa.](kuvat/ylösesimerkki.png)


#### `ALAS --`

> Laskee konnan alas. [s. 5, käskysanat](#käskysanat)

Kumoaa käytännössä sanan `YLÖS` vaikutuksen. Konnan ollessa alhaalla, sanat `ETEEN` ja `TAAKSE` saavat konnan jättämään jäljen. Jälki voi kuitenkin olla musta, jos konna on asetettu pyyhkimistilaan komennolla `PYYHI`. Koodiesimerkki: ks. [`YLÖS`](#ylös---) ja sen [esimerkki](#ylös_esimerkki).


#### `l ETEEN --`

> Siirtää konnaa eteen `x` askelta. [s. 6, käskysanat](#käskysanat)

Lopulta tapahtuvaan siirrokseen voi vaikuttaa sanalla `SIVUSUHDE`, mutta oletuksena konna siirtyy pikselin jokaista askelta kohden suunnasta riippumatta. Jos konna on alhaalla (ks. [`ALAS`](#alas---) ja [`YLÖS`](#ylös---)), se jättää jäljen. Koodiesimerkki: ks. [`YLÖS`](#ylös--) ja sen [esimerkki](#ylös_esimerkki).


#### `l TAAKSE --`

> Siirtää konnaa taakse $x$ askelta. [s. 6, käskysanat](#käskysanat)

`x TAAKSE` vastaa sanaa kuin `x * -1 = ETEEN`.


#### `l OIKEA --`

> Kääntää konnaa oikealle `x` astetta. [s. 6, käskysanat](#käskysanat)

Koodiesimerkki: ks. [`ASEMA NÄYTÄ`](#asema-näytä---) ja sen [esimerkki](#asema_näytä_esimerkki).


#### `l VASEN --`

> Kääntää konnaa vasemmalle `x` astetta. [s. 6, käskysanat](#käskysanat)

Koodiesimerkki: ks. [`ASEMA NÄYTÄ`](#asema-näytä---) ja sen [esimerkki](#asema_näytä_esimerkki).


#### `PYYHI --`

> Konna pyyhkii edetessään aikaisemmin piirtämänsä viivan. [s. 6, käskysanat](#käskysanat)

Käytännössä pyyhkiminen tapahtuu piirtämällä yhden pikselin paksuista mustaa viivaa.

<a id="pyyhi_esimerkki"></a>

```
KONNA
320 100 PAIKKA
100 ETEEN
90 OIKEA
100 ETEEN
170 OIKEA
PYYHI
200 ETEEN
```


Saadaan kuvaa vastaava tulos `KONNA`-komennolla avattuun ikkunaan

![`PYYHI`-komennon esimerkki. Huomaa, kuinka pystyviivasta puuttuu pikseli siitä, mistä konna kulki](kuvat/pyyhi_esimerkki.png)


#### `PIIRRÄ --`

> Poistaa `PYYHI` sanan vaikutuksen. Ts. konna piirtää taas viivaa. [s. 6, käskysanat](#käskysanat)

#### `x y PAIKKA --`

> Sijoittaa konnan paikkaan `x`, `y`. [s. 6, käskysanat](#käskysanat)

Konna ei jätä tältä matkalta jälkeä. Konna asetetaan alas siirtymisen jälkeen.

<a id="konna_kotiin_uusi"></a>

|| `KONNA` | `UUSI KOTIIN` | `KOTIIN` | `UUSI` |
|:-:| :-----: | :-----------: | :------: | :----: |
| Ylhäällä / alhaalla | Alhaalla | Alhaalla | Alhaalla | Ylhäällä |
| Piirtää / Pyyhkii | Piirtää | Piirtää | Piirtää | Piirtää |
| Sijainti | `(320, 200)`[^sivusuhdehuomio] | `(320, 200)` | `(320, 200)`| Säilyy |
| Suunta | 90° | 90° | 90° | Säilyy |
| Aikaisemmat viivat | Tuhoutuvat | Tuhoutuvat | Säilyvät | Tuhoutuvat |
| Sivusuhde | Säilyy | Säilyy | Säilyy | Säilyy |

_Kuinka `KONNA`, `UUSI KOTIIN`, `KOTIIN` ja `UUSI` vaikuttavat tilaan_

[^sivusuhdehuomio]: SAMPO 2.0:ssa kaikille kolmelle `(320, 100)`, sillä sen sivusuhde on oletuksena 1:2


#### `KOTIIN --`

Komento siirtää konnan (ilmassa) kotiosoitteeseen `(320, 200)`. Suuntakulmaksi tulee 90°. Konna päätyy alas ja se piirtää. Aikaisempi piirros säilyy. Sivusuhde ei muutu. Ks. myös [`KONNA`](#konna---), [`UUSI`](#uusi---) ja taulukko [_Kuinka `KONNA`, `UUSI KOTIIN`, `KOTIIN` ja `UUSI` vaikuttavat tilaan_](#konna_kotiin_uusi), joka avaa näiden komentojen eroja.


#### `KONNA --`

Komentoa `KONNA` on kutsuttava kerran ennen piirtämisen aloittamista. Tämä avaa piirtoikkunan, mikäli sellaista ei ennen ollut auki. Komento siirtää konnan (ilmassa) kotiosoitteeseen `(320, 200)`. Suuntakulmaksi tulee 90°. Konna päätyy alas ja se piirtää. Aikaisempi piirros hävitetään. Sivusuhde ei muutu. Ks. myös [`KOTIIN`](#kotiin---), [`UUSI`](#uusi---) ja taulukko [_Kuinka `KONNA`, `UUSI KOTIIN`, `KOTIIN` ja `UUSI` vaikuttavat tilaan_](#konna_kotiin_uusi), joka avaa näiden komentojen eroja. Sivusuhde säilyy, jos sitä on muutettu. Sanat `UUSI KOTIIN` tuottavat saman tuloksen kuin `KONNA`, jos konnaikkuna on jo avattu.



#### `x KULMA --`

> Asettaa konnan suuntakulmaksi `x`:n [s. 6, käskysanat](#käskysanat)

Kulma annetaan asteina ja 90° on suoraan ylös.


#### `SUUNTA n`

> Tuottaa pinon päähän konnan suuntakulman. [s. 6, käskysanat](#käskysanat)


#### `XKO n`

Tuottaa pinon päähän konnan `x`-koordinaatin. (Ks. [s. 6, käskysanat](#käskysanat)) `x`-koordinaatin arvoa ei kerrota sivusuhteella.


#### `YKO n`

Tuottaa pinon päähän konnan `y`-koordinaatin. (Ks. [s. 6, käskysanat](#käskysanat)) `y`-koordinaatin arvoa ei kerrota sivusuhteella.


#### `x y SIVUSUHDE --`

> Säätää näyttöruudun koordinaatistoa. [s. 6, käskysanat](#käskysanat)

Oletuksena käytössä on sivusuhde 1:1. SAMPO 2.0:ssa oletussivusuhde on 1:2, tämä ilmeisesti ajan näyttöruutujen ei-neliömäisien pikselien kompensoimiseksi.

#### `UUSI --`

Konna päätyy ylös ja se piirtää. Konnan sijainti ja suuntakulma säilyvät. Aikaisempi piirros tuhoutuu. Sivusuhde ei muutu. Ks. myös [`KONNA`](#konna---), [`KOTIIN`](#kotiin---) ja taulukko [_Kuinka `KONNA`, `UUSI KOTIIN`, `KOTIIN` ja `UUSI` vaikuttavat tilaan_](#konna_kotiin_uusi), joka avaa näiden komentojen eroja.


#### `KUVA --`

Kuva tulostaa komentoriviin tikz-muotoisen LaTeX-dokumenttiin upotettavaksi sopivan kuvan. [^kuvahuomio]

```
KONNA ok
36 KERTAA 10 ETEEN 10 OIKEA VIELÄ? ok
KUVA
\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \fill [black] (0, 0) rectangle (640, 400);
    \draw (320, 200) -- (320, 210) -- (321, 219) -- (325, 229) -- (330, 237) -- (336, 245) -- (344, 251) -- (352, 256) -- (362, 260) -- (372, 262) -- (382, 262) -- (391, 260) -- (401, 256) -- (410, 251) -- (417, 245) -- (424, 237) -- (429, 229) -- (432, 219) -- (434, 209) -- (434, 199) -- (432, 190) -- (429, 180) -- (424, 172) -- (417, 164) -- (410, 158) -- (401, 153) -- (391, 149) -- (382, 147) -- (372, 147) -- (362, 149) -- (352, 153) -- (344, 158) -- (336, 164) -- (330, 172) -- (325, 180) -- (321, 190) -- (320, 199) [color=white];
\end{tikzpicture}
```

Kun tämä ladotaan LaTeX-dokumenttiin, saadaan kuvaa vastaava tulos.

![`KUVA`-esimerkissä tulostuva tikz-kuva](kuvat/kuvaesimerkki.png)

[^kuvahuomio]: SAMPO 2.0:ssa tämä komento toimi eri tavalla: "Tulostaa grafiikkamuotoisen näyttöruudun paperille.". Tämä on kuitenkin nykykäyttöjärjestelmillä yllättävän kimuranttia toteuttaa eikä välttämättä ole muutenkaan tarkoituksenmukaista.

#### `a y x TULOSTA --`

Kirjoittaa näyttöruudulle sanan `a` lukujen `x` ja `y` kuvaamaan sijaintiin. Mikäli piirtoalueen resoluutio on 640x200, 0 0 on vasen yläkulma, muuten se on piste keskellä näytön vasenta laitaa. Teksti on 16px-korkeaa 200-korkealla näytöllä ja 8px-korkeaa 400-korkealla näytöllä. Koordinaattien yksikkö on merkin korkeus ja leveys.


#### `n y x MERKKI --`

Kirjoittaa näyttöruudulle lukua `n` vastaavan merkin lukujen `x` ja `y` kuvaamaan sijaintiin. Mikäli piirtoalueen resoluutio on 640x200, 0 0 on vasen yläkulma, muuten se on piste keskellä näytön vasenta laitaa. Teksti on 16px-korkeaa 200-korkealla näytöllä ja 8px-korkeaa 400-korkealla näytöllä. Koordinaattien yksikkö on merkin korkeus ja leveys. Sama kuin `n MERKIKSI x y TULOSTA`.


#### `x y PISTE --`

Merkitsee ruudulle pisteen paikkaan `(x, y)`. Mikäli konna piirtää, piste on valkoinen, muuten se on musta.


#### `x y VIIVA --`

Siirtää konnan (maassa) paikkaan `(x, y)`. Mikäli konna on maassa, se piirtää tai pyyhkii senhetkisen piirtotilan mukaan.


#### `x y RVIIVA --`

Siirtää konnan (maassa) paikkaan `(x + XKO, y + YKO)` (r komennon nimessä tullee sanasta _relative_). Mikäli konna on maassa, se piirtää tai pyyhkii senhetkisen piirtotilan mukaan.


#### `x y x' y' JANA --`

Piirtää tai pyyhkii janan pisteestä `(x, y)` pisteeseen `(x', y')` senhetkisen piirtotilan mukaan. Konnan ylhäällä tai alhaalla olo ei vaikuta janan piirtymiseen tai pyyhkiintymiseen.


#### `K --`

Ks. [`KOTIIN`](#kotiin---)



#### `Y --`

Ks. [`YLÖS`](#ylös---)


#### `A --`

Ks. [`ALAS`](#alas---)


#### `l E --`

Ks. [`ETEEN`](#x-eteen---)


#### `l T --`

Ks. [`TAAKSE`](#x-taakse---)


#### `l O --`

Ks. [`OIKEA`](#x-oikea---)


#### `l V --`

Ks. [`VASEN`](#x-vasen---)

