# Usage: 'input1 input2 input3' output [checklist]
# Formats files inputX into .BLK format, force-wrapping
# lines at 64 characters and inserting initial spaces
# in cases where word ends are ambiguous. Pads the ends
# of every input such that every input starts from the
# top of a screen.
# 
# If checklist is provided, it should be a comma-separated
# list of the starts of every input in the resulting file.
# If the resulting file has misaligned sections, this program
# exits with a code of 1. If checklist is not provided,
# a list of where the sections land will be printed

import sys
import subprocess

checklist = None
try:
    checklist = list(map(int, sys.argv[3].split(",")))
except:
    pass

sections = []

with open(sys.argv[2], "w") as target:
    lines_printed = 0
    i = 0
    for source in sys.argv[1].split():
        print(source)
        subprocess.run(["pandoc", "-f", "markdown", "-t", "plain", source, "-o", "tmp", "--columns=64", "--wrap=auto"])
        with open("tmp", "r") as f:
            if checklist != None and checklist[i] != lines_printed // 16:
                print(f"Wrong block start for file {source}: should be {checklist[i]}, is {lines_printed // 16}")
                sys.exit(-1)
            elif checklist == None:
                sections.append(lines_printed // 16)
            needs_sep = False
            for line in f:
                line = line.strip("\n")
                if needs_sep:
                    line = " " + line
                something_printed = False
                while(len(line) > 64):
                    something_printed = True
                    target.write(line[:64])
                    line = line[64:]
                    lines_printed += 1
                if len(line) > 0 or not something_printed:
                    target.write(line)
                    target.write(" " * (64-len(line)))
                    lines_printed += 1
                    if len(line) == 64:
                        needs_sep = True
                    else:
                        needs_sep = False
        lines_missing = (16 - lines_printed) % 16
        target.write(" "*64*lines_missing)
        lines_printed += lines_missing
        i += 1
subprocess.run(["rm", "tmp"])

if checklist == None:
    print(f"Sections: {','.join(map(str, sections))}")
