import json
import os
import sys

def file_iterator(parent, path):
    for f in os.scandir(parent + "/" + path):
        f_path = path + "/" + f.name
        if f.is_dir():
            yield {"path": f_path, "is_dir": True}
            yield from file_iterator(parent, f_path)
        else:
            with open(f.path) as current:
                yield {"path": f_path, "is_dir": False, "contents": current.read()}

print(json.dumps(list(file_iterator(sys.argv[1], ""))))
