(define-library (console)
		(import (scheme base) (scheme write) (chibi emscripten) (util) (srfi 130))
		(export
		  clear
		  tab
		  edit-frame
		  stack-show
		  stack-hide)
		(begin
		  (define (clear)
		    (display "§\n")
		    (eval-script! "postMessage(['clear page'])"))
		  (define (tab n)
		    (display "§\n")
		    (eval-script!
		      (construct-call "postMessage" (list (list "tab" n)))))
		  (define (edit-frame frame)
		    (eval-script!
		      (construct-call "postMessage" (list (list "edit frame" (string-join (vector->list frame) "\n")))))
		    (wait-on-event!)
		    (list->vector (string-split (string-eval-script "frame") "\n")))
		  (define (stack-show stack)
		    (eval-script!
		      (construct-call "postMessage" (list (list "update stack" (append stack '("Pinon pohja")))))))
		  (define (stack-hide)
		    (eval-script! "postMessage(['hide stack']);"))))
