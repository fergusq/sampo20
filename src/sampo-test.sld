(define-library (sampo-test)
        (import (scheme base) (scheme write) (sampo) (util))
        (export run-tests)
        (begin

(define (exception->string x)
   (let ((output-port (open-output-string)))
     (parameterize ((current-output-port output-port))
        (display x))
     (get-output-string output-port)))

(define (run-tests add-success add-failure add-header final-result)

(define success 0)
(define failed 0)
(define global-failure #f)

(define (naive-string x)
  (let ((output-port (open-output-string)))
    (parameterize ((current-output-port output-port))
      (display x))
    (get-output-string output-port)))

(define (test-equality desc a b skip)
  (if (equal? a b)
    (begin
      (add-success desc (not skip))
      (set! success (+ success 1)))
    (begin
      (add-failure desc (string-append "Haluttiin '" (naive-string a) "', saatiin '" (naive-string b) "'.") (not skip))
      (set! failed (+ failed 1))
      (if (not skip)
        (set! global-failure #t)
	'()))))

(define (test-stack-and-output__ desc code expected-stack expected-out skip)
  (let* ((output-port (open-output-string))
         (output-state (parameterize ((current-input-port (open-input-string code))
	                (current-output-port output-port))
			  (with-exception-handler
			    (lambda (x)
			      (add-failure desc (exception->string x) (not skip))
			        (set! failed (+ failed 1)))
			    (lambda ()
			      (call/cc
			        (lambda (return)
			          (run-words (state-set-words initial-state (read-code '()))
				             (lambda (state)
					       (return state))))))))))
       (cond
         ((not (equal? (state-stack output-state) expected-stack))
	   (add-failure desc (string-append "Haluttiin pino " (naive-string expected-stack) ", saatiin pino " (naive-string (state-stack output-state))) (not skip))
	   (if (not skip)
	     (set! global-failure #t)
	     '())
	   (set! failed (+ failed 1)))
	 ((not (equal? (get-output-string output-port) expected-out))
	   (add-failure desc (string-append "Haluttiin tuloste '" expected-out"', saatiin tuloste '" (get-output-string output-port) "'") (not skip))
	   (if (not skip)
	     (set! global-failure #t)
	     '())
	   (set! failed (+ failed 1)))
         ((not (equal? (state-call-stack output-state) '()))
	   (add-failure desc (string-append "Haluttiin tyhjä kutsupino, saatiin " (naive-string (state-call-stack output-state))) (not skip))
	   (if (not skip)
	     (set! global-failure #t)
	     '())
	   (set! failed (+ failed 1)))
	 (else
	   (begin
	     (add-success desc (not skip))
	     (set! success (+ success 1)))))))

(define (test-stack-and-output desc code expected-stack expected-out)
  (test-stack-and-output__ desc code expected-stack expected-out #f))

(define (test-stack desc code expected)
  (test-stack-and-output desc code expected ""))

(define (test-output desc code expected)
  (test-stack-and-output desc code '() expected))

(add-header "Sisäiset testit")

(test-equality "js-bleach (patologinen merkkijono)"
	     "`\\`\"'\n\\\\\`"
             (js-bleach "`\"'\n\\")
	     #f)

(test-equality "construct-call (ei parametreja)"
	     "kissa()"
             (construct-call "kissa" '())
	     #f)

(test-equality "construct-call (totuusarvot)"
	     "kissa(true,false,)"
             (construct-call "kissa" '(#t #f))
	     #f)

(test-equality "construct-call (kokonaisluku)"
	     "kissa(13,)"
             (construct-call "kissa" '(13))
	     #f)

(test-equality "construct-call (liukuluku)"
	     "kissa(0.5,)"
             (construct-call "kissa" '(0.5))
	     #f)

(test-equality "construct-call (murtoluku muuntuu liukuluvuksi)"
	     "kissa(0.5,)"
             (construct-call "kissa" (list (/ 1 2)))
	     #f)

(test-equality "construct-call (lista)"
	     "kissa(1,[[2,3,],[4,5,],],6,)"
             (construct-call "kissa" '(1 ((2 3) (4 5)) 6))
	     #f)

(test-equality "construct-call (patologinen merkkijono)"
	     "kissa(`\\`\"'\n\\\\\`,)"
             (construct-call "kissa" '("`\"'\n\\"))
	     #f)

(add-header "Syöttö ja tulostus tekstitilassa")

(test-output "UUSIRIVI (tuloste)"
             "UUSIRIVI"
             "\n")

(add-header "Pinot")

(test-stack "TUPLAA"
            "1 2 3 TUPLAA"
            '(3 3 2 1))

(test-stack "VAIHDA"
            "1 2 3 VAIHDA"
            '(2 3 1))

(test-stack "KIERRÄ"
            "1 2 3 4 KIERRÄ"
            '(2 4 3 1))

(test-stack "POISTA"
            "1 2 3 POISTA"
            '(2 1))

(test-stack "YLI"
            "1 2 3 YLI"
            '(2 3 2 1))

(test-stack-and-output ". (1)"
                       "10 ."
		       '()
                       "10 ")

(test-stack-and-output ". (2)"
                       "10 42 1337 ."
                       '(42 10)
                       "1337 ")

(test-output ". (liukuluvun formatointi)"
             "0.5 ."
             "0,5 ")

(test-output ". (liukuluvun formatointi 2)"
	     "0,5 ."
	     "0,5 ")

(test-stack-and-output "? (1)"
                       "10 ?"
                       '(10)
                       "10 ")

(test-stack-and-output "? (2)"
                       "10 42 1337 ?"
                       '(1337 42 10)
                       "1337 ")

(test-stack-and-output "? (liukuluvun formatointi)"
                       "0.5 ?"
                       '(0.5)
                       "0,5 ")

(add-header "Listakomennot")

(test-stack "KPL (lista)"
	    "( a b c d ) KPL"
	    '(4))

(test-stack "KPL (merkkijono)"
            " \" KISSA KPL"
	    '(5))

(test-stack "PÄÄ (lista)"
	    "( a b c d ) PÄÄ"
	    '("a"))

(test-stack "PÄÄ (alkio)"
	    "\" talo PÄÄ"
	    '("t"))

(test-stack "PERÄ (lista)"
	    "( a b c d ) PERÄ"
	    '("d"))

(test-stack "PERÄ (alkio)"
	    "\" talo PERÄ"
	    '("o"))

(test-stack "HÄNTÄ (lista)"
	    "( a b c d ) HÄNTÄ"
	    '(("b" "c" "d")))

(test-stack "HÄNTÄ (alkio)"
	    "\" talo HÄNTÄ"
	    '("alo"))

(test-stack "ALKUPÄÄ (lista)"
	    "( a b c d ) ALKUPÄÄ"
	    '(("a" "b" "c")))

(test-stack "ALKUPÄÄ (alkio)"
	    "\" talo ALKUPÄÄ"
	    '("tal"))


(test-stack "PÄÄKSI (kaksi alkiota)"
	    "\" ELÄIN \" KISSA PÄÄKSI"
	    '("KISSAELÄIN"))

(test-stack "PÄÄKSI (alkio)"
	    "( a b c d ) \" z PÄÄKSI"
	    '(("z" "a" "b" "c" "d")))

(test-stack "PÄÄKSI (lista)"
	    "( a b c d ) ( x y ) PÄÄKSI"
	    '(("x" "y" "a" "b" "c" "d")))

(test-stack "PERÄÄN (kaksi alkiota)"
	    "\" ELÄIN \" KISSA PERÄÄN"
	    '("ELÄINKISSA"))

(test-stack "PERÄÄN (alkio)"
	    "( a b c d ) \" z PERÄÄN"
	    '(("a" "b" "c" "d" "z")))

(test-stack "PERÄÄN (lista)"
	    "( a b c d ) ( x y ) PERÄÄN"
	    '(("a" "b" "c" "d" "x" "y")))

(test-stack "LIITÄ"
	    "\" x \" y LIITÄ"
	    '(("x" "y")))

(test-stack "KUULUU? (tosi tapaus)"
	    "( a b c d ) \" b KUULUU?"
	    '(-1))

(test-stack "KUULUU? (epätosi tapaus)"
	    "( a b c d ) \" z KUULUU?"
	    '(0))

(test-stack "LISTAKSI"
            "\" kissa LISTAKSI"
            '(("kissa")))

(test-stack "ALKIOKSI"
            "
            1 ALKIOKSI
            2,5 ALKIOKSI
            "
            '("2,5" "1"))

(test-stack "LUVUKSI"
            "
            \" 4 LUVUKSI
            \" 5,5 LUVUKSI
            "
            '(5.5 4))

(test-stack "ALKIO? (tosi tapaus)"
            "\" kissa ALKIO?"
            '(-1))

(test-stack "ALKIO? (epätosi tapaus)"
            "
            4 ALKIO?
            ( kissa ) ALKIO?
            "
            '(0 0))

(test-stack "LISTA? (tosi tapaus)"
            "( kissa ) LISTA?"
            '(-1))

(test-stack "LISTA? (epätosi tapaus)"
            "
            4 LISTA?
            \" kissa LISTA?
            "
            '(0 0))

(test-stack "TEE (pino)"
            "( 2 ) TEE"
            '(2))

(test-output "TEE (tulostus)"
             "( 2 . ) TEE"
             "2 ")

(test-stack "TEE (sisäkkäiset listat)"
            "( ( 2 + 2 ) = ) TEE"
            '(4))

(test-output "TEE (kertaa)"
            "( 5 KERTAA I . VIELÄ? ) TEE"
	    "1 2 3 4 5 ")

(add-header "Totuusarvot ja ehtolauseet")

(test-stack "-1 TAI -1"
            "-1 TAI -1 ="
            '(-1))

(test-stack "-1 TAI 0"
            "-1 TAI 0 ="
            '(-1))

(test-stack "0 TAI -1"
            "0 TAI -1 ="
            '(-1))

(test-stack "0 TAI 0"
            "0 TAI 0 ="
            '(0))

(test-stack "-1 JA -1"
            "-1 JA -1 ="
            '(-1))

(test-stack "-1 JA 0"
            "-1 JA 0 ="
            '(0))

(test-stack "0 JA -1"
            "0 JA -1 ="
            '(0))

(test-stack "0 JA 0"
            "0 JA 0 ="
            '(0))

(test-stack "EI -1"
            "EI -1 ="
	    '(0))

(test-stack "EI 0"
            "EI 0 ="
	    '(-1))

(test-stack "EI 123"
            "EI 123 ="
	    '(0))

(test-stack "EI osana sulkulauseketta"
            "( EI 1 == 2 ) JA -1 ="
	    '(-1))

(test-stack "== (yhtäsuuri)"
            "123 == 123 ="
            '(-1))

(test-stack "== (suurempi)"
            "123 == 122 ="
            '(0))

(test-stack "== (pienempi)"
            "123 == 124 ="
            '(0))

(test-stack "<> (yhtäsuuri)"
            "123 <> 123 ="
            '(0))

(test-stack "<> (suurempi)"
            "123 <> 122 ="
            '(-1))

(test-stack "<> (pienempi)"
            "123 <> 124 ="
            '(-1))

(test-stack "> (yhtäsuuri)"
            "123 > 123 ="
            '(0))

(test-stack "> (suurempi)"
            "123 > 122 ="
            '(-1))

(test-stack "> (pienempi)"
            "123 > 124 ="
            '(0))

(test-stack "< (yhtäsuuri)"
            "123 < 123 ="
            '(0))

(test-stack "< (suurempi)"
            "123 < 122 ="
            '(0))

(test-stack "< (pienempi)"
            "123 < 124 ="
            '(-1))

(test-stack ">= (yhtäsuuri)"
            "123 >= 123 ="
            '(-1))

(test-stack ">= (suurempi)"
            "123 >= 122 ="
            '(-1))

(test-stack ">= (pienempi)"
            "123 >= 124 ="
            '(0))

(test-stack "<= (yhtäsuuri)"
            "123 <= 123 ="
            '(-1))

(test-stack "<= (suurempi)"
            "123 <= 122 ="
            '(0))

(test-stack "<= (pienempi)"
            "123 < 124 ="
            '(-1))

(add-header "LUO ja itse määriteltyjen funktioiden kutsut")

(test-stack "Luo (funktion määritteleminen ei tuota virhettä)"
            "LUO F 10 VALMIS"
            '())

(test-stack "Luo (funktion määritteleminen varaa symbolin)"
            "LUO F VALMIS F"
            '())

(test-stack "Luo (funktion pino säilyttää aiempaa dataa)"
            "10 LUO F VALMIS F"
            '(10))

(test-stack "Luo (funktion pinolle työntämät arvot säilyvät)"
            "LUO F 10 VALMIS F"
            '(10))

(test-stack "Luo (nimetyt muuttujat 1)"
            "
            LUO :a :b :c VALITSE-A :a VALMIS
            LUO :a :b :c VALITSE-B :b VALMIS
            LUO :a :b :c VALITSE-C :c VALMIS
            12 34 43 VALITSE-A
            43 45 87 VALITSE-B
            98 78 65 VALITSE-C
            "
            '(65 45 12))

(test-stack "LUO (nimetyt muuttujat 2)"
            "
            LUO :a TUPLAA :a + :a = VALMIS
            35 TUPLAA
            "
            '(70))

(test-stack "LUO (nimetyt muuttujat 3)"
            "
            LUO :a F
            :a
            JOS :a == 1
            tosi?
            muuten :a - 1 = F
            jatka
            valmis
            10 F
            "
            '(1 2 3 4 5 6 7 8 9 10))
(test-stack "LUO (nimetyt muuttujat 3)"
            "
            LUO :a F
            :a
            JOS :a == 1
            tosi?
            muuten :a - 1 = F
            jatka
            :a
            valmis
            10 F
            "
            '(10 9 8 7 6 5 4 3 2 1 1 2 3 4 5 6 7 8 9 10))

(test-stack "LUO (nimetyt muuttujat lausekkeissa)"
            "
            LUO :a-r :a-i :b-r :b-i KOMPLEKSISUMMA :a-r + :b-r = :a-i + :b-i = VALMIS
            32 -43 21 9 KOMPLEKSISUMMA
            "
            '(-34 53))

; Ilmeisesti alkuperäinen sampo ei tue tätä. Jos ominaisuutta päätetään tukea, niin kelpo testi. Alkuperäinen sampo puskee pinolle (ennen = -sanaa)
; 
; 7
; *
; ( :a + :b )

; (test-stack "LUO (nimetyt muuttujat sulkulausekkeissa)"
;             "
;             LUO :a :b :c F ( :a + :b ) * :c = VALMIS
;             2 5 7 F
;             "
;             '(49))

(test-stack "Luo (yksi nimetty muuttuja toimii kertomassa)"
            "
            luo :n kertoma
            jos :n <= 1
            tosi?   1
            muuten  :n - 1 = kertoma * :n =
            jatka
            valmis
            7 KERTOMA
            "
            '(5040))

(test-stack-and-output "LUO ja UNOHDA (funktion hävitys)"
	    "
LUO VEKS POISTA VALMIS
1 2 3
VEKS
UNOHDA VEKS
VEKS
"
'(2 1)
"Tuntematon komento VEKS. Kutsupino: ()\n")

(test-stack-and-output "LUO ja UNOHDA (yksinkertainen peruutus)"
"
LUO X 1 VALMIS
LUO X 2 VALMIS
UNOHDA X
X
UNOHDA X
X
"
'(1)
"Tuntematon komento X. Kutsupino: ()\n")

(test-stack "LUO ja UNOHDA (funktioparin riippuvuus)"
"
LUO A 1 VALMIS
LUO B 2 VALMIS
LUO A B VALMIS
A
UNOHDA B
A
"
'(1 2))

(test-stack "LUO, MUUTTUJA ja UNOHDA (muuttujat ja funktiot 1)"
"
MUUTTUJA A
MUUTTUJA B
3 B LLE
LUO X 10 VALMIS
LUO B 20 VALMIS
UNOHDA B
X
B ARVO
"
'(3 10))

(test-stack "LUO, MUUTTUJA ja UNOHDA (muuttujat ja funktiot 2)"
"
LUO A 10 VALMIS
LUO B 10 VALMIS
MUUTTUJA A
MUUTTUJA B
UNOHDA B
B
"
'(10))

(test-output "LUO, MUUTTUJA, JONO ja VAKIO (muuttujat eivät vuoda)"
"
LUO F \" KELPO VAKIO X X . VALMIS
F
X
UNOHDA F
LUO F MUUTTUJA X \" KELPO X LLE X ARVO . VALMIS
F
X
UNOHDA F
LUO F 10 JONO X \" KELPO 5 X LLE 5 X ARVO . VALMIS
F
X
"
"KELPO Tuntematon komento X. Kutsupino: ()
KELPO Tuntematon komento X. Kutsupino: ()
KELPO Tuntematon komento X. Kutsupino: ()
")

(test-stack "LUO, TEE (TEE:n sisällä luodut tulevat juurinimiavaruuteen)"
"
LUO F ( LUO G 10 VALMIS ) TEE ( G ) TEE VALMIS
F
G
"
'(10 10))

(test-output "LUO (sanat eivät vuoda)"
"
LUO F LUO G VALMIS G VALMIS
F
G
"
"Tuntematon komento G. Kutsupino: ()\n")

(test-output "LUO, MUUTTUJA, JONO, VAKIO, TEE: TEEn sisällä muokataan juurinimiavaruutta"
"
LUO F ( LUO G VALMIS ) TEE G ( G ) TEE VALMIS
F
UNOHDA F

LUO F ( MUUTTUJA G ) TEE G ( G ARVO POISTA ) TEE VALMIS
F
UNOHDA F

LUO F ( 10 VAKIO G ) TEE G ( G POISTA ) TEE VALMIS
F
UNOHDA F

LUO F ( 10 JONO G ) TEE G ( 5 G POISTA ) TEE VALMIS
F
UNOHDA F
"
"Tuntematon komento G. Kutsupino: (F)
Tuntematon komento G. Kutsupino: (F)
Tuntematon komento G. Kutsupino: (F)
Tuntematon komento G. Kutsupino: (F)
")

(add-header "Ohjausrakenteet")


(test-stack "Jos tosi? -lause (tosihaara, yksinkertainen tapaus)"
            "JOS -1 TOSI? ( TOSIHAARA ) MUUTEN ( VALEHAARA ) JATKA"
            '(("TOSIHAARA")))

(test-stack "Jos tosi? -lause (valehaara, yksinkertainen tapaus)"
            "JOS 0 TOSI? ( TOSIHAARA ) MUUTEN ( VALEHAARA ) JATKA"
            '(("VALEHAARA")))


(test-stack "Jos vale? -lause (tosihaara, yksinkertainen tapaus)"
            "JOS 0 VALE? ( TOSIHAARA ) MUUTEN ( VALEHAARA ) JATKA"
            '(("TOSIHAARA")))

(test-stack "Jos vale? -lause (valehaara, yksinkertainen tapaus)"
            "JOS -1 VALE? ( TOSIHAARA ) MUUTEN ( VALEHAARA ) JATKA"
            '(("VALEHAARA")))

(test-stack "Jos-lause (explisiittinen =)"
            "JOS -1 = TOSI? ( TOSIHAARA ) MUUTEN ( VALEHAARA ) JATKA"
            '(("TOSIHAARA")))

(test-stack "Jos-lause (ehdon sisältä vuotamisen oikeellisuus)"
            "JOS 1 2 3 -1 TOSI? MUUTEN JATKA"
            '(3 2 1))

(test-stack "Jos-lause (sisäkkäisyyden oikeellisuus)"
            "JOS
                JOS -1 TOSI? 1 -1 MUUTEN 2 0 JATKA
             TOSI?
                JOS 0 TOSI? 3 MUUTEN 4 JATKA
             MUUTEN
                JOS -1 TOSI? 5 MUUTEN 6 JATKA
             JATKA"
            '(4 1))

(test-stack "Jos-lause (sisäkkäisyyden oikeellisuus 2)"
            "JOS
                JOS -1 TOSI? 1 -1 MUUTEN 2 0 JATKA
             VALE?
                JOS 0 TOSI? 3 MUUTEN 4 JATKA
             MUUTEN
                JOS -1 VALE? 5 MUUTEN 6 JATKA
             JATKA"
            '(6 1))

(test-stack "Jos on -lause (yksinkertainen)"
            "
            JOS 42 ON
                42 NIIN \" TOSIHAARA LOPPUUN
                ELLEI \" VALEHAARA
            LOPPU
            "
            '("TOSIHAARA"))

(test-stack "Jos on -lause (monta, ensimmäinen oikea)"
            "
            JOS 42 ON
                42 NIIN \" TOSIHAARA LOPPUUN
                16 NIIN \" VALEHAARA1 LOPPUUN
                ELLEI \" VALEHAARA2
            LOPPU
            "
            '("TOSIHAARA"))

(test-stack "Jos on -lause (monta, keskimmäinen oikea)"
            "
            JOS 42 ON
                57 NIIN \" VALEHAARA1 LOPPUUN
                42 NIIN \" TOSIHAARA LOPPUUN
                ELLEI \" VALEHAARA2
            LOPPU
            "
            '("TOSIHAARA"))

(test-stack "Jos on -lause (monta, ellei-haara oikea)"
            "
            JOS 42 ON
                57 NIIN \" VALEHAARA1 LOPPUUN
                16 NIIN \" VALEHAARA2 LOPPUUN
                ELLEI \" TOSIHAARA
            LOPPU
            "
            '("TOSIHAARA"))

(test-stack "Toista-silmukka ja Jos-lause (Collatz)"
            "
            7
            TOISTA TUPLAA <> 1 YHÄ?
                JOS TUPLAA MOD 2 = <> 0 TOSI?
                    TUPLAA * 3 + 1 =
                MUUTEN
                JATKA
                JOS TUPLAA MOD 2 = == 0 TOSI?
                    TUPLAA / 2 =
                MUUTEN
                JATKA
            ALKUUN"
            '(1 2 4 8 16 5 10 20 40 13 26 52 17 34 11 22 7))

(test-stack "Kertaa-silmukka (kertoma)"
            "1 7 KERTAA * I = VIELÄ?"
            '(5040))

(test-stack "Kertaa-silmukka (sisäkkäiset)"
            "3 KERTAA 4 KERTAA I J VIELÄ? VIELÄ?"
            '(4 3 3 3 2 3 1 3 4 2 3 2 2 2 1 2 4 1 3 1 2 1 1 1))

(test-stack "Toista-silmukka 1 (iterointi)"
            "10 TOISTA TUPLAA > 0 YHÄ? TUPLAA - 1 = ALKUUN"
            '(0 1 2 3 4 5 6 7 8 9 10))

(test-stack "Toista-silmukka 2 (Collatz)"
            "7 TOISTA TUPLAA <> 1 YHÄ? TOISTA TUPLAA MOD 2 = <> 0 YHÄ? TUPLAA * 3 + 1 = ALKUUN TOISTA TUPLAA MOD 2 = <> 0 LOPETA? TUPLAA / 2 = ALKUUN ALKUUN"
            '(1 2 4 8 16 5 10 20 40 13 26 52 17 34 11 22 7))


(test-stack "Toista-silmukka 2 (Collatz, silmukan rivitys)"
            "
            7
            TOISTA TUPLAA <> 1 YHÄ?
                TOISTA TUPLAA MOD 2 = <> 0 YHÄ?
                    TUPLAA * 3 + 1 =
                ALKUUN
                TOISTA TUPLAA MOD 2 = <> 0 LOPETA?
                    TUPLAA / 2 =
                ALKUUN
            ALKUUN"
            '(1 2 4 8 16 5 10 20 40 13 26 52 17 34 11 22 7))

(add-header "Laskutoimitukset")

(test-stack "Yksinkertainen laskenta 1"
            "2 + 2 ="
            '(4))

(test-stack "Yksinkertainen laskenta 2"
            "3 - 2 ="
            '(1))

(test-stack "Laskujärjestys 1"
            "2 * 3 + 1 ="
            '(7))

(test-stack "Laskujärjestys 2"
            "1 + 6 / 2 - 2 ="
            '(2))

(test-stack "Sulut laskennassa 1"
            "( 3 - 2 ) + 1 ="
            '(2))

(test-stack "Sulut laskennassa 2"
            "( ( 3 + 4 ^ 2 ) / 2 + 3 ^ 2 ^ 2 - 4 / 2 + ( 4 - 3 ) * 2 ) ="
            '(90.5))

(test-stack "Sulut laskennassa 3"
            "( ( 3 ) ) ="
            '(3))

(test-stack-and-output__ "Sulut laskennassa 4 (suoritusjärjestys)"
                         "( 1 10 ) + ( 2 10 ) + ( 3 10 ) ="
                         '(30 1 2 3)
			 ""
			 #t)

(test-stack "+ (yhteenlasku)"
            "1 + 2 + 3 ="
            '(6))

(test-stack "- (vähennyslasku)"
            "6 - 2 - 2 ="
            '(2))

(test-stack "* (kertolasku)"
            "1 * 2 * 3 ="
            '(6))

(test-stack "/ (jakolasku)"
            "8 / 2 / 2 ="
            '(2))

(test-stack "^ (potenssi)"
            "2 ^ 1 ^ 2 ="
            '(2))

(test-stack "^ (potenssi, float^int = float)"
            "1.0 ^ 1 ="
	    '(1.0))

(test-stack "^ (potenssi, int^float = float)"
            "1 ^ 1.0 ="
	    '(1.0))

(test-stack "^ (potenssi, int^(int > 0) = int)"
            "3 ^ 3 ="
	    '(27))

(test-stack "^ (potenssi, int^(int = 0) = int)"
            "3 ^ 0 ="
	    '(1))

(test-stack "^ (potenssi, int^(int < 0) = float)"
            "2 ^ -1 ="
	    '(0.5))

(test-stack "Muuttujat suluissa"
            "
            MUUTTUJA A 2 A LLE
            MUUTTUJA B 3 B LLE
            ( A * B ) =
            "
            '(6))

(test-stack "Vakiot suluissa"
            "
            5 VAKIO A
            6 VAKIO B
            ( A * B ) =
            "
            '(30))

(test-stack-and-output "Nollalla jako tuottaa virheviestin"
            "5 / 0 ="
            '(0)
            "Virhe: nollalla jako laskussa 5 / 0\n")

(test-stack-and-output "Nollalla modulo tuottaa virheviestin"
            "5 MOD 0 ="
            '(0)
            "Virhe: modulo nollalla laskussa 5 MOD 0\n")

(test-stack-and-output "Nolla korotettuna negatiiviseen potenssiin tuottaa virheviestin"
            "0 ^ -1 ="
            '(0)
            "Virhe: nolla korotettu negatiiviseen potenssiin laskussa 0 ^ -1\n")

(test-stack-and-output "Ei-lukutyyppisellä arvolla laskeminen tuottaa virheviestin 1"
            "\" kissa + 1 ="
            '(0)
            "Virhe: jompikumpi laskettavista arvoista ei ole lukutyyppinen. Arvot ovat kissa ja 1\n")

(test-stack-and-output "Ei-lukutyyppisellä arvolla laskeminen tuottaa virheviestin 2"
            " 1 + \" kissa ="
            '(0)
            "Virhe: jompikumpi laskettavista arvoista ei ole lukutyyppinen. Arvot ovat 1 ja kissa\n")

(test-stack-and-output "Ei-lukutyyppisellä arvolla laskeminen tuottaa virheviestin 3"
            "\" kissa + \" koira ="
            '(0)
            "Virhe: jompikumpi laskettavista arvoista ei ole lukutyyppinen. Arvot ovat kissa ja koira\n")

(test-stack-and-output "Ei-lukutyyppisellä arvolla laskiessa virheviestissä oikein tulostuvat ääkköselliset alkiot"
            "\" Ä + \" Ö ="
            '(0)
            "Virhe: jompikumpi laskettavista arvoista ei ole lukutyyppinen. Arvot ovat Ä ja Ö\n")

(test-stack "SIN"
            "
            LUO KATKAISU * 10000000000 = INT VALMIS
            0 SIN KATKAISU
            30 SIN KATKAISU
            45 SIN KATKAISU
            90 SIN KATKAISU
            180 SIN KATKAISU
            360 SIN KATKAISU
            405 SIN KATKAISU
            "
	    '(7071067811 -1 0 10000000000 7071067811 4999999999 0))

(test-stack "COS"
            "
            LUO KATKAISU * 10000000000 = INT VALMIS
            0 COS KATKAISU
            30 COS KATKAISU
            45 COS KATKAISU
            90 COS KATKAISU
            180 COS KATKAISU
            360 COS KATKAISU
            405 COS KATKAISU
            "
            '(7071067811 10000000000 -10000000000 0 7071067811 8660254037 10000000000))

(test-stack "SIN (suorittaa laskutoimituksen)"
            "
            LUO KATKAISU * 10000000000 = INT VALMIS
            10 89 + 1 SIN KATKAISU
            "
            '(10000000000 10))

(test-stack "SIN (ei hämmenny suluista)"
            "
            LUO KATKAISU * 10000000000 = INT VALMIS
            ( 90 ) SIN KATKAISU
            "
            '(10000000000))

(test-stack "COS (suorittaa laskutoimituksen)"
            "
            LUO KATKAISU * 10000000000 = INT VALMIS
            10 89 + 1 COS KATKAISU
            "
            '(0 10))

(test-stack "COS (ei hämmenny suluista)"
            "
            LUO KATKAISU * 10000000000 = INT VALMIS
            ( 90 ) COS KATKAISU
            "
            '(0))

(test-stack "INT"
            "
            0,1 INT
            1,5 INT
            2,7 INT
            3,9999 INT
            "
            '(3 2 1 0))

(test-stack "ASCIIKSI (ascii-merkki, suuraakkonen)"
            "\" X ASCIIKSI"
	    '(88))

(test-stack "ASCIIKSI (ascii-merkki, pienaakkonen)"
            "\" x ASCIIKSI"
	    '(120))

(test-stack "ASCIIKSI (Latin-1-merkki)"
            "\" ä ASCIIKSI"
	    '(228))
(test-stack "ASCIIKSI (Hiragana-merkki)"
            "\" あ ASCIIKSI"
	    '(12354))

(test-stack "MERKIKSI (ascii-merkki, suuraakkonen)"
            "88 MERKIKSI"
	    '("X"))

(test-stack "MERKIKSI (ascii-merkki, pienaakkonen)"
	    "120 MERKIKSI"
	    '("x"))

(test-stack "MERKIKSI (Latin-1-merkki)"
            "228 MERKIKSI"
	    '("ä"))

(test-stack "MERKIKSI (Hiragana-merkki)"
            "12354 MERKIKSI"
	    '("あ"))

(test-stack "SATTUMA"
            "
            MUUTTUJA A
            100 KERTAA 2 SATTUMA A LLE JOS A <> 1 JA A <> 2 TOSI? 0 JATKA VIELÄ?
            "
            '())

(test-output "SATTUMA ja SIEMEN"
             "
Ö Siemenluvun asetus

10 SIEMEN LLE

Ö Arvotaan 10 lukua

MUUTTUJA LUVUT
( ) LUVUT LLE
10 KERTAA
LUVUT ARVO
100 SATTUMA
PERÄÄN
LUVUT LLE
VIELÄ?

Ö Uudelleenalustetaan satunnaisluvut

9 SIEMEN LLE
10 SATTUMA POISTA
10 SIEMEN LLE

10 KERTAA
JOS LUVUT ARVO PÄÄ == 100 SATTUMA
TOSI?
MUUTEN
\" RIKKI .
JATKA
LUVUT ARVO HÄNTÄ LUVUT LLE
VIELÄ?
"
	     "")

(add-header "Muuttujat ja jonot")

(test-stack "MUUTTUJA (yksinkertainen muuttuja)"
            "MUUTTUJA KISSA 10 KISSA LLE KISSA ARVO"
            '(10))

(test-stack "MUUTTUJA (fibonacci)"
            "
            MUUTTUJA a 1 a LLE
            MUUTTUJA b 0 b LLE
            10 KERTAA a + b = a ARVO b LLE a LLE b ARVO
            "
            '(55 34 21 13 8 5 3 2 1 1))

(test-stack "JONO"
            "
            3 JONO L
            2 1 L LLE
            4 2 L LLE
            8 3 L LLE
            2 L ARVO
            1 L ARVO
            3 L ARVO
            2 L ARVO
            "
            '(4 8 2 4))

(test-stack "VAKIO"
            "
            8 VAKIO KASI
            9 VAKIO YSI
            YSI KASI KASI YSI
            "
            '(9 8 8 9))

(add-header "Konna")

(test-stack "TULOSTA ei aiheuta muistinhallintakaatumista"
"
KONNA
36 KERTAA 10 E 10 O VIELÄ?
20 KERTAA I ALKIOKSI 1 I TULOSTA VIELÄ?
36 KERTAA 10 E 10 V VIELÄ?
20 KERTAA I ALKIOKSI 2 I TULOSTA VIELÄ?
UUSI
36 KERTAA 10 E 10 O VIELÄ?
20 KERTAA I ALKIOKSI 1 I TULOSTA VIELÄ?
36 KERTAA 10 E 10 V VIELÄ?
20 KERTAA I ALKIOKSI 2 I TULOSTA VIELÄ?
KONNA
36 KERTAA 10 E 10 O VIELÄ?
20 KERTAA I ALKIOKSI 1 I TULOSTA VIELÄ?
36 KERTAA 10 E 10 V VIELÄ?
20 KERTAA I ALKIOKSI 2 I TULOSTA VIELÄ?
UUSI
36 KERTAA 10 E 10 O VIELÄ?
20 KERTAA I ALKIOKSI 1 I TULOSTA VIELÄ?
36 KERTAA 10 E 10 V VIELÄ?
20 KERTAA I ALKIOKSI 2 I TULOSTA VIELÄ?
"
	    '())

(test-stack "MERKKI (on olemassa)"
            " KONNA 75 1 1 MERKKI"
	    '())

(test-stack "XKO ja YKO"
	    "
KONNA
4 KERTAA XKO YKO 90 VASEN 10 ETEEN VIELÄ?"
	    '(190 320 190 310 200 310 200 320))

(test-stack "SIVUSUHDE ei vaikuta XKO ja YKO"
	    "
KONNA
XKO YKO
4 4 SIVUSUHDE
XKO YKO
1 1 SIVUSUHDE"
            '(200 320 200 320))

(test-output "ETEEN ja TAAKSE"
	     "
KONNA
YLÖS 639 100 PAIKKA ALAS
45 VASEN
9 KERTAA 50 ETEEN 90 OIKEA 50 TAAKSE 90 VASEN VIELÄ?
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (639, 100) -- (603, 135) -- (568, 100) -- (532, 135) -- (497, 100) -- (462, 135) -- (426, 100) -- (391, 135) -- (356, 100) -- (320, 135) -- (285, 100) -- (250, 135) -- (214, 100) -- (179, 135) -- (144, 100) -- (108, 135) -- (73, 100) -- (37, 135) -- (2, 100) [color=white];\n\\end{tikzpicture}\n")

(test-output "PYYHI"
	     "
KONNA
100 ETEEN
90 OIKEA
100 ETEEN
PYYHI
170 OIKEA
200 ETEEN
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) -- (420, 300) [color=white];\n    \\draw (420, 300) -- (223, 265) [color=black];\n\\end{tikzpicture}\n")

(test-output "PYYHI 2 (katkoympyrä)"
	     "
KONNA
36 KERTAA 10 ETEEN 10 OIKEA VIELÄ?
PYYHI
18 KERTAA ALAS 10 ETEEN 10 OIKEA YLÖS 10 ETEEN 10 OIKEA VIELÄ?
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 210) -- (321, 219) -- (325, 229) -- (330, 237) -- (336, 245) -- (344, 251) -- (352, 256) -- (362, 260) -- (372, 262) -- (382, 262) -- (391, 260) -- (401, 256) -- (410, 251) -- (417, 245) -- (424, 237) -- (429, 229) -- (432, 219) -- (434, 209) -- (434, 199) -- (432, 190) -- (429, 180) -- (424, 172) -- (417, 164) -- (410, 158) -- (401, 153) -- (391, 149) -- (382, 147) -- (372, 147) -- (362, 149) -- (352, 153) -- (344, 158) -- (336, 164) -- (330, 172) -- (325, 180) -- (321, 190) -- (320, 199) [color=white];\n    \\draw (320, 199) -- (320, 209) [color=black];\n    \\draw (321, 219) -- (325, 229) [color=black];\n    \\draw (330, 237) -- (336, 245) [color=black];\n    \\draw (344, 251) -- (352, 256) [color=black];\n    \\draw (362, 260) -- (372, 262) [color=black];\n    \\draw (382, 262) -- (391, 260) [color=black];\n    \\draw (401, 256) -- (410, 251) [color=black];\n    \\draw (417, 245) -- (424, 237) [color=black];\n    \\draw (429, 229) -- (432, 219) [color=black];\n    \\draw (434, 209) -- (434, 199) [color=black];\n    \\draw (432, 190) -- (429, 180) [color=black];\n    \\draw (424, 172) -- (417, 164) [color=black];\n    \\draw (410, 158) -- (401, 153) [color=black];\n    \\draw (391, 149) -- (382, 147) [color=black];\n    \\draw (372, 147) -- (362, 149) [color=black];\n    \\draw (352, 153) -- (344, 158) [color=black];\n    \\draw (336, 164) -- (330, 172) [color=black];\n    \\draw (325, 180) -- (321, 190) [color=black];\n\\end{tikzpicture}\n")

(test-output "PYYHI ja PIIRRÄ (katkoympyrä)"
	     "
KONNA
12 KERTAA PIIRRÄ 10 ETEEN 10 OIKEA PYYHI 10 ETEEN 10 OIKEA 10 ETEEN 10 OIKEA VIELÄ?
150 100 PAIKKA
12 KERTAA PIIRRÄ 10 ETEEN 10 OIKEA 10 ETEEN 10 OIKEA PYYHI 10 ETEEN 10 OIKEA VIELÄ?
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 210) [color=white];\n    \\draw (320, 210) -- (321, 219) -- (325, 229) [color=black];\n    \\draw (325, 229) -- (330, 237) [color=white];\n    \\draw (330, 237) -- (336, 245) -- (344, 251) [color=black];\n    \\draw (344, 251) -- (352, 256) [color=white];\n    \\draw (352, 256) -- (362, 260) -- (372, 262) [color=black];\n    \\draw (372, 262) -- (382, 262) [color=white];\n    \\draw (382, 262) -- (391, 260) -- (401, 256) [color=black];\n    \\draw (401, 256) -- (410, 251) [color=white];\n    \\draw (410, 251) -- (417, 245) -- (424, 237) [color=black];\n    \\draw (424, 237) -- (429, 229) [color=white];\n    \\draw (429, 229) -- (432, 219) -- (434, 209) [color=black];\n    \\draw (434, 209) -- (434, 199) [color=white];\n    \\draw (434, 199) -- (432, 190) -- (429, 180) [color=black];\n    \\draw (429, 180) -- (424, 172) [color=white];\n    \\draw (424, 172) -- (417, 164) -- (410, 158) [color=black];\n    \\draw (410, 158) -- (401, 153) [color=white];\n    \\draw (401, 153) -- (391, 149) -- (382, 147) [color=black];\n    \\draw (382, 147) -- (372, 147) [color=white];\n    \\draw (372, 147) -- (362, 149) -- (352, 153) [color=black];\n    \\draw (352, 153) -- (344, 158) [color=white];\n    \\draw (344, 158) -- (336, 164) -- (330, 172) [color=black];\n    \\draw (330, 172) -- (325, 180) [color=white];\n    \\draw (325, 180) -- (321, 190) -- (320, 199) [color=black];\n    \\draw (150, 100) -- (150, 110) -- (151, 119) [color=white];\n    \\draw (151, 119) -- (155, 129) [color=black];\n    \\draw (155, 129) -- (160, 137) -- (166, 145) [color=white];\n    \\draw (166, 145) -- (174, 151) [color=black];\n    \\draw (174, 151) -- (182, 156) -- (192, 160) [color=white];\n    \\draw (192, 160) -- (202, 162) [color=black];\n    \\draw (202, 162) -- (212, 162) -- (221, 160) [color=white];\n    \\draw (221, 160) -- (231, 156) [color=black];\n    \\draw (231, 156) -- (240, 151) -- (247, 145) [color=white];\n    \\draw (247, 145) -- (254, 137) [color=black];\n    \\draw (254, 137) -- (259, 129) -- (262, 119) [color=white];\n    \\draw (262, 119) -- (264, 110) [color=black];\n    \\draw (264, 110) -- (264, 100) -- (262, 90) [color=white];\n    \\draw (262, 90) -- (259, 80) [color=black];\n    \\draw (259, 80) -- (254, 72) -- (247, 64) [color=white];\n    \\draw (247, 64) -- (240, 58) [color=black];\n    \\draw (240, 58) -- (231, 53) -- (221, 49) [color=white];\n    \\draw (221, 49) -- (212, 47) [color=black];\n    \\draw (212, 47) -- (202, 47) -- (192, 49) [color=white];\n    \\draw (192, 49) -- (182, 53) [color=black];\n    \\draw (182, 53) -- (174, 58) -- (166, 64) [color=white];\n    \\draw (166, 64) -- (160, 72) [color=black];\n    \\draw (160, 72) -- (155, 80) -- (151, 90) [color=white];\n    \\draw (151, 90) -- (150, 100) [color=black];\n\\end{tikzpicture}\n")

(test-output "KOTIIN"
	     "
KONNA
30 ETEEN
90 OIKEA
30 ETEEN
KOTIIN
90 OIKEA
30 ETEEN
90 VASEN
30 ETEEN
YLÖS
KOTIIN
90 VASEN
30 ETEEN
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 230) -- (350, 230) [color=white];\n    \\draw (320, 200) -- (350, 200) -- (350, 230) [color=white];\n    \\draw (320, 200) -- (290, 200) [color=white];\n\\end{tikzpicture}\n")

(test-output "KONNA (Ylhäällä / alhaalla, 1/2)"
	     "KONNA YLÖS KONNA 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")

(test-output "KONNA (Ylhäällä / alhaalla, 2/2)"
	     "KONNA ALAS KONNA 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")

(test-output "KOTIIN (Ylhäällä / alhaalla, 1/2)"
	     "KONNA YLÖS KOTIIN 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")

(test-output "KOTIIN (Ylhäällä / alhaalla, 2/2)"
	     "KONNA ALAS KOTIIN 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-output "UUSI (Ylhäällä / alhaalla, 1/2)"
	     "KONNA YLÖS UUSI 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n\\end{tikzpicture}\n")

(test-output "UUSI (Ylhäällä / alhaalla, 2/2)"
	     "KONNA ALAS UUSI 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n\\end{tikzpicture}\n")
(test-output "KONNA (Piirtää / Pyyhkii 1/2)"
	     "KONNA PIIRRÄ KONNA 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-output "KONNA (Piirtää / Pyyhkii 2/2)"
	     "KONNA PYYHI KONNA 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-output "KOTIIN (Piirtää / Pyyhkii 1/2)"
	     "KONNA PIIRRÄ KOTIIN 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-output "KOTIIN (Piirtää / Pyyhkii 2/2)"
	     "KONNA PYYHI KOTIIN 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-output "UUSI (Piirtää / Pyyhkii 1/2)"
	     "KONNA PIIRRÄ UUSI ALAS 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-output "UUSI (Piirtää / Pyyhkii 2/2)"
	     "KONNA PYYHI UUSI ALAS 100 ETEEN KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-stack "KONNA (Sijainti)"
	     "KONNA 0 0 PAIKKA KONNA XKO YKO"
	     '(200 320))
(test-stack "KOTIIN (Sijainti)"
	     "KONNA 0 0 PAIKKA KOTIIN XKO YKO"
	     '(200 320))
(test-stack "UUSI (Sijainti)"
	     "KONNA 0 0 PAIKKA UUSI XKO YKO"
	     '(0 0))
(test-stack "KONNA (Suunta)"
	    "KONNA 123 OIKEA KONNA SUUNTA"
	    '(90.0))
(test-stack "KOTIIN (Suunta)"
	    "KONNA 123 OIKEA KOTIIN SUUNTA"
	    '(90.0))
(test-stack "UUSI (Suunta)"
	    "KONNA 123 OIKEA UUSI SUUNTA"
	    '(327.0))
(test-output "KONNA (Aikaisemmat viivat)"
	    "KONNA 100 ETEEN KONNA KUVA"
	    "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n\\end{tikzpicture}\n")
(test-output "KOTIIN (Aikaisemmat viivat)"
	    "KONNA 100 ETEEN KOTIIN KUVA"
	    "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 300) [color=white];\n\\end{tikzpicture}\n")
(test-output "UUSI (Aikaisemat viivat)"
	    "KONNA 100 ETEEN UUSI KUVA"
	    "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n\\end{tikzpicture}\n")
(test-output "KONNA (Sivusuhde)"
	    "KONNA 3 3 SIVUSUHDE KONNA 100 ETEEN KUVA 1 1 SIVUSUHDE"
	    "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 400) [color=white];\n    \\draw (320, 0) -- (320, 100) [color=white];\n\\end{tikzpicture}\n")
(test-output "KOTIIN (Sivusuhde)"
	    "KONNA 3 3 SIVUSUHDE KOTIIN 100 ETEEN KUVA 1 1 SIVUSUHDE"
	    "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 400) [color=white];\n    \\draw (320, 0) -- (320, 100) [color=white];\n\\end{tikzpicture}\n")
(test-output "UUSI (Sivusuhde)"
	    "KONNA 3 3 SIVUSUHDE UUSI ALAS 100 ETEEN KUVA 1 1 SIVUSUHDE"
	    "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 200) -- (320, 400) [color=white];\n    \\draw (320, 0) -- (320, 100) [color=white];\n\\end{tikzpicture}\n")

(test-output "SIVUSUHDE"
"
KONNA
LUO YMPYRÄ 10 KERTAA 36 ETEEN 36 OIKEA VIELÄ? VALMIS
100 200 PAIKKA
3 KERTAA 4 2 * I = SIVUSUHDE YMPYRÄ VIELÄ?
1 1 SIVUSUHDE
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \\fill [black] (0, 0) rectangle (640, 400);
    \\draw (100, 200) -- (100, 272) -- (184, 330) -- (321, 352) -- (458, 330) -- (543, 272) -- (543, 200) -- (458, 141) -- (321, 119) -- (184, 141) -- (100, 199) -- (100, 344) -- (140, 400) [color=white];
    \\draw (140, 0) -- (184, 60) -- (321, 104) -- (458, 60) -- (502, 0) [color=white];
    \\draw (502, 400) -- (543, 343) -- (543, 199) -- (458, 83) -- (321, 39) -- (184, 83) -- (100, 199) -- (100, 400) [color=white];
    \\draw (100, 0) -- (100, 15) -- (184, 190) -- (321, 257) -- (458, 190) -- (543, 15) -- (543, 0) [color=white];
    \\draw (543, 400) -- (543, 199) -- (458, 25) -- (406, 0) [color=white];
    \\draw (406, 400) -- (321, 358) -- (236, 400) [color=white];
    \\draw (236, 0) -- (184, 25) -- (100, 199) [color=white];
\\end{tikzpicture}
")

(test-output "KONNA (lyhennekomennot K Y A O V E T)"
"KONNA
100 E
90 O
Y 100 E
A
90 V
100 T
K
100 T
KUVA"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \\fill [black] (0, 0) rectangle (640, 400);
    \\draw (320, 200) -- (320, 300) [color=white];
    \\draw (420, 300) -- (420, 200) [color=white];
    \\draw (320, 200) -- (320, 100) [color=white];
\\end{tikzpicture}
")

(test-output "JANA (ruudulla)"
	     "KONNA 10 10 100 200 JANA KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \\fill [black] (0, 0) rectangle (640, 400);
    \\draw (10, 10) -- (100, 200) [color=white];
\\end{tikzpicture}\n")

(test-output "JANA (ruudun yli)"
	     "KONNA 0 0 320 800 JANA KUVA"
	     "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \\fill [black] (0, 0) rectangle (640, 400);
    \\draw (0, 0) -- (160, 400) [color=white];
    \\draw (160, 0) -- (320, 400) [color=white];
\\end{tikzpicture}\n")

(add-header "Tiedostotestit")

(test-stack "TUO"
	    "
            AVAA KISSA.BLK
            1 1 TUO
            1 2 TUO
            1 3 TUO
            "
	    '(("a" "b" "c" "d") "kissa" 3))

(test-output "AVAA ... TIETOKANTA, POIMI"

"
AVAA PAIKAT.TKA TIETOKANTA
( SADEMÄÄRÄ >= 50 ) SÄÄNTÖ LLE
( KAUPUNKI . SADEMÄÄRÄ . ) KENTÄT LLE
POIMI
"
"
Tietueita 18 kpl
Pariisi 57 
Nizza 68 
Budapest 72 
Moskova 53 
Shotsi 93 
")

(add-header "Regressiotestit")

(test-output "Sierpinski"
             "
KONNA

LUO :koko :i F

3 KERTAA
JOS :i == 0 TOSI? MUUTEN 0 KULMA :koko / 2 = :i - 1 = F JATKA
I - 1 = * 120 = KULMA
:koko JOS :i == 0 TOSI? * 2 MUUTEN JATKA = ETEEN
VIELÄ?
VALMIS

320 100 PAIKKA 150 2 F
KUVA

             "
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (320, 100) -- (395, 100) -- (357, 164) -- (319, 100) -- (394, 100) -- (469, 100) -- (432, 164) -- (394, 100) -- (357, 164) -- (432, 164) -- (394, 229) -- (357, 164) -- (319, 100) -- (469, 100) -- (544, 100) -- (507, 164) -- (469, 100) -- (544, 100) -- (619, 100) -- (582, 164) -- (544, 100) -- (507, 164) -- (582, 164) -- (544, 229) -- (507, 164) -- (469, 100) -- (394, 229) -- (469, 229) -- (432, 294) -- (394, 229) -- (469, 229) -- (544, 229) -- (507, 294) -- (469, 229) -- (432, 294) -- (507, 294) -- (469, 359) -- (432, 294) -- (394, 229) -- (319, 100) [color=white];\n\\end{tikzpicture}\n")

(test-output "Regressio 1"
             "
LUO :n :s poly :n KERTAA :s eteen 360 / :n = VASEN VIELÄ? VALMIS
LUO RESET YLÖS 90 KULMA 320 100 PAIKKA ALAS VALMIS
KONNA
10 KERTAA RESET 1 40 POLY VIELÄ?
KONNA
10 KERTAA RESET I 40 POLY VIELÄ?
10 KERTAA I . VIELÄ?
"
"1 2 3 4 5 6 7 8 9 10 ")

(test-stack "Regressio 1 (konnakomentojen korvaus)"
             "
LUO eteen- POISTA VALMIS
LUO vasen- POISTA VALMIS
LUO konna- VALMIS
LUO ylös- VALMIS
LUO alas- VALMIS
LUO paikka- POISTA POISTA VALMIS
LUO kulma- POISTA VALMIS
LUO :n :s poly :n KERTAA :s eteen- 360 / :n = vasen- VIELÄ? VALMIS
LUO RESET ylös- 90 kulma- 320 100 paikka- alas- VALMIS
konna-
10 KERTAA RESET 1 40 POLY VIELÄ?
konna-
10 KERTAA RESET I 40 POLY VIELÄ?
10 KERTAA I VIELÄ?"
'(10 9 8 7 6 5 4 3 2 1))

(test-stack "Regressio 1 (konnakomentojen poisto)"
            "
10 KERTAA 1 40 POISTA POISTA VIELÄ?
10 KERTAA I 40 POISTA POISTA VIELÄ?
10 KERTAA I VIELÄ?"
'(10 9 8 7 6 5 4 3 2 1))

(test-stack "Regressio 1 (minimaalinen esimerkki)"
            "10 KERTAA VIELÄ? 10 KERTAA I VIELÄ?"
            '(10 9 8 7 6 5 4 3 2 1))


(test-stack "Regressio 1 (minimaalinen esimerkki)"
            "10 KERTAA VIELÄ? 10 KERTAA I VIELÄ?"
            '(10 9 8 7 6 5 4 3 2 1))

(test-stack "Regressio 2, Kertaa-silmukka (parsiminen, kolme sisäkkäistä)"
            "2 KERTAA 3 KERTAA 4 KERTAA 10 VIELÄ? 3 VIELÄ? 2 VIELÄ?"
            '(2 3 10 10 10 10 3 10 10 10 10 3 10 10 10 10 2 3 10 10 10 10
 3 10 10 10 10 3 10 10 10 10))

(test-stack "Regressio 3, sisäkkäisten sulkujen ="
	    "( ( 1 ) ) ="
	    '(1))

(test-stack "Regressio 4, laskujärjestys"
	    "( 2 - 1 + 1 ) ="
	    '(2))

(test-stack "Regressio 5, muuttujat suluissa"
	    "MUUTTUJA A 2 A LLE ( A ) ="
	    '(2))

(test-output "Regressio 6, reunojen ylitykset konnatilassa"
	     "
KONNA
YLÖS 30 30 PAIKKA 180 VASEN ALAS
36 KERTAA 10 ETEEN 10 OIKEA VIELÄ?
YLÖS 320 200 PAIKKA ALAS
36 KERTAA 30 ETEEN 10 OIKEA VIELÄ?
36 KERTAA 50 ETEEN 10 OIKEA VIELÄ?
KUVA"
 "\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (30, 30) -- (29, 20) -- (28, 10) -- (24, 0) -- (24, 0) [color=white];\n    \\draw (24, 400) -- (19, 392) -- (13, 384) -- (5, 378) -- (0, 374) [color=white];\n    \\draw (640, 374) -- (637, 373) -- (627, 369) -- (617, 367) -- (607, 367) -- (598, 369) -- (588, 373) -- (579, 378) -- (572, 384) -- (565, 392) -- (561, 400) [color=white];\n    \\draw (561, 0) -- (560, 0) -- (557, 10) -- (555, 20) -- (555, 30) -- (557, 39) -- (560, 49) -- (565, 57) -- (572, 65) -- (579, 71) -- (588, 76) -- (598, 80) -- (607, 82) -- (617, 82) -- (627, 80) -- (637, 76) -- (640, 75) [color=white];\n    \\draw (0, 75) -- (5, 71) -- (13, 65) -- (19, 57) -- (24, 49) -- (28, 39) -- (29, 30) [color=white];\n    \\draw (320, 200) -- (320, 170) -- (314, 140) -- (304, 112) -- (289, 86) -- (270, 63) -- (247, 44) -- (221, 29) -- (193, 18) -- (163, 13) -- (133, 13) -- (104, 18) -- (75, 29) -- (49, 44) -- (26, 63) -- (7, 86) -- (0, 99) [color=white];\n    \\draw (640, 99) -- (632, 112) -- (622, 140) -- (617, 170) -- (617, 200) -- (622, 229) -- (632, 257) -- (640, 270) [color=white];\n    \\draw (0, 270) -- (7, 283) -- (26, 306) -- (49, 325) -- (75, 340) -- (104, 351) -- (133, 356) -- (163, 356) -- (193, 351) -- (221, 340) -- (247, 325) -- (270, 306) -- (289, 283) -- (304, 257) -- (314, 229) -- (319, 199) -- (319, 149) -- (311, 100) -- (294, 53) -- (269, 10) -- (260, 0) [color=white];\n    \\draw (260, 400) -- (237, 372) -- (198, 340) -- (155, 315) -- (108, 297) -- (59, 289) -- (9, 289) -- (0, 290) [color=white];\n    \\draw (640, 290) -- (600, 297) -- (553, 315) -- (509, 340) -- (471, 372) -- (448, 400) [color=white];\n    \\draw (448, 0) -- (439, 10) -- (414, 53) -- (397, 100) -- (388, 150) -- (388, 200) -- (397, 249) -- (414, 296) -- (439, 339) -- (471, 377) -- (497, 400) [color=white];\n    \\draw (497, 0) -- (509, 9) -- (553, 34) -- (600, 52) -- (640, 59) [color=white];\n    \\draw (0, 59) -- (9, 60) -- (59, 60) -- (108, 52) -- (155, 34) -- (198, 9) -- (210, 0) [color=white];\n    \\draw (210, 400) -- (237, 377) -- (269, 339) -- (294, 296) -- (311, 249) -- (319, 199) [color=white];\n\\end{tikzpicture}\n")

(test-output "Regressio 7, PAIKKA siirtää ilmassa"
	     "
KONNA
10 10 PAIKKA
100 ETEEN
YLÖS
20 20 PAIKKA
100 ETEEN
KUVA"

"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n    \\draw (10, 10) -- (10, 110) [color=white];\n    \\draw (20, 20) -- (20, 120) [color=white];\n\\end{tikzpicture}\n")

(test-stack "Regressio 8, XKO ja YKO eivät välitä sivusuhteesta"
	     "
KONNA
1337 1337 SIVUSUHDE
123 321 PAIKKA
XKO YKO
1 1 SIVUSUHDE
"
'(321 123))

(test-stack "Regressio 9, ASEMA NÄYTÄ ei aiheuta kaatumista kun mennään äärettömyyteen ja sen yli"
	    "
KONNA
ASEMA NÄYTÄ
40 OIKEA
10000000 ETEEN
( EI KAATUNUT )
"
'(("EI" "KAATUNUT")))

(test-output "Regressio 10, ylhäällä piirtääkin viivaa clipatessaan näytön reunan ympäri"
"
KONNA
YLÖS
1000 ETEEN
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n\\end{tikzpicture}\n")

(test-stack "Regressio 10, ylhäällä näytön reunojen ympäri clippaaminen päätyy oikeaan paikkaan"
	    "
KONNA
YLÖS 40 OIKEA
100000 ETEEN
XKO YKO
"
'(4 598))

(test-output "Regressio 11, KONNA resetoi tilan oikein"
"KONNA
18 KERTAA ALAS 10 ETEEN 10 OIKEA YLÖS 10 ETEEN 10 OIKEA VIELÄ?
KONNA
100 ETEEN
KUVA"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \\fill [black] (0, 0) rectangle (640, 400);
    \\draw (320, 200) -- (320, 300) [color=white];
\\end{tikzpicture}
")

(test-output "Regressio 12, ääkkösiä sisältävät alkiot tulostuvat ilman putkimerkkejä"
"\" KÄÄPÄ ."
"KÄÄPÄ ")

(test-stack "Regressio 13, sisäkkäiset TOSI?-lauseet"
            "
            -1 TOSI?
                -1 TOSI?
                    -1 TOSI?
                        ( TOSIHAARA )
                    MUUTEN
                        ( VALEHAARA )
                    JATKA
                JATKA
                ( TOSIHAARA2 )
            MUUTEN
                ( VALEHAARA2 )
            JATKA
            "
            '(("TOSIHAARA2") ("TOSIHAARA")))

(test-output "Regressio 14, alkuperäinen koodi"
"KONNA
200 TAAKSE
8 KERTAA 4 KERTAA 50 E 90 O VIELÄ? 50 E VIELÄ? KUVA"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \\fill [black] (0, 0) rectangle (640, 400);
    \\draw (320, 200) -- (320, 0) -- (320, 50) -- (370, 50) -- (370, 0) -- (320, 0) -- (320, 50) -- (320, 100) -- (370, 100) -- (370, 50) -- (320, 50) -- (320, 100) -- (320, 150) -- (370, 150) -- (370, 100) -- (320, 100) -- (320, 150) -- (320, 200) -- (370, 200) -- (370, 150) -- (320, 150) -- (320, 200) -- (320, 250) -- (370, 250) -- (370, 200) -- (320, 200) -- (320, 250) -- (320, 300) -- (370, 300) -- (370, 250) -- (320, 250) -- (320, 300) -- (320, 350) -- (370, 350) -- (370, 300) -- (320, 300) -- (320, 350) -- (320, 400) [color=white];
    \\draw (320, 0) -- (370, 0) -- (370, 0) [color=white];
    \\draw (370, 400) -- (370, 350) -- (320, 350) -- (320, 400) [color=white];
\\end{tikzpicture}
")

(test-output "Regressio 14, vasemman ja alapuolisen äären ylitys nollasijainnista"
"
KONNA
Ö Alareuna
320 0 PAIKKA
180 OIKEA
50 ETEEN
Ö Vasen reuna
0 200 PAIKKA
90 OIKEA
50 ETEEN
KUVA
"
"\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]
    \\fill [black] (0, 0) rectangle (640, 400);
    \\draw (320, 0) -- (320, 0) [color=white];
    \\draw (320, 400) -- (320, 350) [color=white];
    \\draw (0, 200) -- (0, 200) [color=white];
    \\draw (640, 200) -- (590, 200) [color=white];
\\end{tikzpicture}
")

(test-output "Regressio 15, TEE käyttää kutsuhetkistä nimiavaruutta"
"
LUO F ( X ) TEE VALMIS
LUO X \" TOIMIN . VALMIS
F
"
"TOIMIN ")

(add-header "Integraatiotestit")

(test-output "Mandelbrot"
             "
LUO C+ KIERRÄ + VAIHDA = KIERRÄ KIERRÄ + VAIHDA = VAIHDA VALMIS
LUO C^2 YLI YLI * VAIHDA * 2 = KIERRÄ ^ 2 = KIERRÄ ^ 2 = - VAIHDA = VAIHDA VALMIS
LUO KARKASIKO? ^ 2 = VAIHDA ^ 2 = + VAIHDA = > 4 = VALMIS
LUO SKAALAA-PISTE -20 -15 C+ / 10 = VAIHDA / 10 = VAIHDA VALMIS
 
UUSIRIVI

30 KERTAA 30 KERTAA 0 0 C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ JOS KARKASIKO? TOSI? 1 . MUUTEN 0 . JATKA VIELÄ? UUSIRIVI VIELÄ?
             "
             "
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 
1 1 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 1 1 
1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 
1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 
1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 1 0 0 0 0 1 0 0 0 0 1 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
")

(test-output "Mandelbrot (nimetyt muuttujat)"
             "
MUUTTUJA C_r
MUUTTUJA C_i
MUUTTUJA KOKO 30 KOKO LLE
MUUTTUJA ITERAATIOITA 10 ITERAATIOITA LLE

LUO :a-r :a-i :b-r :b-i C+ :a-r + :b-r = :a-i + :b-i = VALMIS
LUO :r :i C^2 :r ^ 2 - :i ^ 2 = 2 * :r * :i = VALMIS
LUO :r :i KARKASIKO? :r ^ 2 + :i ^ 2 > 4 VALMIS
LUO :r :i SKAALAA-PISTE :r - 20 = / 10 = :i - 15 = / 10 = VALMIS

UUSIRIVI

30 KERTAA 30 KERTAA 0 0 C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ C^2 I J SKAALAA-PISTE C+ JOS KARKASIKO? TOSI? 1 . MUUTEN 0 . JATKA VIELÄ? UUSIRIVI VIELÄ?
             "
             "
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 
1 1 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 1 1 
1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 
1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 
1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 0 1 0 0 0 0 1 0 0 0 0 1 0 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
")

(final-result success failed global-failure))))
