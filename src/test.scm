(import (scheme base) (scheme small) (scheme write) (sampo-test))

(define (add-success desc significant)
  (display desc)
  (display ": \x1b[0;32;40mOK\x1b[0m")
  (if (not significant)
      (display " (ei merkitsevä)")
      '())
  (newline))
(define (add-failure desc message significant)
  (display desc)
  (if significant
      (display ": \x1b[0;31;40m")
      (display ": (ei-merkitsevä) \x1b[0;33;40m"))
  (display message)
  (display "\x1b[0m")
  (newline))

(define (add-header header)
  (display header)
  (newline)
  (newline))

(define (final-result success failed global-failure)
  (display success)
  (display " onnistui, ")
  (display failed)
  (display " epäonnistui (yhteensä ")
  (display (+ success failed))
  (display ")")
  (newline)
  (if global-failure
      (begin
	(display "Vähintään yksi merkitsevä testi epäonnistui")
	(newline)
	(exit 1))
      '()))

(run-tests add-success add-failure add-header final-result)
