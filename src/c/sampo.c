#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct Point {
	double x;
	double y;
} Point;

typedef struct Chain {
	int start; // First node included in the chain
	int end; // First node excluded from the chain
	int color;
} Chain;

SDL_Window *win = NULL;
SDL_Renderer *ren = NULL;

char* font_path;
TTF_Font *font;
TTF_Font *print_font;

Point *positions = NULL;
Chain *chains = NULL;
char** prints = NULL;
Point *print_positions = NULL;

int stroke_container_size;
int chain_container_size;
int print_container_size;

int stroke_qty;
int chain_qty;
int print_qty;

int pen_is_down;
int is_drawing;
int turtle_visible;
int is_started = 0;
int is_headless = 0;
double direction;
double x_factor = 1;
double y_factor = 1;

void set_headless() {
	is_headless = 1;
}

int get_chain_qty(){
	return chain_qty;
}

int get_chain_color(int i){
	return chains[i].color;
}

int get_chain_start(int i){
	return chains[i].start;
}

int get_chain_end(int i){
	return chains[i].end;
}

int get_x_pos(int i){
	return (int) positions[i].x;
}

int get_y_pos(int i){
	return (int) positions[i].y;
}

int turtle_x_pos(){
	return (int) positions[stroke_qty - 1].x;
}

int turtle_y_pos(){
	return (int) positions[stroke_qty - 1].y;
}

int is_pen_down(){
	return pen_is_down;
}

void set_factors(double x, double y){
	x_factor = x;
	y_factor = y;
}

void grow_stroke_container(){
	Point *new_positions = (Point*) malloc(sizeof(Point)*stroke_container_size*2);
	stroke_container_size *= 2;
	for(int i = 0; i < stroke_qty; i++){
		new_positions[i].x = positions[i].x;
		new_positions[i].y = positions[i].y;
	}
	free(positions);
	positions = new_positions;
}

void grow_chain_container(){
	Chain *new_chains = (Chain*) malloc(sizeof(Chain)*chain_container_size*2);
	chain_container_size *= 2;
	for(int i = 0; i < chain_qty; i++){
		new_chains[i].start = chains[i].start;
		new_chains[i].end = chains[i].end;
		new_chains[i].color = chains[i].color;
	}
	free(chains);
	chains = new_chains;
}

void grow_print_container(){
	char** new_prints = (char**) malloc(sizeof(char*)*print_container_size*2);
	Point* new_print_positions = (Point*) malloc(sizeof(Point)*print_container_size*2);
	print_container_size *= 2;
	for(int i = 0; i < print_qty; i++){
		new_prints[i] = prints[i];
		new_print_positions[i].x = print_positions[i].x;
		new_print_positions[i].y = print_positions[i].y;
	}
	free(prints);
	free(print_positions);
	prints = new_prints;
	print_positions = new_print_positions;
}

void free_prints(){
	if(prints != NULL){
		for(int i = 0; i < print_qty; i++){
			free(prints[i]);
		}
		free(prints);
		prints = NULL;
	}
}

int start(char* cwd){

	free_prints();

	stroke_qty = 0;
	chain_qty = 0;
	print_qty = 0;
	pen_is_down = 0;
	is_drawing = 1;
	turtle_visible = 0;
	if(!is_started){
		x_factor = 1;
		y_factor = 1;
		direction = 90;
		if(!is_headless){
			int init_status = SDL_Init(SDL_INIT_VIDEO);
			if(TTF_Init() != 0){
				printf("Failed to init TTF\n");
			}
			font_path = malloc(sizeof(char)*(strlen(cwd) + 24));
			sprintf(font_path, "%sMxPlus_IBM_VGA_8x14.ttf", cwd);
			font = TTF_OpenFont(font_path, 8*2);
			print_font = TTF_OpenFont(font_path, 8);
			if(font == NULL){
				printf("Failed to load font\n");
			}
			if(init_status != 0)
				return init_status;
			win = SDL_CreateWindow("Sampo", 0, 0, 640, 400, SDL_WINDOW_SHOWN);
			if(win == NULL)
				return 1;
			ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (ren == NULL){
				return 1;
			}
		}
	}
	clear_drawing();
	is_started = 1;
	return 0;
}

void clear_drawing() {
	free_prints();
	Point current = {320, 200};
	if(stroke_qty > 0){
		current.x = positions[stroke_qty-1].x;
		current.y = positions[stroke_qty-1].y;
	}
	stroke_qty = 1;
	chain_qty = 0;
	print_qty = 0;
	stroke_container_size = 1;
	chain_container_size = 1;
	print_container_size = 1;
	if(chains != NULL) {
		free(chains);
		free(positions);
		free(print_positions);
	}
	chains = (Chain*) malloc(sizeof(Chain));
	positions = (Point*) malloc(sizeof(Point));
	print_positions = (Point*) malloc(sizeof(Point));
	prints = (char**) malloc(sizeof(char*));
	positions[0].x = current.x;
	positions[0].y = current.y;
}

void set_direction(double new_direction){
	direction = fmod(new_direction, 360.0);
	if(direction < 0) direction += 360.0;
}

void alter_direction(double change){
	direction += change;
	direction = fmod(direction, 360.0);
	if(direction < 0) direction += 360.0;
}

double get_direction(){
	return direction;
}

// Undefined if stroke_qty = 0

void forwards(double distance){
	Point delta;
	delta.x = cos(direction * M_PI / 180) * x_factor*distance;
	delta.y = sin(direction * M_PI / 180) * y_factor*distance;
	move_to(delta.x + positions[stroke_qty-1].x, delta.y + positions[stroke_qty-1].y);
}

void allocate_new_chain(){
	if(chain_qty == chain_container_size) grow_chain_container();
	chains[chain_qty].start = stroke_qty-2; // Include previous point
	chains[chain_qty].end = stroke_qty;
	chains[chain_qty].color = is_drawing;
	chain_qty++;
}

void continue_chain(){
	chains[chain_qty-1].end++;
}

void push_stroke(double x, double y){

	// A new stroke is always allocated

	if(stroke_qty == stroke_container_size) grow_stroke_container();
	positions[stroke_qty].x = x;
	positions[stroke_qty].y = y;
	stroke_qty++;

	// Logic on updating chains:
	// - If pen is up, do nothing
	// - If the chain_qty == 0
	//   allocate a new chain
	// - If the chain_qty > 0
	//   - The color is the same
	//       - If we have an existing chain, continue it
	//       - If we don't have an existing chain, allocate a new one
	//   - The color is different
	//       - Allocate a new chain


	if(pen_is_down){
		if(chain_qty == 0){
			allocate_new_chain();
		}else if(chains[chain_qty-1].color == is_drawing){
			if(chains[chain_qty-1].end == stroke_qty-1){
				continue_chain();
			}else{
				allocate_new_chain();
			}
		}else{
			allocate_new_chain();
		}
	}
}

void move_to(double x, double y){
	Point prev = {0, 0};

	if(stroke_qty > 0){
		prev.x = positions[stroke_qty-1].x;
		prev.y = positions[stroke_qty-1].y;
	}

	Point delta = {x - prev.x, y - prev.y};

	if(pen_is_down){
		while(prev.x + delta.x < 0 || prev.x + delta.x >= 640 || prev.y + delta.y < 0 || prev.y + delta.y >= 400){
			// How much do we have to travel to hit various bounds
			// The factors can be read as prev + k_top * delta = x where x is a point on the top edge
			// If a factor is negative, then we are travelling away from that edge, ie. we will cross some other edge
			double k_top = (400 - prev.y) / (delta.y);
			double k_bottom = (0 - prev.y) / (delta.y);
			double k_right = (640 - prev.x) / (delta.x);
			double k_left = (0 - prev.x) / (delta.x);
			// prev.x \in ]0, 640] and prev.y \in ]0, 400]
			// Both of the pairs (k_top, k_bottom) and (k_right, k_left) have at least one non-negative element
			// (That is, assuming that delta.x and delta.y are non-negative)
			// We will be crossing a horizontal bound iff:
			// * delta.y == 0 => we are travelling dead horizontally
			// * max(k_top, k_bottom) >= max(k_top, k_bottom): the horizontal bound comes at worst at the same time as the vertical
			// (both delta.x and delta.y can't be zero at the same time, because of the bounds on prev and the loop condition)
			if(delta.y == 0 || fmax(k_top, k_bottom) >= fmax(k_left, k_right)){
				// If it's the left bound that we are crossing first
				// (The case k_left == 0 && k_right == 0 is not possible, lower and upper edges don't cross)
				if(delta.x < 0){
					push_stroke(0, k_left*delta.y + prev.y);
					penup();
					push_stroke(640, k_left*delta.y + prev.y);
					pendown();
					delta.x += prev.x;
					double delta_y_copy = delta.y;
					delta.y -= k_left*delta.y;
					prev.x = 640;
					prev.y = k_left*delta_y_copy + prev.y;
				}else{
					push_stroke(640, k_right*delta.y + prev.y);
					penup();
					push_stroke(0, k_right*delta.y + prev.y);
					pendown();
					delta.x -= 640 - prev.x;
					double delta_y_copy = delta.y;
					delta.y -= k_right*delta.y;
					prev.x = 0;
					prev.y = k_right*delta_y_copy + prev.y;
				}
			}else{
				if(delta.y < 0){
					push_stroke(k_bottom*delta.x + prev.x, 0);
					penup();
					push_stroke(k_bottom*delta.x + prev.x, 400);
					pendown();
					delta.y += prev.y;
					double delta_x_copy = delta.x;
					delta.x -= k_bottom*delta.x;
					prev.y = 400;
					prev.x = k_bottom*delta_x_copy + prev.x;
				}else{
					push_stroke(k_top*delta.x + prev.x, 400);
					penup();
					push_stroke(k_top*delta.x + prev.x, 0);
					pendown();
					delta.y -= 400 - prev.y;
					double delta_x_copy = delta.x;
					delta.x -= k_top*delta.x;
					prev.y = 0;
					prev.x = k_top*delta_x_copy + prev.x;
				}
			}
		}
	}
	if(fabs(delta.x) > 0.001 || fabs(delta.y) > 0.001){
		push_stroke(fmod(prev.x + delta.x, 640), fmod(prev.y + delta.y, 400));
	}
}

void update_render(){
	if(is_headless) return;
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	SDL_Point *points = malloc(sizeof(SDL_Point) * stroke_qty);
	for(int i = 0; i < stroke_qty; i++){
		points[i].x = positions[i].x;
		points[i].y = 400 - positions[i].y;
	}
	for(int i = 0; i < chain_qty; i++){
		if(chains[i].color){
			SDL_SetRenderDrawColor(ren, 255, 255, 255, 1);
		}else{
			SDL_SetRenderDrawColor(ren, 0, 0, 0, 1);
		}
		SDL_RenderDrawLines(ren, points+chains[i].start, chains[i].end - chains[i].start);
	}
	free(points);

	SDL_SetRenderDrawColor(ren, 255, 255, 255, 1);

	if(stroke_qty > 0 && pen_is_down){
		double x_i = -sin(direction * M_PI / 180)*7;
		double x_j = cos(direction * M_PI / 180)*7;
		double y_i = -cos(direction * M_PI / 180)*20;
		double y_j = sin(direction * M_PI / 180)*20;
		double tip_x = positions[stroke_qty-1].x;
		double tip_y = positions[stroke_qty-1].y;
		SDL_RenderDrawLine(ren, tip_x, 400-tip_y, tip_x + x_i + y_i, 400-tip_y + y_j - x_j);
		SDL_RenderDrawLine(ren, tip_x, 400-tip_y, tip_x - x_i + y_i, 400-tip_y + y_j + x_j);
		SDL_RenderDrawLine(ren, tip_x + x_i + y_i, 400-tip_y + y_j - x_j, tip_x - x_i + y_i, 400-tip_y + y_j + x_j);
	}

	if(turtle_visible){
		char position_string[30]; // "X = " (4) + 4 + " Y = " (5) + 4 + " KULMA = " (7) + 4 nul = 29
		sprintf(position_string, "X = %d Y = %d KULMA = %d", turtle_x_pos(), turtle_y_pos(), ((int) get_direction()));
		int text_width;
		int text_height;
		SDL_Surface *surface;
		SDL_Rect rect;
		SDL_Color textColor = {255, 255, 255, 0};
		surface = TTF_RenderText_Solid(font, position_string, textColor);
		SDL_Texture *texture = SDL_CreateTextureFromSurface(ren, surface);
		text_width = surface->w;
		text_height = surface->h;
		SDL_FreeSurface(surface);
		rect.x = 10;
		rect.y = 10;
		rect.w = text_width;
		rect.h = text_height;

		SDL_RenderCopy(ren, texture, NULL, &rect);
	}
	for(int i = 0; i < print_qty; i++){
		int text_width;
		int text_height;
		SDL_Surface *surface;
		SDL_Rect rect;
		SDL_Color textColor = {255, 255, 255, 0};
		surface = TTF_RenderText_Solid(print_font, prints[i], textColor);
		SDL_Texture *texture = SDL_CreateTextureFromSurface(ren, surface);
		text_width = surface->w;
		text_height = surface->h;
		SDL_FreeSurface(surface);
		rect.x = print_positions[i].x;
		rect.y = 400 - print_positions[i].y;
		rect.w = text_width;
		rect.h = text_height;

		SDL_RenderCopy(ren, texture, NULL, &rect);
	}
	SDL_RenderPresent(ren);
}

void quit(){
	if(!is_headless){
		TTF_CloseFont(font);
		TTF_CloseFont(print_font);
		SDL_DestroyWindow(win);
		SDL_DestroyRenderer(ren);
		TTF_Quit();
		SDL_Quit();
	}
	free(positions);
}

void penup(){
	pen_is_down = 0;
}

void pendown(){
	pen_is_down = 1;
}

void draw(){
	is_drawing = 1;
}

void _erase(){
	is_drawing = 0;
}

void show_turtle(){
	turtle_visible = 1;
}

void hide_turtle(){
	turtle_visible = 0;
}

void draw_segment(double x1, double y1, double x2, double y2){
	Point prev = {0, 0};
	if(stroke_qty > 0){
		prev.x = positions[stroke_qty-1].x;
		prev.y = positions[stroke_qty-1].y;
	}
	int pen_is_down_prev = pen_is_down;
	penup();
	move_to(x1, y1);
	pendown();
	move_to(x2, y2);
	penup();
	move_to(prev.x, prev.y);
	pen_is_down = pen_is_down_prev;
}

// It is the caller's responsibility to free text, therefore it gets copied manually
// This is defined as (free string) in the stub file

void draw_text(double x, double y, char *text) {
	if(print_qty == print_container_size) grow_print_container();
	prints[print_qty] = malloc(sizeof(char)*(strlen(text)+1));
	strcpy(prints[print_qty], text);
	print_positions[print_qty].x = x;
	print_positions[print_qty].y = y;
	print_qty++;
}
