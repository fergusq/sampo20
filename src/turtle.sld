(define-library (turtle)
		(import (scheme base) (scheme write) (srfi 144) (srfi 98))
		(include-shared "libsampo")
		(export turtle-update-render
			turtle-penup
			turtle-pendown
			turtle-erase
			turtle-draw
			turtle-forwards
			turtle-show-turtle
			turtle-hide-turtle
			turtle-get-angle
			turtle-move
			turtle-draw-segment
			turtle-draw-text
			turtle-set-direction
			turtle-set-factors
			turtle-home
			turtle-new
			turtle-started?
			turtle-alter-direction
			turtle-x-pos
			turtle-y-pos
                        turtle-clear-position
                        turtle-clear-direction
                        turtle-clear-drawing
			emit-stroke
			emit-tikz
			set_headless)
		(begin
			(define (iota i)
			    (iota_ (- i 1) '()))
			(define (iota_ n e)
			  (if (< n 0)
			    e
			    (iota_ (- n 1) (cons n e))))
			(define turtle-start-state #f)
			(define (turtle-started?)
			  turtle-start-state)
			(define (turtle-update-render) (update_render))
			(define (turtle-penup) (penup))
			(define (turtle-pendown) (pendown))
			(define (turtle-erase) (_erase))
			(define (turtle-draw) (draw))
			(define (turtle-forwards x) (forwards (flonum x)))
			(define (turtle-show-turtle) (show_turtle))
			(define (turtle-hide-turtle) (hide_turtle))
			(define (turtle-get-angle) (get_direction))
			(define (turtle-move x y) (move_to (flonum x) (flonum y)))
			(define (turtle-draw-segment x1 y1 x2 y2) (draw_segment (flonum x1) (flonum y1) (flonum x2) (flonum y2)))
			(define (turtle-draw-text x y text) (draw_text (flonum x) (flonum y) text))
			(define (turtle-set-direction direction) (set_direction (flonum direction)))
			(define (turtle-set-factors x y) (set_factors (flonum x) (flonum y)))
			(define (turtle-home) '())
			(define (turtle-new)
			  (begin
			    (start (string-append (get-environment-variable "SAMPO_LIB") "/../"))
			    (set! turtle-start-state #t)))
			(define (turtle-alter-direction direction) (alter_direction (flonum direction)))
			(define (turtle-x-pos) (turtle_x_pos))
			(define (turtle-y-pos) (turtle_y_pos))
			(define (turtle-clear-position) (turtle-move 320 200))
			(define (turtle-clear-direction) (turtle-set-direction 90))
			(define (turtle-clear-drawing) (clear_drawing))
			(define (__get_x_pos i) (get_x_pos i))
			(define (__get_y_pos i) (get_y_pos i))
			(define (__get_chain_qty) (get_chain_qty))
			(define (__get_chain_end i) (get_chain_end i))
			(define (__get_chain_start i) (get_chain_start i))
			(define (__get_chain_color i) (get_chain_color i))
			(define (emit-stroke stroke)
			  (let ((body (cdr (apply append (map (lambda (i)
			    (list " -- " "(" (number->string (__get_x_pos i)) ", " (number->string (__get_y_pos i)) ")"))
			    (map (lambda (i)
			      (+ i (__get_chain_start stroke)))
			    (iota (- (__get_chain_end stroke) (__get_chain_start stroke))))))))
			    (color (if (eq? (__get_chain_color stroke) 1) '(" [color=white];\n") '(" [color=black];\n"))))
			    (apply string-append (append '("    \\draw ") body color))))
			(define (emit-tikz)
			  (apply string-append  
			    (append
			      '("\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n")
			        (map emit-stroke (iota (__get_chain_qty)))
				'("\\end{tikzpicture}\n"))))))
