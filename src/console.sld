(define-library (console)
		(import (scheme small) (scheme write))
		(import (srfi 98))
		(import (srfi 130))
		(import (chibi process))
		(import (chibi temp-file))
		(export
		  clear
		  tab
		  edit-frame
		  stack-show
		  stack-hide)
		(begin
		  (define (collect-till-eof)
		    (let ((line (read-line)))
		      (if (eof-object? line)
		        '()
			(cons line (collect-till-eof)))))
		  (define (clear)
		    (display "\x1B[H\x1B[J"))
		  (define (tab n)
		    (display (string-append "\x1B[" (number->string (+ 1 n)) "G")))
		  (define (edit-frame frame)
		    (with-output-to-file "/tmp/sampo-korjaa-tmp"
		      (lambda ()
		        (display (string-join (vector->list frame) "\n"))))
		    (system
		      (let ((potential (get-environment-variable "EDITOR")))
		        (if potential potential "nano"))
		      "/tmp/sampo-korjaa-tmp")
		      (with-input-from-file "/tmp/sampo-korjaa-tmp"
		        (lambda ()
		            (list->vector (collect-till-eof)))))
		  (define (stack-hide)
		    '())
		  (define (stack-show stack)
		    (map (lambda (k)
		      (display k)
		      (newline)) stack)
		    (display "Pinon pohja")
		    (newline))))
