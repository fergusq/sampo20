(define-library (util)
		(import (scheme base) (scheme write))
		(export
		  xml-bleach
		  construct-call
		  js-bleach)
		(begin
		  (define (xml-bleach str)
		    (apply string-append (map
		      (lambda (k)
		        (cond
			  ((eq? k #\") "&quot;")
			  ((eq? k #\') "&apos;")
			  ((eq? k #\&) "&amp;")
			  ((eq? k #\<) "&lt;")
			  ((eq? k #\>) "&gt;")
			  (else (list->string (list k)))))
			  (string->list str))))

		      (define (js-bleach__ k)
		        (cond
			  ((string? k)
			    (display "`")
			    (display 
			      (apply string-append (map
			        (lambda (k)
			          (cond
				    ((eq? k #\`) "\\`")
				    ((eq? k #\\) "\\\\")
				    (else (list->string (list k)))))
				    (string->list k))))
			    (display "`"))
			  ((boolean? k)
			    (display (if k "true" "false")))
			  ((number? k)
			    (if (integer? k)
			      (display k)
			      (display (inexact k))))
			  ((list? k)
			    (display "[")
			    (map (lambda (x)
			      (js-bleach__ x)
			      (display ",")) k)
			    (display "]"))
			  (else
			    (display "ARVOA '")
			    (display k)
			    (display "' ei saa muunnettua puhtaasti")
			    )))
		      (define (js-bleach k)
		        (let ((output-port (open-output-string)))
			  (parameterize ((current-output-port output-port))
			    (js-bleach__ k))
			  (get-output-string output-port)))
		      (define (construct-call function arguments)
		        (let ((output-port (open-output-string)))
			  (parameterize ((current-output-port output-port))
			    (display function)
			      (display "(")
			      (map (lambda (k)
			            (js-bleach__ k)
				    (display ",")) arguments)
			      (display ")"))
		            (get-output-string output-port)))))
