(define-library (turtle)
		(import (scheme base) (scheme write) (chibi emscripten) (util))
		(export turtle-update-render
			turtle-penup
			turtle-pendown
			turtle-erase
			turtle-draw
			turtle-forwards
			turtle-show-turtle
			turtle-hide-turtle
			turtle-get-angle
			turtle-move
			turtle-draw-segment
			turtle-draw-text
			turtle-set-direction
			turtle-set-factors
			turtle-home
			turtle-new
			turtle-started?
			turtle-alter-direction
			turtle-x-pos
			turtle-y-pos
			turtle-clear-position
			turtle-clear-direction
			turtle-clear-drawing
			emit-stroke
			emit-tikz)
		(begin
			(define (iota i)
			    (iota_ (- i 1) '()))
			(define (iota_ n e)
			  (if (< n 0)
			    e
			    (iota_ (- n 1) (cons n e))))
			(define turtle-start-state #f)
			(define (turtle-started?)
			  turtle-start-state)
			(define (turtle-update-render)
			    (eval-script! "turtle_update_render();"))
			(define (turtle-penup) (eval-script! "penup()"))
			(define (turtle-pendown) (eval-script! "pendown()"))
			(define (turtle-erase) (eval-script! "erase()"))
			(define (turtle-draw) (eval-script! "draw()"))
			(define (turtle-forwards x)
			    (eval-script!
			      (construct-call "forwards" (list (exact (round x))))))
			(define (turtle-show-turtle) (eval-script! "show_turtle();"))
			(define (turtle-hide-turtle) (eval-script! "hide_turtle();"))
			(define (turtle-get-angle) (string->number (string-eval-script "get_direction();")))
			(define (turtle-move x y)
			    (eval-script! (string-append "move_to(new Point(" 
			        (number->string (exact (round x)))
				", "
			        (number->string (exact (round y)))
				"))")))
			(define (turtle-draw-segment x1 y1 x2 y2)
			    (eval-script! (string-append "draw_segment(new Point("
				(number->string (exact (round x1)))
				", "
				(number->string (exact (round y1)))
				"), new Point("
				(number->string (exact (round x2)))
				", "
				(number->string (exact (round y2)))
				"))")))
			(define (turtle-draw-text x y text)
			    (eval-script! (string-append "draw_text(new Point("
				(number->string (exact (round x)))
				", "
				(number->string (exact (round y)))
				"), " (js-bleach text) ")")))
			(define (turtle-set-direction direction)
			    (eval-script!
			      (construct-call "set_direction" (list direction))))
			(define (turtle-set-factors x y)
			    (eval-script!
			      (construct-call "set_factors" (list x y))))
			(define (turtle-home)
			    (eval-script! "turtle_home();"))
			(define (turtle-new)
			    (begin
			      (eval-script! "turtle_start();")
			      (set! turtle-start-state #t)))
			(define (turtle-alter-direction direction)
			    (eval-script!
			      (construct-call "alter_direction" (list direction))))
			(define (turtle-x-pos) (integer-eval-script "turtle_x_pos();"))
			(define (turtle-y-pos) (integer-eval-script "turtle_y_pos();"))
			(define (turtle-clear-position) (eval-script! "turtle_clear_position();"))
			(define (turtle-clear-direction) (eval-script! "turtle_clear_direction();"))
			(define (turtle-clear-drawing) (eval-script! "turtle_clear_drawing();"))
			(define (__get_x_pos i) (integer-eval-script
			  (construct-call "get_x_pos" (list i))))
			(define (__get_y_pos i) (integer-eval-script
			  (construct-call "get_y_pos" (list i))))
			(define (__get_chain_qty) (integer-eval-script (string-append "get_chain_qty();")))
			(define (__get_chain_end i) (integer-eval-script
			  (construct-call "get_chain_end" (list i))))
			(define (__get_chain_start i) (integer-eval-script
			  (construct-call "get_chain_start" (list i))))
			(define (__get_chain_color i) (integer-eval-script
			  (construct-call "get_chain_color" (list i))))

			(define (emit-stroke stroke)
			  (let ((body (cdr (apply append (map (lambda (i)
			    (list " -- " "(" (number->string (__get_x_pos i)) ", " (number->string (__get_y_pos i)) ")"))
			    (map (lambda (i)
			      (+ i (__get_chain_start stroke)))
			    (iota (- (__get_chain_end stroke) (__get_chain_start stroke))))))))
			    (color (if (eq? (__get_chain_color stroke) 1) '(" [color=white];\n") '(" [color=black];\n"))))
			    (apply string-append (append '("    \\draw ") body color))))
			(define (emit-tikz)
			  (apply string-append  
			    (append
			      '("\\begin{tikzpicture}[x=1pt,y=1pt,scale=0.5]\n    \\fill [black] (0, 0) rectangle (640, 400);\n")
			        (map emit-stroke (iota (__get_chain_qty)))
				'("\\end{tikzpicture}\n"))))))
