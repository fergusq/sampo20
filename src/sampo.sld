(define-library (sampo)
	(import (scheme base)
		(scheme write)
		(scheme file)
		(scheme char)
		(scheme inexact)
		(srfi 1)
		(srfi 27)
		(srfi 130)
		(srfi 132)
		(srfi 144)
		(chibi filesystem)
		(turtle)
		(console))
	(export sampo-repl
		initial-state
		read-code
		state-set-words
		state-stack
                state-call-stack
		run-words
		sampo-reader
		sampo-prompt
		sampo-prompt-partial
		sampo-response
		to-string)
	(begin

; Tulkin tilaa esittää state-olio

(define-record-type <state> (make-state words vocab stack call-stack stack-shown root-vocab) state?
   (words state-words state-words-set!)
   (vocab state-vocab state-vocab-set!)
   (stack state-stack state-stack-set!)
   (call-stack state-call-stack state-call-stack-set!)
   (stack-shown state-stack-shown state-stack-shown-set!)
   (root-vocab state-root-vocab state-root-vocab-set!))

(define-record-type <variable-reference> (make-variable-reference symbol value) variable-reference?
   (symbol variable-reference-symbol variable-reference-symbol-set!)
   (value variable-reference-value variable-reference-value-set!))

(define-record-type <fact> (make-fact value) fact?
   (value fact-value fact-value-set!))

; Tuottaa uuden assosiaatiolistan, joka sisältää avaimelle key arvon value. Ei kasvata ellei ole välittömän pakko

(define (assq-set alist key value)
(cond ((eq? alist '())
  `((,key . ,value)))
 ((eq? (caar alist) key)
  (cons `(,key . ,value) (cdr alist)))
 (else (cons (car alist) (assq-set (cdr alist) key value)))))

(define (take predicate data)
(if (or (equal? data '())
   (not (predicate (car data))))
'()
(cons (car data) (take predicate (cdr data)))))

(define (drop predicate data)
(if (or (equal? data '())
   (not (predicate (car data))))
data
(drop predicate (cdr data))))

(define (drop-ndth l n)
  (if (eq? n 0)
    (cdr l)
    (cons (car l) (drop-ndth (cdr l) (- n 1)))))

(define (fold-left f state data)
(if (eq? data '())
state
(fold-left f (f state (car data)) (cdr data))))

; Ajatus (apply or ...)

(define (any k)
(if (eq? k '())
#f
(or (car k) (any (cdr k)))))

; Apufunktioita

(define (state-copy state)
   (make-state (state-words state)
               (state-vocab state)
               (state-stack state)
               (state-call-stack state)
	       (state-stack-shown state)
               (state-root-vocab state)))

(define (state-set-words state words)
   (let ((copy (state-copy state)))
        (state-words-set! copy words)
        copy))

(define (state-words-head state)
   (car (state-words state)))

(define (state-words-head-lookahead! state)
(let ((next (state-words-head state)))
(cond
 ((procedure? next)
  (begin
     (state-words-set! state (next (sampo-prompt-partial)))
     (state-words-head-lookahead! state)))
 (else next))))

(define (state-words-rest state)
   (cdr (state-words state)))

(define (state-words-pop state)
   (state-set-words state (state-words-rest state)))

(define (state-set-vocab state vocab)
   (let ((copy (state-copy state)))
        (state-vocab-set! copy vocab)
        copy))

(define (state-set-stack state stack)
   (let ((copy (state-copy state)))
        (state-stack-set! copy stack)
        copy))

(define (state-set-call-stack state call-stack)
   (let ((copy (state-copy state)))
        (state-call-stack-set! copy call-stack)
        copy))

(define (state-set-stack-shown state shown)
   (let ((copy (state-copy state)))
        (state-stack-shown-set! copy shown)
	copy))

(define (state-stack-head state)
   (let ((stack (state-stack state)))
        (if (eq? stack '())
            (begin
               (display "Pinon alivuoto. ")
               (display "Kutsupino: ")
               (display (state-call-stack state))
               (newline)
               0)
            (car (state-stack state)))))

(define (state-set-root-vocab state root-vocab)
    (let ((copy (state-copy state)))
          (state-root-vocab-set! copy root-vocab)
          copy))

(define (state-stack-rest state)
   (let ((stack (state-stack state)))
        (if (eq? stack '())
            (begin
               (display "Pinon alivuoto. ")
               (display "Kutsupino: ")
               (display (state-call-stack state))
               (newline)
               '())
            (cdr (state-stack state)))))

(define (state-stack-push state x)
   (state-set-stack state (cons x (state-stack state))))

(define (state-stack-pop state)
   (state-set-stack state (state-stack-rest state)))

(define (state-call-stack-push state x)
   (state-set-call-stack state (cons x (state-call-stack state))))

(define (state-call-stack-pop state)
   (state-set-call-stack state (cdr (state-call-stack state))))

(define (state-set-relevant-vocab state vocab c)
   (if (equal? (state-call-stack state) c)
       (begin
           (state-set-root-vocab state vocab))
       (begin
           (state-set-vocab state vocab))))

(define (state-relevant-vocab state c)
   (if (equal? (state-call-stack state) c)
       (state-root-vocab state)
       (state-vocab state)))

; Tietue sanoja varten

(define-record-type <word> (make-word token word) word?
(token word-token word-token-set!)
(word word-word word-word-set!))

(define (word->symbol word)
(cond
((word? word) (word-word word))
((number? word) (string->symbol (number->string word)))
(else word)))

(define (word->token word)
(if (word? word)
(word-token word)
word))

; Säiemakro lainattu Clojuresta

(define-syntax ->
(syntax-rules ()
((_ obj) obj)
((_ obj (f . as) . fs)
(-> (f obj . as) . fs))))

(define-syntax typed-pop
(syntax-rules ()
((_ word s s2 () . body)
(let ((s2 s)) . body))
((_ word s s2 ((var check expectation) . vars) . body)
(if (eq? s '())
  (begin
    (display "Virhe sanan ")
    (display (to-string (quote word)))
    (display " kutsussa: haluttiin parametriin ")
    (display (to-string (quote var)))
    (display " ")
    (display expectation)
    (display ", mutta pinon pohja tuli vastaan.")
    (newline)
    '())
  (let ((var (car s)))
    (if (check var)
      (typed-pop word (cdr s) s2 vars . body)
      (begin
        (display "Virhe sanan ")
        (display (to-string (quote word)))
        (display " kutsussa: haluttiin parametriin ")
        (display (to-string (quote var)))
        (display " ")
        (display expectation)
        (display ", saatiin ")
        (display (printable-type var))
        (display " '")
        (display (sampo-output var))
        (display "'.")
        (newline)
	(cdr s))))))))

(define-syntax with-turtle
(syntax-rules ()
((_ command with-branch without-branch)
(if (turtle-started?)
  with-branch
  (begin
    (display "Konnakomentoa ")
    (display (to-string (quote command)))
    (display " kutsuttu ilman, että konna alustettiin.")
    (newline)
    without-branch)))))

(define-syntax with-file
(syntax-rules ()
((_ command with-branch without-branch)
(if (vector? (vector-ref current-blk 0))
  with-branch
  (begin
    (display "Tiedostokomentoa ")
    (display (to-string (quote command)))
    (display " kutsuttu ilman, että on avointa tiedostoa")
    (newline)
    without-branch)))))

(define-syntax with-frame
(syntax-rules ()
((_ command with-branch without-branch)
(if (vector? current-frame)
  with-branch
  (begin
    (display "Ruutukomentoa ")
    (display (to-string (quote command)))
    (display " kutsuttu ilman, että on aktiivista ruutua")
    (newline)
    without-branch)))))

(define-syntax with-database
(syntax-rules ()
((_ command with-branch without-branch)
(if (not (eq? current-database '()))
  with-branch
  (begin
    (display "Tietokantakomentoa ")
    (display (to-string (quote command)))
    (display " kutsuttu ilman, että on aktiivista tietokantaa")
    (newline)
    without-branch)))))

(define (remove-last l)
(if (null? l)
'()
(reverse (cdr (reverse l)))))

(define (seq n)
   (fill-vector 1 n (make-vector n)))

(define (fill-vector i n vec)
   (vector-set! vec (- i 1) i)
   (if (= i n)
       vec
       (fill-vector (+ i 1) n vec)))

(define-syntax repeat
   (syntax-rules ()
      ((_ n i . body) (repeat-call n (lambda (i) . body)))))

(define (repeat-call n f)
   (if (= n 0)
       '()
       (cons (f n) (repeat-call (- n 1) f))))

; read-only: doesn't touch the stack or vocabulary

(define (read-only-cmd f)
(lambda (state return) (begin (f state) (return state))))

(define (string->sampo-number str)
   (string->number (string-map (lambda (k)
                                  (if (member k '(#\, #\/)) #\. k))
                               str)))

(define (safe-read message default)
  (let ((result ((sampo-reader) message)))
    (if (eq? result (eof-object))
	(begin
	  (display "Syötettä ei ole.")
	  (newline)
	  default)
	result)))

(define (read-number message)
   (let ((result (car (safe-read (string-append (sampo-output message) " ") (list (make-word "0" 0))))))
        (if (and result (number? (word-word result)))
            (word-word result)
            (read-number message))))

(define (read-word message)
   (let ((result (car (safe-read (string-append (sampo-output message) " ") (list (make-word "eof" 'eof))))))
        (if result (word-token result) (read-word message))))

(define (query-list message state return)
   (let* ((words (safe-read (string-append (sampo-output message) " ") (list (make-word "(" '|(|) (make-word ")" '|)|))))
          (state2 (state-set-words state words)))
         (run-words state2 (lambda (state3)
                              (let* ((input (state-stack-head state3))
                                     (state4 (-> state3
                                                 (state-stack-pop)
                                                 (state-set-words (state-words state)))))
                                    (if (list? input)
                                        (return state4 input)
                                        (query-list message state4 return)))))))

(define (parse-list ws return)
   (parse-block '(|(|) '|)| ws
	(lambda (block ws2)
	   (let loop ((words block)
		      (stack '()))
		(cond
		 ((eq? words '())
		  (return ws2 (reverse stack)))
		 ((eq? (word-word (car words)) '|(|)
		  (parse-list (cdr words) (lambda (words2 stack2)
					     (loop words2 (cons stack2 stack)))))
		 (else (loop (cdr words) (cons (word->token (car words)) stack))))))))

(define (push-cmd t)
(lambda (state return)
(return (state-stack-push state t))))

(define (stack-cmd f)
(lambda (state return)
(return (state-set-stack state (f (state-stack state))))))

(define (if-cmd condition)
   (lambda (state return)
     (parse-block '(TOSI? VALE?) 'JATKA (state-words state)
                  (lambda (block ws2)
                     (parse-block-with-depth 0 '(TOSI? VALE?) '(JATKA) 'MUUTEN #f block
                                  (lambda (true-branch false-branch)
                                     (run-words (state-set-words state (list (make-word "=" '=)))
                                                (lambda (post-condition-state)
                                                   (let ((branch (if (condition (state-stack-head post-condition-state)) true-branch false-branch)))
								      (run-words (-> post-condition-state
										     (state-stack-pop)
										     (state-set-words branch))
										 (lambda (post-if-state)
										    (return (state-set-words post-if-state ws2)))))))))))))

(define (display-words words)
   (cond
    ((eq? words '()) )
    ((procedure? (car words)) (begin (display "↲") (display-words (cdr words))))
    ((word? (car words)) (begin (display (word-word (car words))) (display " ") (display-words (cdr words))))
    (else (begin (display (car words)) (display " ") (display-words (cdr words))))))

(define (to-string value)
    (cond
      ((number? value)
        (number->string value))
      ((string? value)
        value)
      ((symbol? value)
        (symbol->string value))
      ((list? value)
        (string-append "( "
	  (apply string-append (map (lambda (k)
	    (string-append (to-string k) " ")) value))
	  ")"))
      ((variable-reference? value)
        (symbol->string (variable-reference-symbol value)))))

(define (printable-type value)
    (cond
      ((exact-integer? value)
        "kokonaisluku")
      ((real? value) ; Ei-liukuluvut nappautuvat exact-integeriin
        "liukuluku")
      ((string? value)
        "alkio tai fakta")
      ((symbol? value)
        "sisäinen symboli")
      ((list? value)
        "lista")
      ((variable-reference? value)
        "muuttujaviittaus")))

; last-pair (ei löydy r7rs:stä)

(define (last-pair l)
(if (eq? (cdr l) '())
l
(last-pair (cdr l))))

; exact->inexact (ei löydy r7rs:stä)

(define (exact->inexact x)
(+ 0.0 x))

(define (sampo-output value)
   (cond
    ((inexact? value)
     (string-map (lambda (c)
                    (if (eq? c #\.) #\, c))
                 (number->string value)))
    ((variable-reference? value)
     (symbol->string (variable-reference-symbol value)))
    ((fact? value)
     (string-append "FAKTA " (fact-value value)))
    ((symbol? value)
     (string-append "sisäinen symboli '" (symbol->string value) "'"))
    (else (to-string value))))

(define (sampo-equal? a b)
   (if (and (variable-reference? a)
            (variable-reference? b))
       (sampo-equal? (variable-reference-value a)
                     (variable-reference-value b))
       (equal? a b)))

(define (string?->list x)
  (if (string? x) (string->list x) x))

(define (list-or-string? x)
  (or (list? x) (string? x)))

(define (nonempty-list-or-string? x)
  (and (list-or-string? x)
       (not (eq? x '()))
       (not (equal? x ""))))

(define (string?-apply a f)
  (if (string? a)
    (let ((k (f (string->list a))))
      (if (list? k)
        (list->string k)
	(string k)))
    (f a)))

(define siemen (make-variable-reference 'SIEMEN 1337))
(define lukuteksti (make-variable-reference 'LUKUTEKSTI "LUKU:"))
(define listateksti (make-variable-reference 'LISTATEKSTI "LISTA:"))
(define alkioteksti (make-variable-reference 'ALKIOTEKSTI "SANA:"))
(define sääntö (make-variable-reference 'SÄÄNTÖ 0))
(define kentät (make-variable-reference 'KENTÄT '("KAIKKITULOSTA")))

(define previous-seed #f)
(define rng-state #f)
(define current-frame-count 10)
(define current-frame "")
(define current-frame-i 0)
(define current-blk #(""))
(define current-blk-name "")
(define last-blk #()) ; asetetaan myöhemmin sampo-blk:ksi
(define current-database '())

(define A: "A-DISK")
(define B: "B-DISK")
(define C: (current-directory))

(define vocab
        `(
        (Ö . ,(lambda (state return)
                 (let loop ((loop-state state))
                      (if (or (eq? (state-words loop-state) '()) (eq? (word->symbol (state-words-head loop-state)) '|\n|))
                          (return loop-state)
                          (loop (state-words-pop loop-state))))))
        (UUSIRIVI . ,(read-only-cmd (lambda (state)
            (display "\n"))))
	(UUSISIVU . ,(read-only-cmd (lambda (state)
	    (clear))))
	(TAB . ,(stack-cmd (lambda (s)
	  (typed-pop TAB s s2 ((n exact-integer? "kokonaisluku"))
	    (begin
	      (tab n)
	      s2)))))
	(SCHEMEDISPLAY . ,(stack-cmd (lambda (s)
	  (typed-pop SCHEMEDISPLAY s s2 ((x (lambda (k) #t) "mikä tahansa arvo"))
	    (begin
	      (display x)
	      (display " ")
	      s)))))
	(? . ,(stack-cmd (lambda (s)
	  (typed-pop ? s s2 ((x (lambda (k) #t) "mikä tahansa arvo"))
	    (begin
	      (display (sampo-output x))
	      (display " ")
	      s)))))
	(,(string->symbol ".") . ,(stack-cmd (lambda (s)
	  (typed-pop "." s s2 ((x (lambda (k) #t) "mikä tahansa arvo"))
	    (begin
	      (display (sampo-output x))
	      (display " ")
	      s2)))))
	(,(string->symbol "(") . ,(lambda (state return)
		  (parse-list (state-words state)
			      (lambda (ws2 l)
				 (return (-> state
					     (state-set-words ws2)
					     (state-stack-push l)))))))
	; pinokomennot:
	(POISTA . ,(stack-cmd (lambda (s)
	  (typed-pop POISTA s s2 ((x (lambda (k) #t) "mikä tahansa arvo")) s2))))
	(VAIHDA . ,(stack-cmd (lambda (s)
	  (typed-pop VAIHDA s s2 ((y (lambda (k) #t) "mikä tahansa arvo")
	                          (x (lambda (k) #t) "mikä tahansa arvo"))
	                        `(,x ,y . ,s2)))))
	(KIERRÄ . ,(stack-cmd (lambda (s)
	  (typed-pop KIERRÄ s s2 ((z (lambda (k) #t) "mikä tahansa arvo")
	                          (y (lambda (k) #t) "mikä tahansa arvo")
	                          (x (lambda (k) #t) "mikä tahansa arvo"))
	                        `(,x ,z ,y . ,s2)))))
	(TUPLAA . ,(stack-cmd (lambda (s)
	  (typed-pop TUPLAA s s2 ((x (lambda (k) #t) "mikä tahansa arvo"))
	                        `(,x ,x . ,s2)))))
	(YLI . ,(stack-cmd (lambda (s)
	  (typed-pop YLI s s2 ((y (lambda (k) #t) "mikä tahansa arvo")
	                       (x (lambda (k) #t) "mikä tahansa arvo"))
	                     `(,x ,y ,x . ,s2)))))
	; listakomennot:
	(KPL    . ,(stack-cmd (lambda (s)
	  (typed-pop KPL s s2 ((x list-or-string? "lista tai alkio"))
	    `(,(length (if (string? x) (string->list x) x)) . ,s2)))))
	(PÄÄ . , (stack-cmd (lambda (s)
	  (typed-pop PÄÄ s s2 ((k nonempty-list-or-string? "epätyhjä lista tai alkio"))
	    (cons (string?-apply k car) s2)))))
	(HÄNTÄ  . ,(stack-cmd (lambda (s)
	  (typed-pop HÄNTÄ s s2 ((k nonempty-list-or-string? "epätyhjä lista tai alkio"))
	    (cons (string?-apply k cdr) s2)))))
	(PERÄ . , (stack-cmd (lambda (s)
	  (typed-pop PERÄ s s2 ((k nonempty-list-or-string? "epätyhjä lista tai alkio"))
	    (cons (string?-apply k last) s2)))))
	(ALKUPÄÄ  . ,(stack-cmd (lambda (s)
	  (typed-pop ALKUPÄÄ s s2 ((k nonempty-list-or-string? "epätyhjä lista tai alkio"))
	    (cons (string?-apply k remove-last) s2)))))
	(PÄÄKSI . ,(stack-cmd (lambda (s)
	  (typed-pop PÄÄKSI s s2 ((y (lambda (s) #t) "mikä tahansa arvo")
	                          (x list-or-string? "lista tai alkio"))
				 (cons (if (string? x)
				         (if (string? y)
					   (string-append y x)
				           (begin
				             (display "Virhe sanan PÄÄKSI kutsussa: Ei-alkiota ")
					     (display (sampo-output y))
					     (display " (")
					     (display (printable-type y))
					     (display ") ei voida asettaa alkion ")
					     (display (sampo-output x))
					     (display " päähän")
					     (newline)
					     '()))
				         (if (list? y)
				           (append y x)
				           (cons y x)))
				       s2)))))
	(PERÄÄN . ,(stack-cmd (lambda (s)
	  (typed-pop PERÄÄN s s2 ((y (lambda (s) #t) "mikä tahansa arvo")
	                           (x list-or-string? "lista tai alkio"))
				  (cons (if (string? x)
				          (if (string? y)
					    (string-append x y)
				            (begin
				              (display "Virhe sanan PERÄÄN kutsussa: Ei-alkiota ")
					      (display (sampo-output y))
					      (display " (")
					      (display (printable-type y))
					      (display ") ei voida asettaa alkion ")
					      (display (sampo-output x))
					      (display " perään")
					      (newline)
					      '()))
				          (if (list? y)
				            (append x y)
				            (append x (list y))))
				          s2)))))
	(LIITÄ  . ,(stack-cmd (lambda (s)
	  (typed-pop LIITÄ s s2 ((y (lambda (s) #t) "mikä tahansa arvo")
	                         (x (lambda (s) #t) "mikä tahansa arvo"))
			       `(,(list x y) . ,s2)))))
	(KUULUU? . ,(stack-cmd (lambda (s)
	  (typed-pop KUULUU? s s2 ((y (lambda (s) #t) "mikä tahansa arvo")
	                           (x list? "lista"))
			         `(,(if (member y x) -1 0) . ,s2)))))
	(LISTAKSI . ,(stack-cmd (lambda (s)
	  (typed-pop LISTAKSI s s2 ((a string? "alkio"))
	                          `(,(list a) . ,s2)))))
	(ALKIOKSI . ,(stack-cmd (lambda (s)
	  (typed-pop ALKIOKSI s s2 ((l number? "luku"))
	                          `(,(sampo-output l) . ,s2)))))
	(LUVUKSI . ,(stack-cmd (lambda (s)
	  (typed-pop LUVUKSI s s2 ((a
	                            (lambda (k)
				      (and (string? k)
				           (string->sampo-number k)))
				    "lukua esittävä alkio"))
	                          `(,(string->sampo-number a) . ,s2)))))
	(ALKIO? . ,(stack-cmd (lambda (s)
	  (typed-pop ALKIO? s s2 ((x (lambda (k) #t) "mikä tahansa arvo"))
                                `(,(if (string? x) -1 0) . ,s2)))))
	(LISTA? . ,(stack-cmd (lambda (s)
	  (typed-pop LISTA? s s2 ((x (lambda (k) #t) "mikä tahansa arvo"))
                                `(,(if (list? x) -1 0) . ,s2)))))
	(=$  . ,(stack-cmd (lambda (s)
	  (typed-pop =$ s s2 ((b (lambda (k) #t) "mikä tahansa arvo")
	                      (a (lambda (k) #t) "mikä tahansa arvo"))
			    `(,(if (sampo-equal? a b) -1 0) . ,s2)))))
	(KERTAA . ,(lambda (pre-pop-state return)
	              (typed-pop KERTAA (state-stack pre-pop-state) post-pop-stack ((n exact-integer? "kokonaisluku"))
		      (let ((state (state-set-stack pre-pop-state post-pop-stack)))
		      (parse-block-with-depth 0 '(KERTAA) '(VIELÄ?) 'VIELÄ? #t (state-words state)
				   (lambda (block ws2)
				      (let loop ((iteration 1)
						 (loop-state (-> state
								 (state-set-words block)
								 (state-set-vocab (list `(ULOS . ,(lambda (out-state out-return)
												     (return (-> out-state
														 (state-set-words ws2) ; ws2 = silmukan jälkeiset käskyt
														 (state-set-vocab (state-vocab state)) ; resetoidaan sanasto
														 ))))                                                 
											(state-vocab state))))))
					   (if (> iteration n)
					       (return
						       (-> loop-state
							   (state-set-vocab (state-vocab state)) ; resetoidaan sanasto                                                
							   (state-set-words ws2))) ; resetoidaan käskyt                                                               
					       (run-words (state-set-vocab loop-state (cond
										       ((assq 'J (state-vocab state)) (state-vocab state))                            
										       ((assq 'I (state-vocab state)) (cons `(J . ,(push-cmd iteration)) (state-vocab state)))
										       (else (cons `(I . ,(push-cmd iteration)) (state-vocab state)))))
							  (lambda (loop-state2)                                                                                       
							     (loop (+ iteration 1) (state-set-words loop-state2 block)))))))))))) ; lisätään silmukan vartalo takaisin käskypinoon
	(TOISTA . ,(lambda (state return)
	  (parse-block '(TOISTA) 'ALKUUN (state-words state)
	    (lambda (block ws2)
	      (let loop ((loop-state (-> state
	                             (state-set-words block)
	                             (state-set-vocab (let ((cond-cmd (lambda (r)
	                                                                (lambda (loop-state2 loop-return)
	                                                                  (calculation loop-state2
	                                                                    (lambda (state-after val)
	                                                                      (if (= val r)
	                                                                        (loop-return state-after)
	                                                                        (return (-> state-after
	                                                                          (state-call-stack-pop)
	                                                                          (state-set-words ws2) ; ws2 = sulmukan jälkeiset käskyt
	                                                                          (state-set-vocab (state-vocab state)) ; resetoidaan sanasto
	                                                   )))))))))
	                                                   (cons `(YHÄ? . ,(cond-cmd -1))
	                                                   (cons `(LOPETA? . ,(cond-cmd 0))
	                               (state-vocab state))))))))
			(run-words loop-state
	                  (lambda (loop-state2)
	                    (loop (state-set-words loop-state2 block)))))))))
	(JOS . ,(lambda (state return) (return state)))
	(TOSI? . ,(if-cmd (lambda (c) (not (= c 0)))))
	(VALE? . ,(if-cmd (lambda (c) (= c 0))))
	(ON . ,(lambda (state return)
                  (let ((value (state-stack-head state))
                        (state2 (state-stack-pop state)))
                       (parse-block '(ON) 'LOPPU (state-words state2)
                                    (lambda (block ws2)
                                       (run-words (-> state2
                                                      (state-set-words block)
                                                      (state-set-vocab `((NIIN . ,(lambda (state3 return2)
                                                                                     (let ((value2 (state-stack-head state3))
                                                                                           (state4 (state-stack-pop state3)))
                                                                                          (parse-block '(NIIN) 'LOPPUUN (state-words state4)
                                                                                                       (lambda (block2 ws3)
                                                                                                          (if (sampo-equal? value value2)
                                                                                                              (run-words (-> state4
                                                                                                                             (state-set-words block2))
                                                                                                                         (lambda (state5)
                                                                                                                            (return (-> state5
                                                                                                                                        (state-set-words ws2)
                                                                                                                                        (state-call-stack-pop)
                                                                                                                                        (state-set-vocab (state-vocab state))))))
                                                                                                              (return2 (-> state4
                                                                                                                           (state-set-words ws3)))))))))
                                                                         (ELLEI . ,(lambda (state3 return) (return state3)))
                                                                         . ,(state-vocab state2))))
                                                  (lambda (state6)
                                                     (return (-> state6
                                                                 (state-set-words ws2)
                                                                 (state-set-vocab (state-vocab state)))))))))))
	(LUO . ,(lambda (state return)
		   (parse-block '(LUO) 'VALMIS (state-words state)
				(lambda (block ws2)
				   (letrec* ((is-parameter
					      (lambda (word)
						 (equal? (car (string->list (symbol->string (word->symbol word))))
							 (car (string->list ":")))))
					     (parameters (take is-parameter block))
					     (consume-parameter
					      (lambda (function-state parameter)
						 (let ((head (state-stack-head function-state)))
						      (-> function-state
							  (state-stack-pop)
							  (state-set-vocab
							   (assq-set (state-vocab function-state)
								     (word->symbol parameter)
								     (lambda (parameter-state parameter-return)
									(parameter-return (state-stack-push parameter-state head)))))))))
					     (rest (drop is-parameter block))
					     (name (word->symbol (car rest)))
					     (body (cdr rest))
					     (function (lambda (function-state function-return)
							  (let* ((scoped-state (state-set-vocab function-state new-vocab))
								 (parametrized-state (fold-left consume-parameter scoped-state (reverse parameters))))
								(run-words (state-set-words parametrized-state body)
									   (lambda (post-function-state)

									      ; (state-words function-state) clears the existing words

									      (function-return (-> post-function-state
												   (state-set-words (state-words function-state))
												   (state-set-vocab (state-vocab function-state)))))))))
					     (new-vocab (cons `(,name . ,function) (state-vocab state))))
					    (return (-> state
							(state-set-relevant-vocab new-vocab '(LUO))
							(state-set-words ws2))))))))
	(MUUTTUJA . ,(lambda (state return)
			(let ((name (word->symbol (state-words-head-lookahead! state))))
			     (return (-> state
					 (state-set-relevant-vocab (cons `(,name . ,(push-cmd (make-variable-reference name 0))) (state-relevant-vocab state '(MUUTTUJA))) '(MUUTTUJA))
					 (state-words-pop))))))
	(JONO . ,(lambda (pre-pop-state return)
	      (typed-pop JONO (state-stack pre-pop-state) post-pop-stack ((n exact-integer? "kokonaisluku"))
			(let* ((state (state-set-stack pre-pop-state post-pop-stack))
			       (name (word->symbol (state-words-head-lookahead! state)))
			       (variables (vector-map (lambda (x)
							 (make-variable-reference (string->symbol (string-append (number->string x)
														 " "
														 (symbol->string name)))
										  0))
						      (seq n))))
			      (return (-> state
					  (state-set-relevant-vocab (cons
					                     `(,name . ,(stack-cmd (lambda (s)
							        (typed-pop JONOKOMENNON-INDEKSÖINTI s s2 ((i (lambda (k)
								                                               (and (exact-integer? k)
													            (> k 0)))
													     "positiivinen kokonaisluku"))
								  `(,(vector-ref variables (- (exact i) 1)) . ,s2)))))
							   (state-relevant-vocab state '(JONO))) '(JONO))
					  (state-words-pop)))))))
	(VAKIO . ,(lambda (state return)
	       (typed-pop VAKIO (state-stack state) post-pop-stack ((x (lambda (k) #t) "mikä tahansa arvo"))
			(let ((name (word->symbol (state-words-head-lookahead! state))))
			     (return (-> state
					 (state-set-relevant-vocab (cons `(,name . ,(push-cmd x)) (state-relevant-vocab state '(VAKIO))) '(VAKIO))
					 (state-set-stack post-pop-stack)
					 (state-words-pop)))))))
	(UNOHDA . ,(lambda (state return)
			(let ((name (word->symbol (state-words-head-lookahead! state))))
			  (if (not (any
					  (map (lambda (pair)
						 (eq? (car pair) name)) (state-relevant-vocab state '(UNOHDA)))))
			      (begin
				(display "Tuntematon sana ")
				(display (symbol->string name))
				(display "!\n")
				(return (state-words-pop state)))                                                                                                     
			      (return (-> state                                                                                                                       
					  (state-set-relevant-vocab (cdr (drop (lambda (pair)                                                                                  
								     (not (eq? (car pair) name))) (state-relevant-vocab state '(UNOHDA)))) '(UNOHDA))                                              
					  (state-words-pop)))))))
	(ASEMA . ,(push-cmd 'AASEMA))
	(PINO . ,(push-cmd 'PPINO))
	(NÄYTÄ . ,(lambda (state return)
	  (typed-pop NÄYTÄ (state-stack state) s2 ((command symbol? "sisäinen symboli"))
		        (cond
			  ((eq? command 'AASEMA)
			    (with-turtle NÄYTÄ
			      (begin
			        (turtle-show-turtle)
			        (turtle-update-render)
			        (return (state-set-stack state s2)))
			      (return state)))
			  ((eq? command 'PPINO)
			    (return
			      (state-set-stack (state-set-stack-shown state #t) s2)))
			  (else
			    (display "Mikä?")
			    (newline)
			    (display "argumentin pitää olla joko PINO tai ASEMA")
			    (newline)
			    (return (state-set-stack state s2)))))))
	(PIILOTA . ,(lambda (state return)
	  (typed-pop PIILOTA (state-stack state) s2 ((command symbol? "sisäinen symboli"))
		        (cond
			  ((eq? command 'AASEMA)
			    (with-turtle PIILOTA
			      (begin
			        (turtle-hide-turtle)
			        (turtle-update-render)
			        (return (state-set-stack state s2)))
			      (return state)))
			  ((eq? command 'PPINO)
			    (return
			      (state-set-stack (state-set-stack-shown state #f) s2)))
			  (else
			    (display "Mikä?")
			    (newline)
			    (display "argumentin pitää olla joko PINO tai ASEMA")
			    (newline)
			    (return (state-set-stack state s2)))))))
	(ARVO . ,(stack-cmd (lambda (s)
	  (typed-pop ARVO s s2 ((v variable-reference? "muuttujaviittaus"))
		    (cons (variable-reference-value v) s2)))))
	(LLE . ,(stack-cmd (lambda (s)
	  (typed-pop LLE s s2
	    ((v variable-reference? "muuttujaviittaus")
	     (x (lambda (k) #t) "mikä tahansa arvo"))
	    (begin
	      (variable-reference-value-set! v x)
	      s2)))))
	(= .      ,(lambda (state return)
		      (calculation state (lambda (state2 val)
					    (return (state-stack-push state2 val))))))
	(+ . ,(push-cmd '+))
	(- . ,(push-cmd '-))
	(* . ,(push-cmd '*))
	(/ . ,(push-cmd '/))
	(MOD . ,(push-cmd 'MOD))
	(^ . ,(push-cmd '^))
	(== . ,(push-cmd '==))
	(<> . ,(push-cmd '<>))
	(< . ,(push-cmd '<))
	(<= . ,(push-cmd '<=))
	(> . ,(push-cmd '>))
	(>= . ,(push-cmd '>=))
	(TAI . ,(push-cmd 'TAI))
	(JA . ,(push-cmd 'JA))
	(EI . ,(stack-cmd (lambda (s)
	  (cons 'EI (cons 0 s)))))
	(SATTUMA . ,(stack-cmd (lambda (s)
	  (typed-pop SATTUMA s s2 ((n exact-integer? "kokonaisluku")) `(,
	    (begin
	      (if (eq? (variable-reference-value siemen) previous-seed)
	        '()
		(begin
		  (set! previous-seed (variable-reference-value siemen))
		  (let ((source (make-random-source)))
		    (begin
		      (random-source-pseudo-randomize! source 1 previous-seed)
		      (set! rng-state (random-source-make-integers source))))))
	      (+ (rng-state n) 1))
	  . ,s2)))))
	(INT . ,(stack-cmd (lambda (s)
	  (typed-pop INT s s2 ((l number? "luku")) `(,(exact (floor l)) . ,s2)))))
	(MERKIKSI . ,(stack-cmd (lambda (s)
	  (typed-pop MERKIKSI s s2 ((l number? "luku"))
	    `(,(list->string (list (integer->char l))) . ,s2)))))
	(ASCIIKSI . ,(stack-cmd (lambda (s)
	  (typed-pop ASCIIKSI s s2 ((a
	                             (lambda (k)
				       (and (not (equal? k ""))
				            (string? k)))
				     "epätyhjä alkio"))
	    `(,(char->integer (car (string->list a))))))))
	(,(string->symbol "\"") . ,(lambda (state return)
		  (let* ((next (state-words-head-lookahead! state))
			 (token (word->token next)))
			(return (-> state
				    (state-stack-push token)
				    (state-words-pop))))))
	(|#| . ,(lambda (state return)
		  (let* ((next (state-words-head-lookahead! state))
			 (token (word->token next)))
			(return (-> state
				    (state-stack-push (make-fact token))
				    (state-words-pop))))))
	(SIN . ,(lambda (state return)
	                   (calculation state (lambda (new-state val)
			       (return
			         (state-stack-push
				   new-state
				   (sin (* (/ val 180) fl-pi))))))))
	(COS . ,(lambda (state return)
	                   (calculation state (lambda (new-state val)
			       (return
			         (state-stack-push
				   new-state
				   (cos (* (/ val 180) fl-pi))))))))
	(LUELUKU . ,(lambda (state return)
                               (return (state-stack-push state (read-number (variable-reference-value lukuteksti))))))
	(LUELISTA . ,(lambda (state return)
	                (query-list (variable-reference-value listateksti)
	                            state
	                            (lambda (state2 input)
	                               (return (state-stack-push state2 input))))))
	(LUEALKIO . ,(lambda (state return)
	                (return (state-stack-push state (read-word (variable-reference-value alkioteksti))))))
	(LUKUTEKSTI . ,(push-cmd lukuteksti))
	(LISTATEKSTI . ,(push-cmd listateksti))
	(ALKIOTEKSTI . ,(push-cmd alkioteksti))
	(SIEMEN . ,(push-cmd siemen))
	(KONNA . ,(read-only-cmd (lambda (state)
                                            (turtle-new)
					    (turtle-penup)
					    (turtle-clear-position)
					    (turtle-clear-direction)
					    (turtle-clear-drawing)
					    (turtle-pendown)
					    (turtle-draw)
					    (turtle-update-render))))
	(UUSI . ,(read-only-cmd (lambda (state)
	  (with-turtle UUSI
	    (begin
	      (turtle-penup)
	      (turtle-clear-drawing)
	      (turtle-draw)
	      (turtle-update-render))
	    '()))))
	(KOTIIN . ,(read-only-cmd (lambda (state)
	  (with-turtle KOTIIN
	    (begin
	      (turtle-penup)
	      (turtle-clear-position)
	      (turtle-clear-direction)
	      (turtle-pendown)
	      (turtle-draw)
	      (turtle-update-render))
	    '()))))
	(OIKEA . ,(stack-cmd (lambda (s)
	  (with-turtle OIKEA
	    (typed-pop OIKEA s s2 ((l number? "luku"))
	      (begin
	        (turtle-alter-direction (* -1 l))
                (turtle-update-render)
		s2))
	    s))))
	(VASEN . ,(stack-cmd (lambda (s)
	  (with-turtle VASEN
	    (typed-pop VASEN s s2 ((l number? "luku"))
	      (begin
	        (turtle-alter-direction l)
                (turtle-update-render)
		s2))
	    s))))
	(ETEEN . ,(stack-cmd (lambda (s)
	            (with-turtle ETEEN
	               (typed-pop ETEEN s s2 ((l number? "luku"))
		           (begin
	                     (turtle-forwards l)
                             (turtle-update-render)
			     s2))
		       s))))
	(TAAKSE . ,(stack-cmd (lambda (s)
	            (with-turtle TAAKSE
	               (typed-pop TAAKSE s s2 ((l number? "luku"))
		           (begin
	                     (turtle-forwards (* -1 l))
                             (turtle-update-render)
			     s2))
		       s))))
	(KULMA . ,(stack-cmd (lambda (s)
	            (with-turtle KULMA
	               (typed-pop KULMA s s2 ((x number? "luku"))
		           (begin
	                     (turtle-set-direction x)
                             (turtle-update-render)
			     s2))
		       s))))
	(PAIKKA . ,(stack-cmd (lambda (s)
	             (with-turtle PAIKKA
	               (typed-pop PAIKKA s s2 ((y number? "luku")
		                              (x number? "luku"))
		           (begin
	                     (turtle-penup)
			     (turtle-move x y)
			     (turtle-pendown)
                             (turtle-update-render)
			     s2))
		       s))))
	(VIIVA . ,(stack-cmd (lambda (s)
	 (with-turtle VIIVA
	  (typed-pop VIIVA s s2 ((y number? "luku")
	                         (x number? "luku"))
			(begin
			  (turtle-move x y)
			  (turtle-update-render)
			  s2))
	  s))))
	(RVIIVA . ,(stack-cmd (lambda (s)
	 (with-turtle RVIIVA
	  (typed-pop RVIIVA s s2 ((y number? "luku")
	                          (x number? "luku"))
			(begin
			  (turtle-move (+ (turtle-x-pos) x) (+ (turtle-y-pos) y)))
                          (turtle-update-render)
			  s2)
	  s))))
	(JANA . ,(stack-cmd (lambda (s)
	 (with-turtle JANA
	  (typed-pop JANA s s2 ((|y'|number? "luku")
	                        (|x'|number? "luku")
				(y number? "luku")
				(x number? "luku"))
			(begin
			  (turtle-draw-segment x y |x'| |y'|)
			  (turtle-update-render)
			  s2))
	  s))))
	(TULOSTA . ,(stack-cmd (lambda (s)
	  (with-turtle TULOSTA
	               (typed-pop TULOSTA s s2 ((x number? "luku")
		                                (y number? "luku")
						(a string? "alkio"))
		           (begin
			     (turtle-draw-text (* x 8)
			                       (- 200 (* y 8.3))
					       (sampo-output a))
			     (turtle-update-render)
			     s2))
		       s))))
	(SIVUSUHDE . ,(stack-cmd (lambda (s)
	  (with-turtle SIVUSUHDE
	               (typed-pop SIVUSUHDE s s2 ((y number? "luku")
		                              (x number? "luku"))
		           (begin
			     (turtle-set-factors x y)
                             (turtle-update-render)
			     s2))
		       s))))
	(YLÖS . ,(read-only-cmd (lambda (state)
	  (with-turtle YLÖS
	    (begin
              (turtle-penup)
              (turtle-update-render))
	    '()))))
	(ALAS . ,(read-only-cmd (lambda (state)
	  (with-turtle ALAS
	    (begin
              (turtle-pendown)
              (turtle-update-render))
	    '()))))
	(PIIRRÄ . ,(read-only-cmd (lambda (state)
	  (with-turtle PIIRRÄ
	    (begin
	      (turtle-draw)
              (turtle-update-render))
	    '()))))
	(PYYHI . ,(read-only-cmd (lambda (state)
	  (with-turtle PYYHI
            (begin
	      (turtle-erase)
	      (turtle-update-render))
	    '()))))
	(KUVA . ,(read-only-cmd (lambda (state)
	  (with-turtle KUVA
	    (display (emit-tikz))
	    '()))))
	(XKO . ,(lambda (state return)
	  (with-turtle XKO
            (return (state-stack-push state (turtle-x-pos)))
	    (return state))))
	(YKO . ,(lambda (state return)
	  (with-turtle YKO
            (return (state-stack-push state (turtle-y-pos)))
	    (return state))))
	(SUUNTA . ,(lambda (state return)
	  (with-turtle SUUNTA
            (return (state-stack-push state (turtle-get-angle)))
	    (return state))))
	(RUUTUINEN . ,(stack-cmd (lambda (s)
	  (typed-pop RUUTUINEN s s2 ((n exact-integer? "kokonaisluku"))
	    (begin
	      (set! current-frame-count n)
	      s2)))))
	(TIEDOSTO . ,(lambda (state return)
			(let ((name (symbol->string (word->symbol (state-words-head-lookahead! state))))
			      (content (make-string (* 1024 current-frame-count) #\space)))
			     (with-output-to-file name (lambda () (display content)))
			     (return (state-words-pop state)))))
	(AVAA . ,(lambda (state return)
			(if (not (equal? current-blk-name ""))
			    (begin
			      (save-blk current-blk-name current-blk)
			      (set! current-database '())))
			(let ((name (symbol->string (word->symbol (state-words-head-lookahead! state)))))
			     (if (file-exists? name)
				 (begin
				    (set! current-blk (read-blk name))
				    (set! current-blk-name (string-append (current-directory) "/" name))
				    (return (-> state (state-words-pop))))
				 (begin
				    (display "Tiedostoa ei ole ")
				    (display name)
				    (display ".\n")
				    (return (-> state (state-words-pop))))))))
	(SULJE . ,(lambda (state return)
	  (with-file SULJE
	    (begin
		     (save-blk current-blk-name current-blk)
		     (set! current-database '())
		     (return state))
	    (return state))))
	(LUE . ,(stack-cmd (lambda (s)
	  (typed-pop LUE s s2 ((n
	                        (lambda (k)
				  (and (exact-integer? k)
				       (> k -1)))
				"ei-negatiivinen kokonaisluku"))
	    (begin
	      (if (>= n (vector-length sampo-blk))
		  (display "Ruutu ei ole tiedostossa SAMPO.BLK\n")
		  (let ((frame (vector-ref sampo-blk n)))
		       (display-frame n "SAMPO.BLK" frame)
		       (set! last-blk sampo-blk)
		       (set! current-frame-i n)
		       (set! current-frame frame)))
	      s2)))))
	(*LUE . ,(stack-cmd (lambda (s)
	  (typed-pop *LUE s s2 ((n
	                        (lambda (k)
				  (and (exact-integer? k)
				       (> k -1)))
				"ei-negatiivinen kokonaisluku"))
	   (with-file *LUE
	    (begin
	      (if (>= n (vector-length current-blk))
		  (display (string-append "Ruutu ei ole tiedostossa " current-blk-name "\n"))
		  (let ((frame (vector-ref current-blk n)))
		       (display-frame n current-blk-name frame)
		       (set! last-blk current-blk)
		       (set! current-frame-i n)
		       (set! current-frame frame)))
	      s2)
	    s)))))
	(SEU . ,(lambda (state return)
	            (if (< (+ current-frame-i 1) (vector-length last-blk))
		      (begin
		        (set! current-frame-i (+ current-frame-i 1))
			(set! current-frame (vector-ref last-blk current-frame-i))
			(display-frame current-frame-i "==||==" current-frame))
		      (display "Ruutua ei löydy avoimesta tiedostosta\n"))
		    (return state)))
	(ED . ,(lambda (state return)
	            (if (>= (- current-frame-i 1) 0)
		      (begin
		        (set! current-frame-i (- current-frame-i 1))
			(set! current-frame (vector-ref last-blk current-frame-i))
			(display-frame current-frame-i "==||==" current-frame))
		      (display "Ruutua ei löydy avoimesta tiedostosta\n"))
		    (return state)))
	(TALLETA . ,(stack-cmd (lambda (s)
	  (typed-pop TALLETA s s2 ((n (lambda (k)
			           (and (exact-integer? k)
				        (> k -1)))
				   "ei-negatiivinen kokonaisluku"))
		(begin
		  (vector-set! sampo-blk n current-frame)
		  s2)))))
	(*TALLETA . ,(stack-cmd (lambda (s)
	  (typed-pop *TALLETA s s2 ((n (lambda (k)
			           (and (exact-integer? k)
				        (> k -1)))
				   "ei-negatiivinen kokonaisluku"))
	     (with-file *TALLETA
		(begin
		  (vector-set! current-blk n current-frame)
		  s2)
		s)))))
	(KORJAA . ,(lambda (state return)
	  (with-frame KORJAA
	    (begin
		      (set! current-frame (edit-frame current-frame))
		      (vector-set! last-blk current-frame-i current-frame)
		      (return state))
	    (return state))))
	(SUORITA . ,(lambda (state return)
	  (with-frame SUORITA
		       (let ((tokens (tokenize (apply string-append (intercalate (vector->list current-frame) "\n")) '()))
                             (orig-vocab (state-vocab state))
                             (orig-call-stack (state-call-stack state)))
		   (run-words (-> state
                                  (state-set-vocab (state-root-vocab state))
				  (state-set-words tokens)
                                  (state-set-call-stack '()))
			      (lambda (state2)
				 (return (-> state2
                                             (state-set-call-stack orig-call-stack)
                                             (state-set-vocab orig-vocab)
                                             (state-set-words (state-words state)))))))
                       (return state))))
	(VIE . ,(stack-cmd (lambda (stack)
			     (typed-pop VIE stack stack2 ((row-n
			                                  (lambda (k)
							    (and (exact-integer? k)
							         (> k -1)
								 (< k 16)))
							  "kokonaisluku -1 < k < 16")
			                                  (frame-n
			                                  (lambda (k)
							    (and (exact-integer? k)
							         (> k -1)))
							  "ei-negatiivinen kokonaisluku")
							  (x
							  (lambda (k) #t)
							  "mikä tahansa arvo"))
				(with-file VIE
				  (begin
				    (if (< frame-n (vector-length current-blk))
				      (begin
				        (vector-set! (vector-ref current-blk frame-n) row-n (sampo-output x))
				        stack2)
				      (begin
				        (display "VIE: yritettiin kirjoittaa ruutuun, joka on tiedoston ulkopuolella")
					(newline)
					stack2)))
				  stack)))))
	(TUO . ,(stack-cmd (lambda (stack)
			     (typed-pop TUO stack stack2 ((row-n
			                                  (lambda (k)
							    (and (exact-integer? k)
							         (> k -1)
								 (< k 16)))
							  "kokonaisluku -1 < k < 16")
							  (frame-n
			                                  (lambda (k)
							    (and (exact-integer? k)
							         (> k -1)))
							  "ei-negatiivinen kokonaisluku"))
				(with-file TUO
				  (let ((row (-> current-blk
						 (vector-ref frame-n)
						 (vector-ref row-n)
						 (string-trim-both))))
				    (if (string-prefix? "(" row)
					`(,(call/cc (lambda (return)
						      (parse-list (tokenize row '())
								  (lambda (ws2 l)
								    (return (car l)))))) . ,stack2)
					`(,(or (string->sampo-number row) row) . ,stack2)))
				  stack)))))
	(KENTÄT . ,(push-cmd kentät))
	(SÄÄNTÖ . ,(push-cmd sääntö))
	(TIETOKANTA . ,(lambda (state return)
	  (with-file TIETOKANTA
	    (begin
			  (set! current-database (blk->tka current-blk))
			  (display "\nTietueita ")
			  (display (database-size current-database))
			  (display " kpl\n")
			  (return state))
	    (return state))))
	(LISÄÄ . ,(lambda (state return)
	  (with-database LISÄÄ
	             (let ((row (map
		                  (lambda (column)
		                  (display (to-string (column-name column)))
		                  (display ": ")
		                  ((column-parser column) (read-line)))
		                  (vector->list (database-columns current-database)))))
			  (database-rows-set! current-database (cons (list->vector row) (database-rows current-database)))
			  (set! current-blk (dump-database))
			  (return state))
		     (return state))))
	(HÄVITÄ . ,(stack-cmd (lambda (s)
	  (typed-pop HÄVITÄ s s2 ((n
		                  (lambda (k)
				    (and (exact-integer? k)
					 (> k 0)))
				  "positiivinen kokonaisluku"))
		    (with-database HÄVITÄ
		      (if (<= n (length (database-rows current-database))) 
		       (begin
	                 (database-rows-set! current-database
		           (drop-ndth
		             (database-rows current-database) (- n 1)))
		         (set! current-blk (dump-database))
		         s2)
		       (begin
		         (display "HÄVITÄ: yritettiin hävittää olematonta tietuetta ")
			 (display n)
			 (newline)))
		     s)))))
	(POIMI . ,(lambda (state return)
	  (with-database POIMI
		     (let* ((rule (variable-reference-value sääntö))
			    (action (variable-reference-value kentät))
			    (handler (lambda (rows i state2 return)
					(if (not (= (state-stack-head state2) 0))
					    (run-words (-> state
							   (state-call-stack-push 'KENTÄT)
							   (state-set-stack '())
							   (state-set-words (list->tokens action))
							   (state-set-vocab (database-add-print-vocab current-database i (car rows) state)))
						       (lambda (state3)
							  (newline)
							  (return (+ i 1) (cdr rows))))
					    (return (+ i 1) (cdr rows))))))
			   (let loop ((i 1)
				      (rows (database-rows current-database)))
				(if (not (eq? rows '()))
				    (let ((state2 (-> state
						      (state-set-stack '())
						      (state-call-stack-push 'SÄÄNTÖ)
						      (state-set-words (list->tokens (append rule '(=))))
						      (state-set-vocab (database-add-vocab current-database i (car rows) state)))))
					 (if (eq? rule 0)
					     (handler rows i (state-stack-push state2 -1) loop)
					     (run-words state2 (lambda (state2) (handler rows i state2 loop)))))
				    (return state))))
	             (return state))))
	(APU . ,(stack-cmd (lambda (s)
	  (typed-pop APU s s2 ((n
	                        (lambda (k)
				  (and (exact-integer? k)
				       (> k -1)))
				"ei-negatiivinen kokonaisluku"))
		   (let* ((frame (vector-ref auta-blk n)))
			 (display-frame n "AUTA.BLK" frame)
			 (set! last-blk auta-blk)
			 (set! current-frame-i n)
			 (set! current-frame frame)
			 s2)))))
	(DIR . ,(lambda (state return)
		   (display (string-join (list-sort! string<? (directory-files ".")) "\n"))
		   (newline)
		   (return state)))
	(CD . ,(lambda (state return)
		  (let ((name (word-token (state-words-head-lookahead! state))))
		       (if (not (change-directory name))
		         (begin
			   (display "'")
			   (display name)
			   (display "' ei ole kansio, johon voisi loikata.")
			   (newline)))
		       (return (state-words-pop state)))))
	(A: . ,(lambda (state return)
		  (change-directory A:)
		  (return state)))
	(B: . ,(lambda (state return)
		  (change-directory B:)
		  (return state)))
	(C: . ,(lambda (state return)
		  (change-directory C:)
		  (return state)))
	(SELITÄ . ,(lambda (state return)
	             (let* ((raw-command (symbol->string (word->symbol (state-words-head-lookahead! state))))
		            (file-name (cond
			                    ((equal? raw-command "/") "JAKO")
					    (else raw-command)))
			    (file-path (string-append
			      "SELITYS/SELITÄ_"
			      file-name
			      ".BLK")))
		       (if (file-exists? file-path)
		         (begin
		           (set! last-blk (read-blk file-path))
		           (set! current-frame-i 0)
		           (set! current-frame (vector-ref last-blk current-frame-i))
		           (display-frame current-frame-i (string-append "SELITÄ_" file-name ".BLK") current-frame))
			   (display (string-append
			     "Sanaa "
			     raw-command
			     " ei löydy SELITÄ-kansiosta. Se saattaa löytyä AUTA-dokumentaatiosta, johon pääset käsiksi sanalla AUTA. AUTA-dokumentaatiosta on myös olemassa pdf- ja verkkoversiot, joiden löytämisen ohjeet löytyvät myös AUTA-komennon takaa.\n")))
		       (return (state-words-pop state)))))
	(TEE . ,(lambda (state return)
	  (typed-pop TEE (state-stack state) s2 ((*X list? "lista"))
              (let ((orig-vocab (state-vocab state))
                    (orig-call-stack (state-call-stack state)))
		   (run-words (-> state
                                  (state-set-vocab (state-root-vocab state))
				  (state-set-words (list->tokens *X))
				  (state-set-stack s2)
                                  (state-set-call-stack '()))
			      (lambda (state2)
				 (return (-> state2
                                             (state-set-call-stack orig-call-stack)
                                             (state-set-vocab orig-vocab)
                                             (state-set-words (state-words state))))))))))
	(|\n| . ,(lambda (state return) (return state)))
	(LOPETA . ,(lambda (state return)
	  (sampo-finalize)))
	))

(define sampo-operators `(((TAI . ,(lambda (a b) (if (and (= a 0) (= b 0)) 0 -1))))
                          ((JA . ,(lambda (a b) (if (or (= a 0) (= b 0)) 0 -1))))
			  ((EI . ,(lambda (a b) (if (= b 0) -1 0))))
                          ((== . ,(lambda (a b) (if (equal? a b) -1 0)))
                           (<> . ,(lambda (a b) (if (equal? a b) 0 -1)))
                           (< . ,(lambda (a b) (if (< a b) -1 0)))
                           (<= . ,(lambda (a b) (if (<= a b) -1 0)))
                           (> . ,(lambda (a b) (if (> a b) -1 0)))
                           (>= . ,(lambda (a b) (if (>= a b) -1 0))))
                          ((+ . ,+)
                           (- . ,-))
                          ((* . ,*)
                           (/ . ,(lambda (a b)
                                    (if (or (inexact? a) (inexact? b) (eq? (modulo a b) 0)) (/ a b)
                                        (exact->inexact (/ a b)))))
                           (MOD . ,(lambda (a b)
                                    (if (eq? b 0)
                                        (error "modulo by zero")
                                        (modulo a b)))))
                          ((^ . ,(lambda (a b)
                            (cond
                              ((and (< b 0) (eq? a 0))
                                (error "zero to a negative power"))
                              ((< b 0)
                                (exact->inexact (expt a b)))
                              (else
                                (expt a b))))))
                          ))

(define sampo-operators-list '(TAI JA EI == <> < <= > >= + - * / MOD ^))

(define (calculation state return)
  (pop-calculation state (lambda (operation state2)
			   (eval-calculation operation state2 (lambda (operation2 state3 val) (return state3 val))))))

(define (pop-calculation state return)
  (let loop ((operation (list (state-stack-head state)))
	     (state (state-stack-pop state)))
    (if (eq? (state-stack state) '())
	(return operation state)
	(let ((v (state-stack-head state))
	      (new-state (state-stack-pop state)))
	  (if (memq v sampo-operators-list)
	      (loop `(,(state-stack-head new-state) ,v . ,operation) (state-stack-pop new-state))
	      (return operation state))))))

(define (eval-calculation operation state return)
  (eval-calculation-2 sampo-operators operation state return))

(define (compute-one-with-error-handler op acc second)
  (call-with-current-continuation
    (lambda (k)
      (with-exception-handler
        (lambda (x)
          (let ((message (error-object-message x)))
            (cond
              ((equal? message "divide by zero")
                (begin
                  (display "Virhe: nollalla jako laskussa ")
                  (display acc)
                  (display " / ")
                  (display second)
                  (newline)))
              ((equal? message "modulo by zero")
                (begin
                  (display "Virhe: modulo nollalla laskussa ")
                  (display acc)
                  (display " MOD ")
                  (display second)
                  (newline)))
              ((equal? message "zero to a negative power")
                (begin
                  (display "Virhe: nolla korotettu negatiiviseen potenssiin laskussa ")
                  (display acc)
                  (display " ^ ")
                  (display second)
                  (newline)))
              ((equal? message "invalid type, expected Number")
                (begin
                  (display "Virhe: jompikumpi laskettavista arvoista ei ole lukutyyppinen. Arvot ovat ")
                  (display (to-string acc))
                  (display " ja ")
                  (display (to-string second))
                  (newline)))
              (else
                 (display "Virhe: aritmeettminen virhe "
                 (write x)
                 (newline)
                 (display message)
                 (newline))))
            (k 0)))
        (lambda ()
          (k (op acc second)))))))

(define (eval-calculation-2 operators operation state return)
  (if (eq? operators '())
      (let ((v (car operation)))
	(cond
	 ((list? v) (run-words (state-set-words state (list->tokens-cons v (list (make-word "=" '=))))
	                       (lambda (state2)
	                          (return (cdr operation) (-> state2
	                                                      (state-stack-pop)
	                                                      (state-set-words (state-words state))) (state-stack-head state2)))))
	 ((or (number? v) (string? v)) (return (cdr operation) state v))
	 ((fact? v) (return (cdr operation) state (fact-value v)))
	 ((variable-reference? v) (return (cdr operation) state (variable-reference-value v)))
	 (else (begin
		 (display "Laiton arvo ")
		 (display v)
		 (display "!\n")
		 (return (cdr operation) state 0)))))
      (eval-calculation-2 (cdr operators)
			  operation
			  state
			  (lambda (operation2 state2 first)
			    (let loop ((loop-operation operation2)
				       (loop-state state2)
				       (acc first))
			      (if (> (length loop-operation) 0)
				  (let* ((raw-name (car loop-operation))
					 (op-name (if (string? raw-name)
						      (string->symbol raw-name)
						      raw-name))
				         (op (assq op-name (car operators))))
				    (if op
					(eval-calculation-2 (if (eq? (car op) '^) operators (cdr operators))
							    (cdr loop-operation)
							    loop-state
							    (lambda (operation3 state3 second)
							      (loop operation3 state3 (compute-one-with-error-handler (cdr op) acc second))))
					(return loop-operation loop-state acc)))
				  (return loop-operation loop-state acc)))))))

(define (parse-block starts end words return)
   (parse-block-with-depth 0 starts (list end) end #f words return))

(define (parse-block-with-depth n starts ends end nl-end words return)
   (if (eq? words '())
       (return '() words)
       (let* ((w (car words))
              (w2 (if (word? w) (word-word w) w)))
             (cond
              ((and (not nl-end) (procedure? w))
               (parse-block-with-depth n starts ends end nl-end (w (sampo-prompt-partial)) return))
              ((and (= n 0) (eq? w2 end))
               (return '() (cdr words)))
              ;((and nl-end (eq? w2 '|\n|))
              ; (return '() (cdr words)))
              (else
               (parse-block-with-depth (cond
                                        ((memq w2 starts) (+ 1 n))
                                        ((memq w2 ends) (- n 1))
                                        (else n))
                                       starts ends end nl-end (cdr words)
                                       (lambda (result rest)
                                          (return (cons w result) rest))))))))

(define (run-words state return)
   (cond
    ((eq? (state-words state) '()) ; Trivial case: no words to run / Lohko on tyhjä
                                   (return state))
    ((procedure? (state-words-head state)) ; Käskypinossa on funktio. Tätä käytetään vain lohkossa uusien rivien lukemiseen, ei tässä
     (return (-> state (state-words-pop))))
    ((number? (word-word (state-words-head state))) ; Push numbers onto the stack / Numerot työnnetään pinoon
     (run-words (-> state
                    (state-words-pop)
                    (state-stack-push (word-word (state-words-head state)))) return))
    (else
     (let* ((word (word-word (state-words-head state)))
            (command-pair (assq word (if (eq? (state-call-stack state) '()) (state-root-vocab state) (begin
                (state-vocab state)
                )
                )))) ; Extract the function for the word / Haetaan käskyä vastaava funktio
           (if command-pair
                     ((cdr command-pair) (let ((s (if (eq? (state-call-stack state) '())
                                                      (state-set-vocab state (state-root-vocab state))
                                                      state))) (-> s
                                             (state-words-pop)
                                             (state-call-stack-push word)))
                                         (lambda (state2)
                                            (run-words (state-call-stack-pop state2) return)))
               (begin ; If no matching command was found / Komentoa ei löytynyt
                  (display "Tuntematon komento ")
		  (display (symbol->string word))
		  (display ". Kutsupino: ")
		  (display (state-call-stack state))
		  (newline)
                  (run-words (-> state
                                 (state-words-pop)) return)))))))

(define (list->tokens l)
   (list->tokens-cons l '()))

(define (list->tokens-cons l ans)
   (if (eq? l '())
       ans
       (let ((obj (car l))
             (rest (list->tokens-cons (cdr l) ans)))
            (cond
             ((string? obj) (cons (string->number-or-symbol obj) rest))
             ((symbol? obj) (cons (string->number-or-symbol (symbol->string obj)) rest))
             ((list? obj) (cons (make-word "(" '|(|) (list->tokens-cons obj (cons (make-word ")" '|)|) rest))))
             (else (cons obj rest))))))

(define (string->number-or-symbol s)
   (if (string->sampo-number s)
       (make-word s (string->sampo-number s))
       (make-word s (string->symbol (string-upcase s)))))

(define (read-code rest)
   (let loop ((words '())
              (current-word '()))
        (let ((c (read-char)))
             (cond
              ((eq? c (eof-object))
               (let final-loop ((final-words (if (eq? current-word '())
                                                 words
                                                 (cons (list->string (reverse current-word)) words)))
                                (result rest))
                    (cond
                     ((eq? final-words '()) result)
                     ((eq? (car final-words) "\n")
                      (final-loop (cdr final-words) result))
                     (else (final-loop (cdr final-words) (cons (string->number-or-symbol (car final-words)) result))))))
              ((eq? c #\newline)
               (if (eq? current-word '())
                   (loop (cons "\n" words) '())
                   (loop (append (list "\n" (list->string (reverse current-word))) words) '())))
              ((eq? c #\space)
               (if (eq? current-word '())
                   (loop words '())
                   (loop (cons (list->string (reverse current-word)) words) '())))
              (else
               (loop words (cons c current-word)))))))

(define (read-line)
   (let loop ((line ""))
     (let ((c (read-char)))
       (cond
	((and (equal? line "") (eq? c (eof-object)))
	 (eof-object))
	((or (eq? c (eof-object)) (eq? c #\newline))
	 line)
	(else
	 (loop (string-append line (string c))))))))

(define (repl-reader message)
  (display message)
  (let ((line (read-line)))
    (if (eq? line (eof-object))
	(eof-object)
	(tokenize line `(,continued-line-reader)))))

(define (continued-line-reader message)
  (display message)
  (let ((line (read-line)))
    (if (eq? line (eof-object))
	'()
	(tokenize line `(,continued-line-reader)))))

(define (tokenize text rest)
   (parameterize ((current-input-port (open-input-string text)))
      (read-code rest)))

(define sampo-reader (make-parameter repl-reader))

(define sampo-prompt (make-parameter ">>> "))
(define sampo-prompt-partial (make-parameter "> "))
(define sampo-response (make-parameter ""))

(define (sampo-repl state)
   (if (state-stack-shown state)
       (stack-show (map to-string (state-stack state)))
       '())
   (let ((words ((sampo-reader) (sampo-prompt))))
     (if (not (eq? words (eof-object)))
	 (run-words (state-set-words state words) (lambda (state2)
                                                    (display (sampo-response))
                                                    (sampo-repl state2)))
	 (sampo-finalize))))

(define (sampo-finalize)
  (if (not (equal? current-blk-name ""))
    (save-blk current-blk-name current-blk)))

(define (read-blk filename)
   (with-input-from-file filename
     (lambda ()
     (let loop ((i 0)
                (overflow #f)
                (frames '()))
          (let ((c (read-char)))
               (cond
                ((eq? c (eof-object))
                 (list->vector (reverse (map (lambda (frame) (list->vector (string-split frame "\n"))) frames))))
                ((= (modulo i 1024) 0)
                 (loop 1 #f (cons (string c) frames)))
                ((and overflow (eq? c #\space))
                 (loop (+ 1 i) #f (cons (string-append (car frames) "\n ") (cdr frames))))
                ((= (modulo i 64) 63)
                 (if (eq? c #\space)
                     (loop (+ 1 i) #f (cons (string-append (car frames) " \n") (cdr frames)))
                     (loop (+ 1 i) #t (cons (string-append (car frames) (string c)) (cdr frames)))))
                (else
                 (loop (+ 1 i) overflow (cons (string-append (car frames) (string c)) (cdr frames))))))))))

(define (save-blk filename frames)
   (with-output-to-file filename
      (lambda ()
         (vector-for-each (lambda (frame)
                             (display (sanitize-frame frame)))
                          frames))))

(define (sanitize-frame frame)
  (let* ((lines (vector->list frame))
	 (sanitized-lines (map sanitize-line lines))
	 (len (length sanitized-lines)))
    (string-join (cond
		  ((< len 16)
		   (append sanitized-lines (make-list (- 16 len) (make-string 64 #\space))))
		  ((> len 16)
		   (take! sanitized-lines 16))
		  (else
		   sanitized-lines))
		 "")))

(define (sanitize-line line)
  (let ((len (string-length line)))
    (cond
     ((< len 64)
      (string-pad-right line 64))
     ((> len 64)
      (string-take line 64))
     (else line))))

; size: number
; columns: vector of column
; rows: list of vector
(define-record-type <database> (make-database size columns rows) database?
   (size database-size database-size-set!)
   (columns database-columns database-columns-set!)
   (rows database-rows database-rows-set!))

(define-record-type <column> (make-column name type parser) column?
   (name column-name column-name-set!)
   (type column-type column-type-set!)
   (parser column-parser column-parser-set!))

(define (database-add-vocab database row-n row state)
   (let loop ((i 0)
              (vocab (state-vocab state)))
        (if (= i (vector-length row))
            vocab
            (loop (+ i 1)
                  (cons `(,(column-name (vector-ref (database-columns database) i)) . ,(push-cmd (vector-ref row i)))
                        vocab)))))

(define (database-add-print-vocab database row-n row state)
   (append `((KAIKKITULOSTA . ,(lambda (state return)
                                  (display "Tietue nro: ")
                                  (display row-n)
                                  (newline)
                                  (let loop ((i 0))
                                       (if (= i (vector-length row))
                                           (begin
                                              (newline)
                                              (return state))
                                           (begin
                                              (display (sampo-output (column-name (vector-ref (database-columns database) i))))
                                              (display " : ")
                                              (display (vector-ref row i))
                                              (newline)
                                              (loop (+ i 1)))))))
             (TNRO. ,(lambda (state return)
                        (display row-n)
                        (display " "))))
           (database-add-vocab database row-n row state)))

(define (blk->tka blk)
   (let ((database (parse-database-scheme (vector-ref blk 1))))
        (let loop ((i 2)
                   (rows '()))
             (if (= (- i 2) (database-size database))
                 (begin (database-rows-set! database (reverse rows)) database)
                 (loop (+ i 1) (cons (parse-database-row database (vector-ref blk i)) rows))))))

(define (parse-database-scheme frame)
   (let* ((frame-rows (map string-trim-both (vector->list frame)))
          (db-size (or (string->number (list-ref frame-rows 0)) 0)))
         (let loop ((loop-frame-rows (cdr frame-rows))
                    (db-columns '()))
              (if (or (eq? loop-frame-rows '()) (equal? (car loop-frame-rows) ""))
                  (make-database db-size (list->vector (reverse db-columns)) '())
                  (loop (cdr loop-frame-rows)
                        (cons (parse-database-column-declaration (car loop-frame-rows)) db-columns))))))

(define (dump-database-scheme)
  (let* ((row-count (number->string (length (database-rows current-database))))
         (columns (map (lambda (col)
	                 (string-append (symbol->string (column-type col))
			                " "
					(symbol->string (column-name col))))
	            (vector->list (database-columns current-database)))))
    (list->vector (cons row-count columns))))

(define (dump-row row)
  (list->vector (cons "" (map to-string (vector->list row)))))

(define (dump-database)
  (let ((database-scheme (dump-database-scheme))
        (database-rows (map dump-row (database-rows current-database))))
       (list->vector (cons (vector) (cons database-scheme database-rows)))))


(define (parse-database-column-declaration line)
   (let ((split (delete! "" (string-split line " "))))
        (if (= (length split) 2)
            (let* ((name (string-upcase (cadr split)))
                   (type (string-upcase (car split)))
                   (parser (cond
                            ((equal? type "TEKSTIKENTTÄ") (lambda (x) x)) ; tekstiä ei tarvitse jäsentää
                            ((equal? type "NUMEROKENTTÄ") string->number)
                            (else (begin
                                     (display "Laiton kenttätyyppi \"")
                                     (display type)
                                     (display "\"\n")
                                     (lambda (x) x))))))
                  (make-column (string->symbol name) (string->symbol type) parser))
            (begin
               (display "Laiton kenttämäärittely \"")
               (display line)
               (display "\".\n")
               (make-column 'TUNTEMATON 'TUNTEMATON (lambda (x) x))))))

(define (parse-database-row database frame)
   (let ((frame-rows (map string-trim-both (vector->list frame)))
         (row (make-vector (vector-length (database-columns database)))))
        (let loop ((i 0)
                   (loop-frame-rows (cdr frame-rows)))
             (vector-set! row i ((column-parser (vector-ref (database-columns database) i)) (car loop-frame-rows)))
             (if (= (+ i 1) (vector-length row))
                 row
                 (loop (+ i 1) 
                       (cdr loop-frame-rows))))))

(define (intercalate l a)
  (reverse (cdr (fold-left (lambda (state item)
    (cons a (cons item state))) '() l))))

(define (display-frame n name frame)
   (display "\nRuutu # ")
   (display n)
   (display "\t\t")
   (display name)
   (newline)
   (let loop ((lines (vector->list frame))
              (i 0))
        (if (not (eq? lines '()))
            (begin
               (display " ")
               (if (< i 10)
                   (display " "))
               (display i)
               (display " ")
               (display (car lines))
               (newline)
               (loop (cdr lines) (+ 1 i))))))

(define sampo-blk (if (file-exists? "SAMPO.BLK")
                      (read-blk "SAMPO.BLK")
                      #()))

(define auta-blk (if (file-exists? "AUTA.BLK")
                     (read-blk "AUTA.BLK")
                     #()))

(define selitä-blk (if (file-exists? "SELITA.BLK")
                       (read-blk "SELITA.BLK")
                       #()))

(set! last-blk sampo-blk)

(define prelude
    "
    LUO K KOTIIN VALMIS
    LUO Y YLÖS VALMIS
    LUO A ALAS VALMIS
    LUO E ETEEN VALMIS
    LUO T TAAKSE VALMIS
    LUO O OIKEA VALMIS
    LUO V VASEN VALMIS
    LUO RIVI UUSIRIVI VALMIS
    LUO UR UUSIRIVI VALMIS
    
    LUO :x :y PISTE :x :y :x + 1 = :y + 1 = JANA VALMIS
    
    LUO AUTA 1 APU VALMIS
    
    LUO TUHOA POISTA VALMIS
    LUO KOPIO TUPLAA VALMIS
    
    LUO SAMAT? =$ VALMIS

    LUO :c :y :x MERKKI :c MERKIKSI :y :x TULOSTA VALMIS
    ")

(define initial-state 
   (parameterize ((current-input-port (open-input-string prelude)))
       (call/cc
         (lambda (return)
	     (run-words (make-state (read-code '()) vocab '() '() #f vocab)
	         return)))))))
