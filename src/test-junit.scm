(import (scheme base) (scheme small) (scheme write) (sampo-test) (turtle) (util))

(set_headless)

(define (add-success desc significant)
  (display "  <testcase classname=\"test\" name=\"")
  (display (xml-bleach desc))
  (display "\"/>")
  (newline))

(define (add-failure desc message significant)
  (display "  <testcase classname=\"test\" name=\"")
  (display (xml-bleach desc))
  (display "\">")
  (newline)
  (display "    <failure type=\"failed\">")
  (display (xml-bleach message))
  (display "</failure>")
  (newline)
  (display "  </testcase>"))

(define (add-header header) '())

(define testqty 0)
(define significant-fail #f)

(define (final-result success failed global-failure) '()
  (set! significant-fail global-failure)
  (set! testqty (+ success failed)))

(let ((output-port (open-output-string)))
  (parameterize ((current-output-port output-port))
    (run-tests add-success add-failure add-header final-result))
  (display "<testsuite tests=\"")
  (display testqty)
  (display "\">")
  (newline)
  (display (get-output-string output-port))
  (display "</testsuite>")
  (if significant-fail
      (exit 1)
      '()))
