# Loitsut paikallisen ja globaalin chibi-schemen käynnistämiseen

LOCAL_CHIBI = LD_LIBRARY_PATH=$LD_LIBRARY_PATH:chibi-scheme CHIBI_MODULE_PATH=chibi-scheme/lib ./chibi-scheme/chibi-scheme -I src
GLOBAL_CHIBI = chibi-scheme -I src

# Onko globaalia chibiä tarjolla?

CHIBI_TEST := $(shell command -v chibi-scheme)

# Käytetään globaalia chibiä, jos sellainen löytyy

ifeq ($(CHIBI_TEST),)
	CHIBI = $(LOCAL_CHIBI)
	CHIBI_INCLUDE = -Ichibi-scheme/include
else
	CHIBI = $(GLOBAL_CHIBI)
endif

build-c: lib/libsampo.so

lib/libsampo.so: src/c/stubfile.stub src/c/sampo.c
	mkdir -p lib
	@if [ -z `command -v chibi-ffi` ]; then\
		make chibi-scheme/chibi-scheme;\
		$(LOCAL_CHIBI) chibi-scheme/tools/chibi-ffi src/c/stubfile.stub;\
	else\
		chibi-ffi src/c/stubfile.stub;\
	fi
	gcc $(CHIBI_INCLUDE) -shared -o lib/libsampo.so -fPIC src/c/stubfile.c `pkg-config --cflags --libs sdl2 SDL2_ttf`;\

# Noutaa chibin lähdekoodin

chibi-scheme:
	git clone https://github.com/ashinn/chibi-scheme.git

# Kääntää chibin natiiviksi binääriksi

chibi-scheme/chibi-scheme: chibi-scheme
	make -C chibi-scheme && make -C chibi-scheme test

# Noutaa ja asentaa paikallisesti emsdk:n

emsdk:
	git clone https://github.com/emscripten-core/emsdk.git
	cd emsdk; ./emsdk install latest
	cd emsdk; ./emsdk activate latest

# Kääntää chibin js:ksi

chibi-scheme/js: emsdk chibi-scheme
	ldd emsdk/upstream/bin/clang
	bash -c 'source emsdk/emsdk_env.sh; make -C chibi-scheme js'

linuxdeploy-x86_64.AppImage:
	wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
	chmod +x linuxdeploy-x86_64.AppImage

appimage: chibi-scheme/chibi-scheme linuxdeploy-x86_64.AppImage lib/libsampo.so auta selita
	./linuxdeploy-x86_64.AppImage --appdir AppDir
	cp AppRun AppDir
	make -C chibi-scheme PREFIX=../AppDir/usr
	make -C chibi-scheme PREFIX=../AppDir/usr install
	cp run-sampo AppDir/usr/bin
	mkdir AppDir/usr/share/sampo
	cp -r src/ AppDir/usr/share/sampo
	cp -r lib/ AppDir/usr/share/sampo
	cp lib/libsampo.so AppDir/usr/lib
	cp MxPlus_IBM_VGA_8x14.ttf AppDir/usr/share/sampo
	cp sampo.desktop AppDir
	cp sampo.svg AppDir
	LD_LIBRARY_PATH=$LD_LIBRARY_PATH:chibi-scheme ./linuxdeploy-x86_64.AppImage --appdir AppDir -o appimage
	mkdir Sampo20-AppImage
	mv Sampo-*.AppImage Sampo20-AppImage
	cp *.BLK *.TKA Sampo20-AppImage
	cp -r SELITYS Sampo20-AppImage

run: lib/libsampo.so auta selita
	SAMPO_LIB=`pwd`/lib $(CHIBI) -I `pwd`/lib src/runsampo.scm

test: lib/libsampo.so
	SAMPO_LIB=`pwd`/lib $(CHIBI) -I `pwd`/lib src/test.scm

test-junit: lib/libsampo.so
	SAMPO_LIB=`pwd`/lib $(CHIBI) -I `pwd`/lib src/test-junit.scm > junit.xml

js-common: chibi-scheme/js documentation/web auta selita
	mkdir -p html5-release
	cp chibi-scheme/js/chibi.js html5-release
	cp chibi-scheme/js/chibi.data html5-release
	cp chibi-scheme/js/chibi.wasm html5-release
	cp codemirror/codemirror.css html5-release
	cp codemirror/codemirror.js html5-release
	cp html5/sampo-mode.js html5-release
	cp html5/editor-theme.css html5-release
	cp html5/index.html html5-release
	cp html5/style.css html5-release
	cp html5/sampo-preloader.js html5-release
	cp html5/sampo-loader.js html5-release
	cp html5/util.js html5-release
	cp html5/turtle.js html5-release
	cp html5/turtle-client.js html5-release
	cp html5/*.woff2 html5-release
	cp html5/google39a35c47fdeb2f0e.html html5-release
	cp html5/BingSiteAuth.xml html5-release
	cp -r documentation/web html5-release/documentation
	# SAMPO-tiedostojärjestelmä
	mkdir -p html5-release/fs
	cp src/sampo.sld html5-release/fs
	cp src/util.sld html5-release/fs
	cp src/turtle-web.sld html5-release/fs/turtle.sld
	cp src/console-web.sld html5-release/fs/console.sld
	cp *.BLK *.TKA html5-release/fs
	cp -r SELITYS html5-release/fs

js: js-common
	# SAMPO-tiedostojärjestelmä json-muotoon
	python3 mk-json.py html5-release/fs > html5-release/fs.json
	rm -rf html5-release/fs/

js-test: js-common html5/test-loader.js
	cp html5/test-loader.js html5-release/sampo-loader.js
	cp src/sampo-test.sld html5-release/fs/sampo-test.sld
	# SAMPO-tiedostojärjestelmä json-muotoon
	python3 mk-json.py html5-release/fs > html5-release/fs.json
	rm -rf html5-release/fs/

documentation/pdf:
	make -C documentation build-pdf

documentation/blk:
	make -C documentation build-blk

documentation/web:
	make -C documentation build-web

documentation: documentation/pdf documentation/blk documentation/web

auta: documentation/blk
	cp documentation/blk/AUTA.BLK .

selita: documentation/blk
	mkdir -p SELITYS
	cp documentation/blk/individual_blk/* SELITYS

clean:
	rm html5-release -rf
	rm lib -rf
	rm chibi-scheme -rf
	rm emsdk -rf
	rm AppDir -rf
	rm -f Sampo-*-x86_64.AppImage
	rm -f linuxdeploy-x86_64.AppImage
	rm -f src/c/stubfile.c
	rm -f junit.xml
	make -C documentation clean
	rm -f AUTA.BLK
	rm -f TESTI.BLK
	rm -rf SELITYS

.PHONY: test run build-c clean
